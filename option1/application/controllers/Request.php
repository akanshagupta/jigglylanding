<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . 'libraries/facebook/facebook.php';

class Request extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');  //Load the Session
		 $this->load->model('getdata');
     }
	
public function invite()
    {
        
    $this->load->view('campaign/wfblp.php');
		
    }

public function submituserinfo()
    {
		//echo "Ashish"; exit;
		 $usernameone = $this->input->post('usernameone');
		 $emailone = $this->input->post('emailone');
		//echo $usernameone; echo $emailone; exit;
		//echo $usernametwo; echo $emailtwo; exit;
		$email = $this->input->post('emailone');
		$this->load->library('uuid');
        $ran_var = $this->uuid->v4();
		$getcampnreg = $this->getdata->getcampnreg($usernameone, $emailone);
		//print_r($getcampnreg); exit;
		if(count($getcampnreg) == '0')
		{
		$inserid = $this->getdata->insert_userinfo();
		//$this->load->view('campaign/thankyou.php');
		 $newdata['value'] = array(
            'insertid' => $inserid,
            'rand_num' => $ran_var,
			'user_name' => $usernameone
        );
		$from_email = "password@rigalio.com";
        $to_email = $email;
        $this->load->library('email');
        $this->email->from($from_email, 'Rigalio Luxury');
        $this->email->to($to_email);
        $this->email->subject('Rigalio Invitation');
        $body = $this->load->view('campaign/mailer.php', $newdata, TRUE);
        $this->email->message($body);
        //Send mail
        if ($this->email->send()) {
            echo "1"; exit;
        } else {

            echo "0"; exit;
        }
		}
		else
		{
		   echo "2"; exit;
		}
	    
    }
	
	public function fblinkedinlogin()
    {
	$this->load->model('getdata');
	$this->session->unset_userdata('inviteuserid');
	$userregistrationid = $this->session->userdata('registrationid');
	$this->load->model('getdata');
	$data['avali_cat'] = $this->getdata->availcat();
	$data['all_products_count'] = $this->getdata->record_count();
	$data['seo'] = $this->getdata->seo('page', 'resetpassword');
	$data['userdata'] = $this->getdata->getuserdata($userregistrationid);
	$data['all_cat'] = $this->getdata->get_all_category();//for getting all the category in the head section
	//$this->load->view('pages/head_main.php', $data);//passing category in the head section
	$this->load->view('campaign/fblinkedin.php', $data);
	$this->load->view('pages/footer.php');
		
	}
	
	 public function reqinvitefb()
    {
        $this->load->model('getdata');
        $email = $this->input->post('email');
        $friend_list = $this->input->post('friendlist');
        $user_fb_id = $this->input->post('user_fb_id');
        $imgdata = "http://graph.facebook.com/" . $user_fb_id . "/picture?type=normal&height=553&width=553";

#-----------------MAIN CODE START HERE----------------------#

        $data = file_get_contents($imgdata);
        $fileName = ".$user_fb_id." . '-' . $user_fb_id . '.jpg';
        $file = fopen('uploads/profilepic/' . $fileName, 'w+');
        fputs($file, $data);
        fclose($file);
        $imgurl = '/uploads/profilepic/' . $fileName;
        $friend = json_encode($friend_list);
        $availemail = $this->getdata->availemail($email);
       //print_r($availemail);
        //$data['availemail'] = $this->getdata->availemail($email);
        //print_r($data['availemail']); 
        if (count($availemail) > 0) {
            echo '1';
        } else {
            $savedata = $this->getdata->requestndinvitefb($friend, $imgurl);
            echo '2';
        }
    }
	
	public function reqinvitelinkedin(){
		$this->load->model('getdata');
	    $email = $this->input->post('email');
		$data['availemail'] = $this->getdata->availemail($email);
		//print_r($data['availemail']); 
		if(count($data['availemail'])>0)
		{
		echo '1';
		}
		else
		{
		$savedata = $this->getdata->requestndinvitelinkdin();
		echo '2';
		}
		}
		
	public function afterregthankyou()
	{
	$this->load->view('campaign/afterregthankyou.php');
	}
	
	public function beforeregthankyou()
	{
	$this->load->view('campaign/thankyou.php');
	
	}
	
	
}
