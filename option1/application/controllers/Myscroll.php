<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Myscroll extends CI_Controller {
 
 
    public function index()
    {
        $this->load->view('scroll.php');
    }
 
    public function getCountry(){
        $page =  $_GET['page'];
        $this->load->model('scrollmodel');
		$this->load->model('getdata');
		 $userregistrationid = $this->session->userdata('registrationid');
		 $data['user_follow']=$this->getdata->get_user_follow($userregistrationid);// To get user follow details
		$data['user_crown']=$this->getdata->get_user_crown($userregistrationid);// To get user crown details
		$data['userdata'] = $this->getdata->getuserdata($userregistrationid);
		 $data['user_saveforlater']=$this->getdata->get_saveforlater($userregistrationid);
		if($userregistrationid == '')
		{
			
			//echo "not"; 
		  $data['products'] = $this->scrollmodel->getCountry($page);
		}
		else
		{
			//echo "ok"; 
			$hideproduct=$this->getdata->hideproduct($userregistrationid); 
			$data['products']=$this->scrollmodel->removehideproductscroll($hideproduct, $page);
		}
        //$data['products'] = $this->scrollmodel->getCountry($page);
        //print_r($data);
        $this->load->view('pages/sc.php',$data);

        
    }
     public function getcat(){
        $page =  $_GET['page'];
      	$category_id=  $_GET['category_id'];
        $this->load->model('scrollmodel');
		 $this->load->model('getdata');
		 $userregistrationid = $this->session->userdata('registrationid');
		 $data['user_follow']=$this->getdata->get_user_follow($userregistrationid);// To get user follow details
		$data['user_crown']=$this->getdata->get_user_crown($userregistrationid);// To get user crown details
		$data['userdata'] = $this->getdata->getuserdata($userregistrationid);
		 $data['user_saveforlater']=$this->getdata->get_saveforlater($userregistrationid);
		if($userregistrationid == '')
		{
        $data['products'] = $this->scrollmodel->getcat_product($page,$category_id);
		}
		else
		{
			$hideproduct=$this->getdata->hideproduct($userregistrationid); 
			$data['products']=$this->scrollmodel->removehideproductscrollcatwise($hideproduct, $page, $category_id);
		}
        $this->load->view('pages/cat_product.php',$data);

        
    }
	 public function getsubcat(){
        $page =  $_GET['page'];
      	$subcategory_id=  $_GET['subcategory_id'];
        $this->load->model('scrollmodel');
		 $this->load->model('getdata');
		 $userregistrationid = $this->session->userdata('registrationid');
		 $data['user_follow']=$this->getdata->get_user_follow($userregistrationid);// To get user follow details
		$data['user_crown']=$this->getdata->get_user_crown($userregistrationid);// To get user crown details
		$data['userdata'] = $this->getdata->getuserdata($userregistrationid);
		 $data['user_saveforlater']=$this->getdata->get_saveforlater($userregistrationid);
		if($userregistrationid == '')
		{
        $data['products'] = $this->scrollmodel->getsubcat_product($page,$subcategory_id);
		}
		else
		{
		$hideproduct=$this->getdata->hideproduct($userregistrationid); 
		$data['products']=$this->scrollmodel->removehideproductscrollsubcatwise($hideproduct,$page,$subcategory_id);
		}
        $this->load->view('pages/subcat_product.php',$data);

        
    }
	 public function getbrand(){
        $page =  $_GET['page'];
      	$brand_id=  $_GET['brand_id'];
        $this->load->model('scrollmodel');
		 $this->load->model('getdata');
		 $userregistrationid = $this->session->userdata('registrationid');
		 $data['user_follow']=$this->getdata->get_user_follow($userregistrationid);// To get user follow details
		$data['user_crown']=$this->getdata->get_user_crown($userregistrationid);// To get user crown details
		 $data['user_saveforlater']=$this->getdata->get_saveforlater($userregistrationid);
		$data['userdata'] = $this->getdata->getuserdata($userregistrationid);
		if($userregistrationid == '')
		{
        $data['products'] = $this->scrollmodel->getbrand_product($page,$brand_id);
		}
		else
		{
		$hideproduct=$this->getdata->hideproduct($userregistrationid); 
		$data['products']=$this->scrollmodel->removehideproductscrollbrandwise($hideproduct,$page,$brand_id);
		}
        $this->load->view('pages/brand_prod.php',$data);        
    }
    public function getbrandprod(){
        $page =  $_GET['page'];
      	$brand_text=  $_GET['brand_text'];
      	 $check = $_GET['check'];
        $this->load->model('scrollmodel');
		 $this->load->model('getdata');
		 $userregistrationid = $this->session->userdata('registrationid');
		 $data['user_follow']=$this->getdata->get_user_follow($userregistrationid);// To get user follow details
		$data['userdata'] = $this->getdata->getuserdata($userregistrationid);
		 $data['user_saveforlater']=$this->getdata->get_saveforlater($userregistrationid);
        $data['search_brandresult'] = $this->scrollmodel->searchbrand_product($page,$brand_text,$check);
        //print_r($data);
        $this->load->view('pages/search_brand.php',$data);

        
    }
    
    public function getsearchprod(){
        $page =  $_GET['page'];
      	$product_text=  $_GET['product_text'];
      	$check = $_GET['check'];
        $this->load->model('scrollmodel');
		 $this->load->model('getdata');
		 $userregistrationid = $this->session->userdata('registrationid');
		 $data['user_follow']=$this->getdata->get_user_follow($userregistrationid);// To get user follow details
		$data['user_crown']=$this->getdata->get_user_crown($userregistrationid);// To get user crown details
		$data['userdata'] = $this->getdata->getuserdata($userregistrationid);
		 $data['user_saveforlater']=$this->getdata->get_saveforlater($userregistrationid);
        $data['search_productresult'] = $this->scrollmodel->search_product($page,$product_text,$check);
       // print_r($data);
     
        $this->load->view('pages/search_product.php',$data);

        
    }
     public function getbrandpeople(){
        $page =  $_GET['page'];
      	$people_text=  $_GET['people_text'];
      	$check = $_GET['check'];
        $this->load->model('scrollmodel');
		 $this->load->model('getdata');
		 $userregistrationid = $this->session->userdata('registrationid');
		$data['userdata'] = $this->getdata->getuserdata($userregistrationid);
        $data['search_people'] = $this->scrollmodel->search_people($page,$people_text,$check);
        //print_r($data);
     
        $this->load->view('pages/brand_people.php',$data);

        
    }
}
