<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'libraries/facebook/facebook.php';
class Usersetting extends CI_Controller {

			 public function __construct(){
				    parent::__construct();
				    $this->load->helper('url_helper');
                    $this->load->helper('form');
                    $this->load->library('form_validation');
					$this->load->model('settingmodel');
				    $this->load->library('session');  //Load the Session
					//$this->config->load('facebook');
					//$this->output->enable_profiler(True );
					//$this->output->cache(5); //Load the facebook.php file which is located in config directory
			    }
				
		     public function setting(){
			  
			       $this->load->model('getdata');
				   $this->load->model('settingmodel');
			  	   $data['seo']=$this->getdata->seo('page','usersetting');
				   $data['products']=$this->getdata->getall();
			  	   $data['all_cat']=$this->getdata->get_all_category();
				   $data['avali_cat']=$this->getdata->availcat();
				   $userregistrationid = $this->session->userdata('registrationid');
				 $data['user_all_detials']=$this->getdata->getuserdata($userregistrationid);
				  // echo $userregistrationid;
				  // $userregistrationid = $this->session->userdata('registrationid');
				  if($userregistrationid != '')
				  {
				   $data['userdata'] = $this->settingmodel->getuserdata($userregistrationid);
				   $data['getuserdataall'] = $this->settingmodel->getuserdataall($userregistrationid); 
				   $data['userpic'] = $this->settingmodel->getuserpic($userregistrationid); 
				   // $data['usercrown'] = $this->settingmodel->getusercrown($userregistrationid);
				 //  $data['usercomment'] = $this->settingmodel->getusercomment($userregistrationid);
				   // print_r($data['usercomment']);  exit;
				  // $data['userpost'] = $this->settingmodel->getuserpost($userregistrationid);
				   $data['getuserdetail'] = $this->settingmodel->getuserdetail($userregistrationid);
				 //  $data['getuserpostimage'] = $this->settingmodel->getuserpostimage($userregistrationid);
				 //  $data['getfollower'] = $this->settingmodel->getfollower($userregistrationid);
                 //  $data['getbrand'] = $this->settingmodel->getbrand($userregistrationid);
				   //print_r($data['getfollower']);  exit;
				   //print_r($getuserpostimage['is_active']); exit;
				   $data['useremail'] = $this->settingmodel->alluseremail($userregistrationid);
				   $data['userphoneno'] = $this->settingmodel->alluserphoneno($userregistrationid); 
				 // print_r($data['useremail']); exit;
                    $this->load->view('pages/head.php',$data);
			 	   $this->load->view('pages/usersetting.php' ,$data);
			 	   $this->load->view('pages/footer.php');
				  }
				  else{
redirect(base_url());
					  }
  		  }	
		  
		  public function userpassexist(){
		
			 $this->load->model('settingmodel');
			 $userregistrationid = $this->session->userdata('registrationid');
			 $currentpass = $this->input->post('currentpass');
			 $password = $this->settingmodel->passexist($currentpass, $userregistrationid);
			 if(count($password) == 0)
			 {
			 echo '0'; exit;
			 }
			 else
			 { 
			 echo '1'; exit;
			 }
  		 }
		 
		public function passwordchange(){
		
			 $this->load->model('settingmodel');
			 $userregistrationid = $this->session->userdata('registrationid');
			 $newpass = $this->input->post('newpass');
			 $newpassword = $this->settingmodel->passupdate($newpass, $userregistrationid);
redirect('main/logout');
//echo "logout";
  		 }
		 
		 public function dobchange(){
		
			 $this->load->model('settingmodel');
			 $userregistrationid = $this->session->userdata('registrationid');
			 $newdob = $this->input->post('newdob');
			 $newpassword = $this->settingmodel->dobupdate($newdob, $userregistrationid);
$getnewdob = $this->settingmodel->getnewdob($userregistrationid);
			 echo $getnewdob[0]['dob'];
  		 }
		 
		 public function addphoneno(){
			 $this->load->model('settingmodel');
			 $userregistrationid = $this->session->userdata('registrationid');
			 $phoneexist = $this->settingmodel->phonenoexist($userregistrationid);
			// $phoneexist = $this->settingmodel->phonenoexist1($userregistrationid);
			 $addphone = $this->input->post('addphone');
			if($phoneexist[0]['phone']=='')
			{
			 $addphoneno = $this->settingmodel->addphoneno($addphone, $userregistrationid);
			// echo '0';
			}
			else{
			$checkphoneno=$this->settingmodel->checkphonenomain($addphone, $userregistrationid);
			$checkphoneno1=$this->settingmodel->checkphoneno($addphone, $userregistrationid);
			if(count($checkphoneno)==0 && count($checkphoneno1)==0){
			$addmorephoneno = $this->settingmodel->addmorephoneno($addphone, $userregistrationid);
			$data['addphonenoajax'] = $this->settingmodel->ajaxphoneno($addmorephoneno);
			$this->load->view('pages/addphoneno.php',$data);
			}
			else{
				//echo "already exist";
				echo '1';
				
			}
			//echo '1';	
			}
		 }
		 
		 public function addemail(){
			$this->load->model('settingmodel');
			$userregistrationid = $this->session->userdata('registrationid');
			$addemail = $this->input->post('addemail');
			$checkemail=$this->settingmodel->checkemailid($addemail, $userregistrationid);
			$checkemail1=$this->settingmodel->checkemailidreg($addemail, $userregistrationid);
			//print_r($checkemail); print_r($checkemail1); exit;
			if(count($checkemail)==0 && count($checkemail1)==0){
			$addmoreemail = $this->settingmodel->addmoreemail($addemail, $userregistrationid);
			$data['addemailajax'] = $this->settingmodel->ajaxemail($addmoreemail);
			//print_r($addemailajax);
			$this->load->view('pages/addemail.php',$data);
				//echo "0";
			}
			else{
			echo "1";
			
			}
			//echo '1';	
		 }
		 
		public function deleteemail()
		{
		$id = $this->uri->segment(3);
		$this->load->model('settingmodel');
		$this->settingmodel->deleteemailid($id);
		redirect($_SERVER['HTTP_REFERER']);
		}
		
		public function makeprimary()
		{
		 $this->load->model('settingmodel');
		 $userregistrationid = $this->session->userdata('registrationid');
		 $primaryemailiddata = $this->settingmodel->getprimaryemail($userregistrationid);
		 $primaryemailid = $primaryemailiddata[0]['email']; 
		 $id = $this->uri->segment(3);
		 $wantstomake = $this->settingmodel->wantstomake($id);
		 $wantstomakepe = $wantstomake[0]['useremail']; 
		 $updatenonprimary = $this->settingmodel->updatenonprimary($id, $primaryemailid);
		 $updateprimary = $this->settingmodel->updateprimary($userregistrationid, $wantstomakepe);
		 $updateprimaryuserlogin = $this->settingmodel->updateprimaryuserlogin($userregistrationid, $wantstomakepe);
		 $updateprimaryuserdetail = $this->settingmodel->updateprimaryuserdetail($userregistrationid, $wantstomakepe);
		 redirect($_SERVER['HTTP_REFERER']);
		}
		
		public function deletephoneno()
		{
		$id = $this->uri->segment(3);
		$this->load->model('settingmodel');
		$this->settingmodel->deletephoneno($id);
		redirect($_SERVER['HTTP_REFERER']);
		}
		
		public function makeprimaryphone()
		{
		 $this->load->model('settingmodel');
		 $userregistrationid = $this->session->userdata('registrationid');
		 $primaryphonenodata = $this->settingmodel->getprimaryphoneno($userregistrationid);
		 $primaryphoneno = $primaryphonenodata[0]['phone']; 
		 //echo $primaryphoneno;
		 $id = $this->uri->segment(3);
		 $wantstomakeph = $this->settingmodel->wantstomakeph($id);
		 $wantstomakephpk = $wantstomakeph[0]['phoneno']; 
		 //echo $wantstomakephpk; exit;
		 $updatenonprimaryph = $this->settingmodel->updatenonprimaryph($id, $primaryphoneno);
		 $updateprimaryph = $this->settingmodel->updateprimaryph($userregistrationid, $wantstomakephpk);
		 redirect($_SERVER['HTTP_REFERER']);
		}
		
		public function updateinfo()
		{
		$this->load->model('settingmodel');
		$userregistrationid = $this->session->userdata('registrationid');
		$updatedinfo = $this->input->post('updatedinfo');
		$updateinfodata = $this->settingmodel->updateinfo($userregistrationid, $updatedinfo);
		//echo $updatedinfo; 
		redirect($_SERVER['HTTP_REFERER']);
		}
		
		public function updatecommunication()
		{
		$this->load->model('settingmodel');
		$userregistrationid = $this->session->userdata('registrationid');
		$keepyou = $this->input->get('keepyou');
		$updatecommunication = $this->settingmodel->updatecommunication($userregistrationid, $keepyou);
		//print_r($keepyou); exit;
		redirect($_SERVER['HTTP_REFERER']);
		}
		
		public function canseecrown()
		{
		$this->load->model('settingmodel');
		$userregistrationid = $this->session->userdata('registrationid');
		$seecrown = $this->input->post('seecrown');
		$updatecommunication = $this->settingmodel->updatecrownsetting($userregistrationid, $seecrown);
		//print_r($keepyou); exit;
		redirect($_SERVER['HTTP_REFERER']);
		}
		
		public function canseecomment()
		{
		$this->load->model('settingmodel');
		$userregistrationid = $this->session->userdata('registrationid');
		$seecomment = $this->input->post('seecomment');
		$updatecomment = $this->settingmodel->updatecommentsetting($userregistrationid, $seecomment);
		//print_r($keepyou); exit;
		redirect($_SERVER['HTTP_REFERER']);
		}
		
		public function canseestatus()
		{
		$this->load->model('settingmodel');
		$userregistrationid = $this->session->userdata('registrationid');
		$seestatus = $this->input->post('seestatus');
		$updatestatus = $this->settingmodel->updatestatussetting($userregistrationid, $seestatus);
		//print_r($keepyou); exit;
		redirect($_SERVER['HTTP_REFERER']);
		}
		
		public function canseephotos()
		{
		$this->output->enable_profiler(True );
		$this->load->model('settingmodel');
		$userregistrationid = $this->session->userdata('registrationid');
		$seephotos = $this->input->post('seephotos');
		//$imagenotnulldata = $this->settingmodel->postimagenotnull($userregistrationid);
		//print_r($me); exit;
		//if(count($imagenotnulldata)>0)
		//{
		//echo count($me); exit;
		$updatephotos = $this->settingmodel->updatephotossetting($userregistrationid, $seephotos);
		//print_r($keepyou); exit;
		//}
		redirect($_SERVER['HTTP_REFERER']);
		}
		
		public function canseefollower()
		{
		$this->output->enable_profiler(True );
		$this->load->model('settingmodel');
		$userregistrationid = $this->session->userdata('registrationid');
		$seefollower = $this->input->post('seefollower');
		$updatefollower = $this->settingmodel->updatefollowersetting($userregistrationid, $seefollower);
		//print_r($keepyou); exit;
		//}
		redirect($_SERVER['HTTP_REFERER']);
		}

                public function canseebrand()
		{
		$this->load->model('settingmodel');
		$userregistrationid = $this->session->userdata('registrationid');
		$seebrand = $this->input->post('seebrand');
		$updatebrand = $this->settingmodel->updatebrandsetting($userregistrationid, $seebrand);
		//print_r($keepyou); exit;
		//}
		redirect($_SERVER['HTTP_REFERER']);
		}
		
		 public function canseecollection()
		{
		$this->load->model('settingmodel');
		$userregistrationid = $this->session->userdata('registrationid');
		$seecollection = $this->input->post('seecollection');
		$updatecollection = $this->settingmodel->updatecollectionsetting($userregistrationid, $seecollection);
		//print_r($keepyou); exit;
		//}
		redirect($_SERVER['HTTP_REFERER']);
		}

              public function logout(){
			$base_url=$this->config->item('base_url'); //Read the baseurl from the config.php file
			$this->session->sess_destroy();  //session destroy
			redirect($base_url);  //redirect to the home page
	      }


	public function accesstimeline()
	{
		$this->load->model('settingmodel');
		$userregistrationid = $this->session->userdata('registrationid');
		$seebrand = $this->input->post('type');
		$updatebrand = $this->settingmodel->updatetimeline($userregistrationid, $seebrand);
		//print_r($keepyou); exit;
		//}
		redirect($_SERVER['HTTP_REFERER']);
	}

public function updateuser_name()
	{

		$this->load->model('settingmodel');
		$userregistrationid = $this->session->userdata('registrationid');
		$name = $this->input->post('user_name');
		$updatebrand = $this->settingmodel->update_user_name($userregistrationid,$name);
		$updatebrand = $this->settingmodel->update_user_name_logintb($userregistrationid,$name);
		
	}

	public function updateuser_username(){

		$this->load->model('settingmodel');
		$userregistrationid = $this->session->userdata('registrationid');
		$name = $this->input->post('user_username');
		$updatebrand = $this->settingmodel->update_user_username($userregistrationid,$name);
		$updatebrand = $this->settingmodel->update_user_username_logintb($userregistrationid,$name);
		
	}



	public function updateuser_occupation(){

		$this->load->model('settingmodel');
		$userregistrationid = $this->session->userdata('registrationid');
		$user_occupation = $this->input->post('user_occupation');
		$updatebrand = $this->settingmodel->update_user_occupation($userregistrationid,$user_occupation);
	}

	public function updateuser_organization(){

		$this->load->model('settingmodel');
		$userregistrationid = $this->session->userdata('registrationid');
		$user_organization = $this->input->post('user_organization');
		$updatebrand = $this->settingmodel->update_user_organization($userregistrationid,$user_organization);

		}

	public  function updateuser_designation(){

		$this->load->model('settingmodel');
		$userregistrationid = $this->session->userdata('registrationid');
		$user_designation = $this->input->post('user_designation');
		$updatebrand = $this->settingmodel->update_user_designation($userregistrationid,$user_designation);

	}
	public function updateuser_gender(){

		$this->load->model('settingmodel');
		$userregistrationid = $this->session->userdata('registrationid');
		$gender = $this->input->post('gender');
		$updatebrand = $this->settingmodel->update_user_gender($userregistrationid,$gender);


	}
		
	public function updateuser_location(){

		$this->load->model('settingmodel');
		$userregistrationid = $this->session->userdata('registrationid');
		$location = $this->input->post('location');
		$updatebrand = $this->settingmodel->update_user_location($userregistrationid,$location);
	}
	
	public function updateuser_sociallink(){

		$this->load->model('settingmodel');
		$userregistrationid = $this->session->userdata('registrationid');
		$user_fblink = $this->input->post('user_fb');
		$user_linkedinlink = $this->input->post('user_linkedin');
		$user_twitterlink = $this->input->post('user_twitter');
		$updatebrand = $this->settingmodel->update_user_sociallink($userregistrationid,$user_fblink,$user_linkedinlink,$user_twitterlink);


	}
		 
		 
}
?>
