<?php
    class Editorial extends CI_Controller
    {

        public function __construct()
        {
            parent::__construct();
            $this->load->library('session');
            $this->load->model('Geteditorial');
            //$this->output->enable_profiler(True );
        }

        public function editorial(){
            $this->session->unset_userdata('inviteuserid');
            $userregistrationid = $this->session->userdata('registrationid');

            $this->load->model('getdata');
            if ($userregistrationid == '') {
                $data['products'] = $this->getdata->getall();
                //print_r($data['products']);exit;
            } else {
                $hideproduct = $this->getdata->hideproduct($userregistrationid);
                //print_r($hideproduct);
                $data['products'] = $this->getdata->removehideproduct($hideproduct);
                //print_r($data['products']); exit;
            }
            $data['avali_cat'] = $this->getdata->availcat();


            //print_r($data['avali_cat']); exit;

            $data['seo'] = $this->getdata->seo('page', 'home');
            $data['user_follow'] = $this->getdata->get_user_follow($userregistrationid);// To get user follow details
            $data['user_crown'] = $this->getdata->get_user_crown($userregistrationid);// To get user crown details
            $data['user_saveforlater'] = $this->getdata->get_saveforlater($userregistrationid);
            $data['userdata'] = $this->getdata->getuserdata($userregistrationid);
            $data['editorial']=$this->Geteditorial->all_editorial();
            //print_r($data);
            $data['all_cat'] = $this->getdata->get_all_category();//for getting all the category in the head section
            $data['heading'] = "See whats new here !";
            $this->load->view('pages/head_main.php', $data);//passing category in the head section
            $this->load->view('editorial/editorial.php');
        }

        public function editorial_subcat($editorial_category,$editorial_id){
            $this->session->unset_userdata('inviteuserid');
            $userregistrationid = $this->session->userdata('registrationid');

            $this->load->model('getdata');
            if ($userregistrationid == '') {
                $data['products'] = $this->getdata->getall();
                //print_r($data['products']);exit;
            } else {
                $hideproduct = $this->getdata->hideproduct($userregistrationid);
                //print_r($hideproduct);
                $data['products'] = $this->getdata->removehideproduct($hideproduct);
                //print_r($data['products']); exit;
            }
            $data['avali_cat'] = $this->getdata->availcat();
            $data['all_products_count'] = $this->getdata->record_count();

            //print_r($data['avali_cat']); exit;
            $data['slider_image'] = $this->getdata->slider('page', 'main');
            $data['seo'] = $this->getdata->seo('page', 'home');
            $data['user_follow'] = $this->getdata->get_user_follow($userregistrationid);// To get user follow details
            $data['user_crown'] = $this->getdata->get_user_crown($userregistrationid);// To get user crown details
            $data['user_saveforlater'] = $this->getdata->get_saveforlater($userregistrationid);
            $data['userdata'] = $this->getdata->getuserdata($userregistrationid);

            $data['editorial_cat_data']=$this->Geteditorial->all_subcat_editorial($editorial_id);

            //print_r($data);
            $data['all_cat'] = $this->getdata->get_all_category();//for getting all the category in the head section
            $data['heading'] = "See whats new here !";
            $this->load->view('pages/head_main.php', $data);//passing category in the head section
            $this->load->view('editorial/editorial_subcat.php');
        }

        public function followcurator(){
            $curator_id=$this->input->post('curator_id');
            $user_id=$this->input->post('userid');

             $check=$this->Geteditorial->follow_curator($user_id,$curator_id);

            if($check>0){
                echo "1"; //start following curator
            }
            else{
                echo "0";

            }



        }
		public function unfollowcurator(){  

						$curator_id= $this->input->post('curator_id');
						$user_id=$this->input->post('userid');
						$check=$this->Geteditorial->unfollow_curator($user_id,$curator_id);
						if($check==0){
                echo "1"; //start unfollowing curator
            }
            else{
                echo "0";

            }

				}

        public function curator($curator_id)
        {
            $this->session->unset_userdata('inviteuserid');
            $userregistrationid = $this->session->userdata('registrationid');

            $this->load->model('getdata');
            $this->load->model('Geteditorial');

            $data['avali_cat'] = $this->getdata->availcat();

            //print_r($data['avali_cat']); exit;
            $data['slider_image'] = $this->getdata->slider('page', 'main');
            $data['seo'] = $this->getdata->seo('page', 'home');
			$data['user_follow'] = $this->Geteditorial->get_user_follow($userregistrationid);// To get user follow details
            $data['user_crown'] = $this->getdata->get_user_crown($userregistrationid);// To get user crown details
            $data['user_saveforlater'] = $this->getdata->get_saveforlater($userregistrationid);
            $data['userdata'] = $this->getdata->getuserdata($userregistrationid);
            //print_r($data);
            $data['all_cat'] = $this->getdata->get_all_category();//for getting all the category in the head section
            $data['curator_detail']= $this->Geteditorial->getcurator($curator_id);
            $data['editorial_detail'] = $this->Geteditorial->curator_wise($curator_id);
            $editorialcount = count($data['editorial_detail']);

            $this->load->view('pages/head_main.php', $data);
            $this->load->view('editorial/curator.php', $data);
            $this->load->view('pages/footer.php');


        }
        public function artical(){
            $editorialid = $this->uri->segment(3, 0);
            //echo $editorialid; exit;
            $this->session->unset_userdata('inviteuserid');
            $userregistrationid = $this->session->userdata('registrationid');
            $this->load->model('getdata');
		$this->load->model('ajaxrequest');
		$this->load->model('geteditorial');
		$data['avali_cat'] = $this->getdata->availcat();
		$data['all_products_count'] = $this->getdata->record_count();
		//print_r($data['avali_cat']); exit;
		$data['slider_image'] = $this->getdata->slider('page', 'main');
		$data['seo'] = $this->getdata->seo('page', 'home');
		$data['user_follow'] = $this->getdata->get_user_follow($userregistrationid);// To get user follow details
		$data['user_crown'] = $this->getdata->get_user_crown($userregistrationid);// To get user crown details
		$data['user_saveforlater'] = $this->getdata->get_saveforlater($userregistrationid);
		$data['userdata'] = $this->getdata->getuserdata($userregistrationid);
		$data['all_cat'] = $this->getdata->get_all_category();//for getting all the category in the head section
		$data['get_editorial_info'] = $this->geteditorial->get_editorial_info($editorialid); // editorial information
		//print_r($data['get_editorial_info']); exit;
	    $curatorproductid = $data['get_editorial_info'][0]['cproduct_idfk'];
		$editorial_subcatid = $data['get_editorial_info'][0]['editorial_subcatidfk']; 
		$curator_id = $data['get_editorial_info'][0]['curator_idfk']; 
	    $data['curator_product'] = $this->geteditorial->curator_product($curatorproductid);
		$data['all_comments']=$this->ajaxrequest->all_comments_cproduct($curatorproductid);
		//print_r($data['curator_product']); exit;
		//$data['editorialsubcat_wise_product'] = $this->getdata->editorial_subcat_wiseproduct($editorial_subcatid);
		//$data['curator_wise_product'] = $this->geteditorial->curator_wise_product($curator_id);
		$data['prod_image'] = $this->geteditorial->curator_product_image($curatorproductid);
		$curator_related_product = $this->geteditorial->curator_related_product($curator_id);
		$data['curator_all_product'] = $this->geteditorial->curator_all_product($curator_related_product);
		//print_r($data['curator_all_product']); exit;
		$data['heading'] = "See whats new here !";
		$this->load->view('pages/head_main.php', $data);//passing category in the head section
		$this->load->view('editorial/editorial_detail.php');
	}
    }
?>