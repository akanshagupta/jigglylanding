<?php

class Ajaxrequest extends CI_Model
{

    /**
     * Responsable for auto load the database
     * @return void
     */
    public function __construct()
    {
        //$this->load->database();
    }

    function getcrown($productid, $personid)
    {

        $array = array('product_id=' => $productid, 'user_id =' => $personid);
        $this->db->select('*');
        $this->db->from('user_crown');
        $this->db->where($array);
        $query = $this->db->get();
        return $query->num_rows();
    }

    function get_post_crown($post_id, $personid)
    {

        $array = array('post_id=' => $post_id, 'user_id =' => $personid);
        $this->db->select('*');
        $this->db->from('user_crown');
        $this->db->where($array);
        $query = $this->db->get();
        return $query->num_rows();
    }

    function insert_crown($productid, $personid)
    {

        $crown = array('product_id' => $productid,
            'user_id' => $personid,
            'crown' => 1
        );

        //print_r($crown);

        $this->db->insert('user_crown', $crown);
        $this->db->insert_id();
        $array = array('product_id=' => $productid);
        $this->db->select('*');
        $this->db->from('user_crown');
        $this->db->where($array);
        $query = $this->db->get();
        return $query->num_rows();
    }

    function insert_post_crown($post_id, $personid)
    {

        $crown = array('post_id' => $post_id,
            'user_id' => $personid,
            'crown' => 1
        );
        $notification_array = array(
            'post_id' => $post_id,
            'user_id' => $personid,
            'post_type' => 'crown',
        );

        $this->db->insert('notifications', $notification_array);

        //print_r($crown);

        $this->db->insert('user_crown', $crown);
        $this->db->insert_id();
        $array = array('post_id=' => $post_id);
        $this->db->select('*');
        $this->db->from('user_crown');
        $this->db->where($array);
        $query = $this->db->get();
        return $query->num_rows();
    }

    function insert_post_crown_friend($post_id, $personid, $friend_id)
    {

        $crown = array('post_id' => $post_id,
            'user_id' => $personid,
            "friend_id" => $friend_id,
            'crown' => 1
        );

        //print_r($crown);

        $this->db->insert('user_crown', $crown);
        $this->db->insert_id();
        $array = array('post_id=' => $post_id);
        $this->db->select('*');
        $this->db->from('user_crown');
        $this->db->where($array);
        $query = $this->db->get();
        return $query->num_rows();

    }


    function get_category_res($productid, $featureid)
    {


        $this->db->select('specification.`specificationId`, specification.`specification_Desc`, specification.`productId_Fk`
, specification.`brandId_Fk`, specification_name.`specificationNameId`
, specification_name.`specification_Name`, specification_type.`specifictionTypeId`
, specification_type.`specifictionType_Name`');
        $this->db->from('specification');
        $this->db->join('specification_type', 'specification.specificationTypeId_Fk = specification_type.specifictionTypeId');
        // $this->db->join('sub_category','sub_category.subCategoryId = product.subcategoryId_Fk');
        $this->db->join('specification_name', 'specification.specificationNameId_Fk = specification_name.specificationNameId');
        // $this->db->group_by('specification`.`specificationTypeId_Fk');
        // $this->db->join('specification','specification.productId_Fk = product.productId');
        $this->db->where('specification.productId_Fk', $productid);
        $this->db->where('specification.specificationTypeId_Fk', $featureid);
        $query = $this->db->get();
        //print_r($query);
        return $query->result_array();

    }

    function follow_brand($userid, $brandid)
    {

        $this->db->select('*');
        $this->db->from('following');
        $this->db->where('user_id', $userid);
        $this->db->where('brand_id', $brandid);
        $query = $this->db->get();


        $rows = $query->num_rows();
        if ($rows > 0) {

            return "0";
        } else {
            $following = array('user_id' => $userid,
                'brand_id' => $brandid,
                'follow' => 1
            );
            $this->db->insert('following', $following);
            return $this->db->insert_id();
        }
    }


    function unfollow_brand($userid, $brandid)
    {

        $this->db->where('user_id', $userid);
        $this->db->where('brand_id', $brandid);
        $this->db->delete('following');
        return "0";


    }


    function follow_category($userid, $categoryid)
    {

        $this->db->select('*');
        $this->db->from('following');
        $this->db->where('user_id', $userid);
        $this->db->where('category_id', $categoryid);
        $query = $this->db->get();


        $rows = $query->num_rows();
        if ($rows > 0) {

            return "0";
        } else {
            $following = array('user_id' => $userid,
                'category_id' => $categoryid,
                'follow' => 1
            );
            $this->db->insert('following', $following);
            return $this->db->insert_id();
        }
    }


    function unfollow_category($userid, $categoryid)
    {

        $this->db->select('*');
        $this->db->from('following');
        $this->db->where('user_id', $userid);
        $this->db->where('category_id', $categoryid);
        $query = $this->db->get();


        $rows = $query->num_rows();
        if ($rows > 0) {

            $following = array('user_id' => $userid,
                'category_id' => $categoryid,
                'follow' => 1
            );

            $this->db->where('user_id', $userid);
            $this->db->where('category_id', $categoryid);
            $this->db->delete('following');
            return "0";


        }
    }

    function save_for_later($userid, $productid, $subcat_id)
    {

        $this->db->select('*');
        $this->db->from('saveforlater');
        $this->db->where('user_id', $userid);
        $this->db->where('product_id', $productid);
        $this->db->where('subcat_id', $subcat_id);
        $query = $this->db->get();
        $rows = $query->num_rows();
        if ($rows > 0) {
            return "0";
        } else {
            $following = array('user_id' => $userid,
                'product_id' => $productid,
                'subcat_id' => $subcat_id,
                'save' => 1
            );
            $this->db->insert('saveforlater', $following);
            return $this->db->insert_id();
        }


    }

    function delete_for_later($userid, $productid)
    {

        $this->db->select('*');
        $this->db->from('saveforlater');
        $this->db->where('user_id', $userid);
        $this->db->where('product_id', $productid);
        $query = $this->db->get();
        $rows = $query->num_rows();
        if ($rows > 0) {
            $following = array('user_id' => $userid,
                'product_id' => $productid,
                'follow' => 1
            );

            $this->db->where('user_id', $userid);
            $this->db->where('product_id', $productid);
            $this->db->delete('saveforlater');
            return "0";
        } else {
            $following = array('user_id' => $userid,
                'product_id' => $productid,
                'save' => 1
            );
            $this->db->insert('saveforlater', $following);
            return $this->db->insert_id();
        }


    }


    //user comment section insert

    function user_comment($userid, $productid, $comment)
    {

        $comment = array('user_id' => $userid,
            'product_id' => $productid,
            'comment' => $comment
        );
        $this->db->insert('comment', $comment);
        return $this->db->insert_id();
    }


    function user_comment_popup($userid, $post_id, $comment)
    {

        $comment = array('user_id' => $userid,
            'post_id' => $post_id,
            'comment' => $comment
        );

        $notification_array = array(
            'post_id' => $post_id,
            'user_id' => $userid,
            'post_type' => 'comment',
        );

        $this->db->insert('notifications', $notification_array);


        $this->db->insert('comment', $comment);

        return $this->db->insert_id();
    }

    function all_comments($productid)
    {

        $this->db->select('comment.date_time as date_time,comment.comment as comment,user_registration.firstname as firstname,comment.id as commentid, user_registration.profile_picture as profile');
        $this->db->from('comment');
        $this->db->join('user_registration', 'comment.user_id=user_registration.registrationid');
        //$this->db->join('user_registration','comment.user_id=user_registration.registrationid');
        $this->db->where('comment.product_id', $productid);
        $query = $this->db->get();
        return $query->result_array();
    }


    function insert_comment_crown($commentid, $userid)
    {
        $crown = array('comment_id' => $commentid,
            'user_id' => $userid,
            'crown' => 1
        );

        $this->db->insert('user_crown', $crown);
        $this->db->insert_id();
        $array = array('comment_id=' => $commentid);
        $this->db->select('*');
        $this->db->from('user_crown');
        $this->db->where($array);
        $query = $this->db->get();
        return $query->num_rows();

    }

    function get_comment_crown($commentid, $personid)
    {

        $array = array('comment_id' => $commentid, 'user_id' => $personid);
        $this->db->select('*');
        $this->db->from('user_crown');
        $this->db->where($array);
        $query = $this->db->get();
        return $query->num_rows();
    }

    //for the user post on wall
    function user_post_insert($post_data, $userid, $image, $visibility)
    {


        $user_details = array('post_data' => $post_data, 'user_id' => $userid, 'image' => $image, 'visibility' => $visibility);
        $this->db->insert('user_post', $user_details);
        $user_insert_id = $this->db->insert_id();
        return $user_insert_id;

    }


    function user_post_insert_wimg($post_data, $userid, $visibility)
    {


        $user_details = array('post_data' => $post_data, 'user_id' => $userid, 'visibility' => $visibility);
        $this->db->insert('user_post', $user_details);
        $user_insert_id = $this->db->insert_id();
        return $user_insert_id;

    }

    function user_comment_popup1($userid, $post_id, $comment)
    {

        $comment = array('user_id' => $userid,
            'post_id' => $post_id,
            'comment' => $comment
        );
        $this->db->insert('comment', $comment);
        return $this->db->insert_id();
    }

    function all_comments_popup($post_id)
    {

        $this->db->select('comment.date_time as date_time,comment.comment as comment,user_registration.firstname as firstname,comment.id as commentid,user_registration.profile_picture as picture');
        $this->db->from('comment');
        $this->db->join('user_registration', 'comment.user_id=user_registration.registrationid');
        $this->db->where('comment.post_id', $post_id);
        $query = $this->db->get();
        return $query->result_array();
    }

    function user_first_post($userid)
    {


        $user_details = array('post_data' => 'Welcome to rigalio!', 'user_id' => $userid);
        $this->db->insert('user_post', $user_details);
        $user_insert_id = $this->db->insert_id();
        return $user_insert_id;

    }

    function accept_request($user_id, $follower_id)
    {
        $data = array(
            'follow' => 1
        );

        $this->db->where('user_id', $user_id);
        $this->db->where('follower_id', $follower_id);
        $this->db->update('following', $data);
        //return $this->db->affected_rows();
    }


    function send_follow_request($myid, $friend_id)
    {


        $user_details = array('follower_id' => $friend_id, 'user_id' => $myid);
        $this->db->insert('following', $user_details);
        $user_insert_id = $this->db->insert_id();
        return $user_insert_id;


    }


    function follow_friend($myid, $friend_id)
    {


        $user_details = array('follower_id' => $friend_id, 'user_id' => $myid, 'follow' => 1);
        $this->db->insert('following', $user_details);
        $user_insert_id = $this->db->insert_id();
        return $user_insert_id;


    }


    function get_collection_data($user_id, $subcat_id)
    {

        $this->db->select('product.`productId`,product.`created_on`,product.`productSmallDiscription`,product.`product_Name`,category.category_color,category.category_icon,product.`product_image`, brand.brandId, brand.`brand_Name`, sub_category.subCategoryId, sub_category.`subCategory_Name`, category.`category_Name`, category.`categoryId`');
        $this->db->from('saveforlater');
        $this->db->join('product', 'saveforlater.product_id = product.productId');
        $this->db->join('brand', 'brand.brandId = product.brandId_Fk');
        $this->db->join('sub_category', 'sub_category.subCategoryId = product.subcategoryId_Fk');
        $this->db->join('category', 'category.categoryId = product.categoryId_Fk');
        $this->db->where('saveforlater.user_id', $user_id);
        $this->db->where('saveforlater.subcat_id', $subcat_id);

        $query = $this->db->get();
        return $query->result_array();


    }


    function collection_subcat_status($user_id, $lock_subcat)
    {

        $data = array(
            'status' => 1
        );

        $this->db->where('user_id', $user_id);
        $this->db->where('subcat_id', $lock_subcat);
        $this->db->update('saveforlater', $data);

    }

    function collection_subcat_status_lock($user_id, $lock_subcat)
    {


        $data = array(
            'status' => 0
        );

        $this->db->where('user_id', $user_id);
        $this->db->where('subcat_id', $lock_subcat);
        $this->db->update('saveforlater', $data);

    }

    function country_wise_state($countryid)
    {

        $this->db->select('*');
        $this->db->from('states');
        $this->db->where('country_id', $countryid);
        $query = $this->db->get();
        return $query->result_array();
    }

    function state_wise_city($stateid)
    {

        $this->db->select('*');
        $this->db->from('cities');
        $this->db->where('state_id', $stateid);
        $query = $this->db->get();
        return $query->result_array();
    }

 function uncrown($userid, $productid)
    {

        $this->db->where('user_id', $userid);
        $this->db->where('product_id', $productid);
        $this->db->delete('user_crown');
       // return "0";


    }

 function getcrown_news($newsid,$userid){

        $array = array('news_id=' => $newsid, 'user_id =' => $userid);
        $this->db->select('*');
        $this->db->from('user_crown');
        $this->db->where($array);
        $query = $this->db->get();
        return $query->num_rows();



    }


    function insert_crown_news($news_id, $personid)
    {

        $crown = array('news_id' => $news_id,
            'user_id' => $personid,
            'crown' => 1
        );

        //print_r($crown);

        $this->db->insert('user_crown', $crown);
        $this->db->insert_id();
        $array = array('news_id=' => $news_id);
        $this->db->select('*');
        $this->db->from('user_crown');
        $this->db->where($array);
        $query = $this->db->get();
        return $query->num_rows();
    }

function postuncrown($userid, $post_id)
    {

        $this->db->where('user_id', $userid);
        $this->db->where('post_id', $post_id);
        $this->db->delete('user_crown');
       // return "0";


    }

function uncrownnews($userid, $newsid)
    {
        $this->db->where('user_id', $userid);
        $this->db->where('news_id', $newsid);
        $this->db->delete('user_crown');
       // return "0";
    }
	
	 function getcuratorprductcrown($cpoductid, $userid)
    {
        $array = array('cproduct_id=' => $cpoductid, 'user_id =' => $userid);
        $this->db->select('*');
        $this->db->from('user_crown');
        $this->db->where($array);
        $query = $this->db->get();
        return $query->num_rows();
    }
	
	 function uncrown_cproduct($userid, $cpoductid)
    {

        $this->db->where('user_id', $userid);
        $this->db->where('cproduct_id', $cpoductid);
        $this->db->delete('user_crown');
       // return "0";

    }
	function insert_cproduct_crown($cpoductid,$userid)
    {

        $crown = array('cproduct_id' => $cpoductid,
            'user_id' => $userid,
            'crown' => 1
        );

        //print_r($crown);

        $this->db->insert('user_crown', $crown);
        $this->db->insert_id();
        $array = array('cproduct_id=' => $cpoductid);
        $this->db->select('*');
        $this->db->from('user_crown');
        $this->db->where($array);
        $query = $this->db->get();
        return $query->num_rows();
    }
	
	function user_comment_cproduct($userid, $cproductid, $comment)
    {

        $comment = array('user_id' => $userid,
            'cproduct_id' => $cproductid,
            'comment' => $comment
        );
        $this->db->insert('comment', $comment);
        return $this->db->insert_id();
    }
	
	 function all_comments_cproduct($cproductid)
    {

        $this->db->select('comment.date_time as date_time,comment.comment as comment,user_registration.firstname as firstname,comment.id as commentid, user_registration.profile_picture as profile');
        $this->db->from('comment');
        $this->db->join('user_registration', 'comment.user_id=user_registration.registrationid');
        //$this->db->join('user_registration','comment.user_id=user_registration.registrationid');
        $this->db->where('comment.cproduct_id', $cproductid);
        $query = $this->db->get();
        return $query->result_array();
    }



}
