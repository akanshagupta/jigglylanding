<div style="width: 100%; display:block; clear: both;" id="ss">
    <div style="width: 100%; display: block; clear: both;">
        <div class="main-slider-container screen-flip-slider">
            <div class=" main-slider loading-ico">
                <div class="ui grid invisible">
                    <div class="five column row ">
                        <div class="column">
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/Audi-V10-plus.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><a href="<?php echo base_url(); ?>product/r8_v10_plus/155/overview"><h4>Audi R8: Pure supercar performance with everyday functionality.</h4> </a>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/kawasaki-H2R.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><a href="<?php echo base_url(); ?>product/kawasaki_h2r/138/overview"><h4>Kawasaki H2R: A Motorcycle that has been built beyond belief.</h4> </a>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image" /> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                   <?php /*?> <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/bmw-7-series.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>The epitome of fine dining </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div><?php */?>
                                </div>
                            </div>
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/indian-motorcycles-chief-dark-horse.jpg" class='ui image'>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span> <a href="<?php echo base_url(); ?>product/indian_motorcycles_chief_dark_horse/62/overview"><h4>Indian Chief Dark Horse: A darker alter ego.</h4> </a>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/Christian-louboutin-for-sabyasachi-shoes.jpg" class='ui image'/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span> <a href="<?php echo base_url(); ?>product/sabyasachi_and_christian_louboutin/91/overview"><h4>Christian Louboutin for Sabyasachi shoes: From Paris with love.</h4> </a>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                   <?php /*?> <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/gulfstream-g650-er.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>LVMH endorses sustainable luxury</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div><?php */?>
                                </div>
                            </div>
                        </div>
                        <div class=" column">
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <img src="<?php echo base_url(); ?>content/slider/large/Paolo-sebastian.jpg" class='ui image' alt="slider_image"/>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span> <a href="<?php echo base_url(); ?>product/paolo_sebastian/152/overview"><h4>Paolo Sebastian: Eternal Elegance from Australia </h4></a>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <img src="<?php echo base_url(); ?>content/slider/large/Balvenie-50-years-old-single-cask.jpg" class='ui image' alt="slider_image"/>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span> <a href="<?php echo base_url(); ?>product/balvenie50_years_old_single_cask/132/overview"><h4>Balvenie 50 Years Old: An ode to a master distiller.</h4> </a>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                 <?php /*?>   <div class="side">
                                        <div class="content">
                                            <img src="<?php echo base_url(); ?>content/slider/large/macallan.jpg" class='ui image' alt="slider_image"/>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><h4>A rare collection of exquisite tableware</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div><?php */?>
                                </div>
                            </div>
                        </div>

                        <div class="column">
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/Sunseeker-yatch-sunseeker-155.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span> <a href="<?php echo base_url(); ?>product/sunseeker_yacht_sunseeker_155/139/overview"><h4>Sunseeker 155: The biggest behemoth come to the shores.</h4> </a>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/Harry-Winston-Histoire-de-Tourbillon-7.jpg" class='ui image' alt="slider_image" />
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span>  <a href="<?php echo base_url(); ?>product/histoire_de_tourbillon_7/156/overview"><h4>Harry Winston Histoire de Tourbillon 7: Exceptional just reached another level.</h4> </a>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                   <?php /*?> <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/samsung-88-inch.jpg" class='ui image'/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>Escape monotony for a man made paradise</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div><?php */?>
                                </div>
                            </div>
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/Bulgari-Tourbillion-berries-watch.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span>  <a href="<?php echo base_url(); ?>product/bulgari_tourbillon_berries_watch/137/overview"><h4>Berries Watch: Bulgari combines its jewellery heritage and craftsmanship to watch making.</h4></a>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/Cessna-Citation-X+.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span> <a href="<?php echo base_url(); ?>product/cessna_citation_x/154/overview"> <h4>Cessna Citation X+: Effortless performance at transonic speeds.</h4></a>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                 <?php /*?>   <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/vertu-bentley.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>A luxurious gastronomic journey</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div><?php */?>
                                </div>
                            </div>
                        </div>
                        <div class=" column hidden-sm4">
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <img src="<?php echo base_url(); ?>content/slider/large/Vertu-signature-for-bentley.jpg" class='ui image' alt="slider_image"/>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span> <a href="<?php echo base_url(); ?>product/vertu_signature_for_bentley/47/overview"><h4>Vertu Signature for Bentley: The stylish cell-phone crafted after Bentley.</h4></a>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <img src="<?php echo base_url(); ?>content/slider/large/Jean-Paul-gaultier-ss-16.jpg" class='ui image' alt="slider_image"/>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><a href="<?php echo base_url(); ?>product/jean_paul_gaultier_ss_16_couture_collection/88/overview"><h4>Jean Paul Gaultier SS 16 Collection: Jean Paul Gaultier’s tribute to Paris’s Queen of Punk.</h4></a>
                             <hr>
                             <div><img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                 <?php /*?>   <div class="side">
                                        <div class="content">
                                            <img src="<?php echo base_url(); ?>content/images/banner/large/Artboard1copy5.jpg" class='ui image' alt="slider_image"/>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><h4>Experience Michelin Star restaurants in the city of love</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div><?php */?>
                                </div>
                            </div>
                        </div>

                        <div class="column hidden-sm5">
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/The-ulyimate-Serpent.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><a href="<?php echo base_url(); ?>product/montblanc_the_ultimate_serpent/133/overview"><h4>The Ultimate Serpent: Montblanc’s one of a kind luxury writing instrument.</h4></a>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/bell_429_lwg.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span> <a href="<?php echo base_url(); ?>product/bell_429_lwg/162/overview"><h4>Bell 429 LWG: The class leader in the open skies.</h4></a>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                   <?php /*?> <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/Artboard1copy14.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>The finest wheels at Concorso d’Eleganza Villa d’Este 2016</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div><?php */?>
                                </div>
                            </div>
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/Maserati-Levante-S.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span>  <a href="<?php echo base_url(); ?>product/levante_s/149/overview"><h4>Maserati Levante S: The Italian definition of a masterful SUV.</h4> </a>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/limelight_cascade_inspiration_secret_watch.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><a href="<?php echo base_url(); ?>product/limelight_cascade_inspiration_secret_watch/134/overview"><h4>Piaget Secret Watch: Jewellery or watch? The best of both worlds</h4></a>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                   <?php /*?> <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/Artboard1copy17.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>The Maharaja’s abode</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div><?php */?>
                                </div>
                            </div>
                        </div>

                    </div>

                    <ul class="progress-buttons">
                        <li class="button active"></li>
                        <li class="button"></li>
                        <li class="button"></li>
                    </ul>
                </div> <!--/ui grid flip slider -->


            </div>
        </div>

        <!--slider for mobile -->
        <div class="main-slider-container mobile-flip-slider hidden-lg">
            <div class=" main-slider loading-ico">
                <div class="ui grid invisible">
                    <div class="two column row ">
                        <div class="column">
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/audemars_piguet_perpetual_calender.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><a href="<?php echo base_url(); ?>product/audemars-piguet-royal-oak-perpetual-calendar/58/overview"><h4>Audemars Piguet Royal Oak Perpetual Calendar: The new invention of time.</h4></a>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                   
				   <div class="side">

                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/audemars_piguet_royal_oak_offshore_chronograph.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><a href="<?php echo base_url(); ?>product/audemars-piguet-royal-oak-off-shore-chronograph/56/overview"><h4>Audemars Piguet Royal Oak Offshore Chronograph: Dynamisma and heritage from the Swiss maestro.</h4></a>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                   <?php /*?> <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/bmw-7-series.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>BMW 7 Series LI M Sport: Benchmarking luxury since 1916.</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->

                                            </div>
                                        </div>
                                    </div><?php */?>
                                </div>
                            </div>
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/bmw-7-series.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><a href="<?php echo base_url(); ?>product/bmw-7-series-750-li-m-sport/34/overview"><h4>BMW 7 Series LI M Sport: Benchmarking luxury since 1916.</h4></a>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/burj-al-arab.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span> <a href="<?php echo base_url(); ?>product/burj-al-arab-hotel/32/overview"><h4>The only 7 star hotel in the world.</h4></a>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                <?php /*?>    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/gulfstream-g650-er.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>Gulfstream 650 ER: The class leader in the skies.</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div><?php */?>
                                </div>
                            </div>
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/ducati_panigale.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><a href="<?php echo base_url(); ?>product/ducati-1198-panigale-r/31/overview"><h4>Track ready, start to finish.</h4></a>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/gulfstream-g650-er.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">

                             <span> <a href="<?php echo base_url(); ?>product/gulfstream-650-er/28/overview"><h4>Gulfstream 650 ER: The class leader in the skies.</h4></a>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div>
                                   <?php /*?> <div class="side">
                                        <div class="content">
                                            <div class="center">
                                                <img src="<?php echo base_url(); ?>content/slider/small/Artboard1copy8.jpg" class='ui image' alt="slider_image"/>
                                                <div class="banner-hover-con">
                                                    <div class="tab">
                                                        <div class="tab-in">
                             <span><h4>Escape monotony for a man made paradise</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                        </div>
                                                    </div>
                                                </div> <!--/banner-hover-con -->
                                            </div>
                                        </div>
                                    </div><?php */?>
                                </div>
                            </div>
                        </div>
                        <div class=" column">
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <img src="<?php echo base_url(); ?>content/slider/small/indian-motorcycle-chief-dark-horse.jpg" class='ui image' alt="slider_image"/>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span> <a href="<?php echo base_url(); ?>product/indian-motorcycles-chief-dark-horse/62/overview"><h4>Indian Chief Dark Horse: a darker alter ego.</h4></a>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <img src="<?php echo base_url(); ?>content/slider/small/quadski-xl.jpg" class='ui image' alt="slider_image"/>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span> <a href="<?php echo base_url(); ?>product/gibbs_quadski_xl/29/overview"> <h4>Gibbs Quadski XL: The spirit of luxury adventure.</h4></a>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                   <?php /*?> <div class="side">
                                        <div class="content">
                                            <img src="<?php echo base_url(); ?>content/slider/small/Artboard1copy11.jpg" class='ui image' alt="slider_image"/>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><h4>A luxurious gastronomic journey</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div><?php */?>
                                </div>
                            </div>
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <img src="<?php echo base_url(); ?>content/slider/small/samsung-88-inch.jpg" class='ui image' alt="slider_image"/>

                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><a href="<?php echo base_url(); ?>product/samsung_88_inches_suhd_4k_curved_smart_tv_js9500_series_9/48/overview"><h4>Samsung SUHD 4K Curved Smart TV: The brilliance of imagery in Ultra High Definition.</h4></a>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <img src="<?php echo base_url(); ?>content/slider/small/subzero-pro-88.jpg" class='ui image' alt="slider_image"/>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span> <a href="<?php echo base_url(); ?>product/sub-zero_pro_48_with_glass_door/49/overview"><h4>Sub-Zero Pro 48: A tribute to nutrition conservation.</h4></a>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                   <?php /*?> <div class="side">
                                        <div class="content">
                                            <img src="<?php echo base_url(); ?>content/slider/small/Artboard1copy14.jpg" class='ui image' alt="slider_image"/>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><h4>The finest wheels at Concorso d’Eleganza Villa d’Este 2016</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div><?php */?>
                                </div>
                            </div>
                            <div class="ui people shape">
                                <div class="sides">
                                    <div class="active side">
                                        <div class="content">
                                            <img src="<?php echo base_url(); ?>content/slider/small/taj-palace-mumbai.jpg" class='ui image' alt="slider_image"/>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span> <a href="<?php echo base_url(); ?>product/taj_palace_mumbai/17/overview"><h4>Taj Palace Mumbai: The most iconic Taj.</h4></a>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                    <div class="side">
                                        <div class="content">
                                            <img src="<?php echo base_url(); ?>content/slider/small/vertu-bentley.jpg" class='ui image' alt="slider_image"/>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span> <a href="<?php echo base_url(); ?>product/vertu_signature_for_bentley/47/overview"><h4>Vertu Signature for Bentley: The stylish cell-phone crafted after Bentley.</h4></a>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div>
                                   <?php /*?> <div class="side">
                                        <div class="content">
                                            <img src="<?php echo base_url(); ?>content/slider/small/Artboard1copy17.jpg" class='ui image' alt="slider_image"/>
                                            <div class="banner-hover-con">
                                                <div class="tab">
                                                    <div class="tab-in">
                             <span><h4>The Maharaja’s abode</h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png" alt="slider_image"/> </div> </span>
                                                    </div>
                                                </div>
                                            </div> <!--/banner-hover-con -->
                                        </div>
                                    </div><?php */?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <ul class="progress-buttons">
                        <li class="button active"></li>
                        <li class="button"></li>
                        <li class="button"></li>
                    </ul>
                </div> <!--/ui grid flip slider -->


            </div>
        </div>

        <!--slider for mobile ends -->
    </div>
</div>

<link href="<?php echo base_url(); ?>content/css/carousel.css" rel="stylesheet">

<div class="main_content">
    <div class="">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12 content_headline">
                        <h2 ng-model="heading"><?php print_r($heading); ?></h2>
                        <hr>
                    </div>
                </div> <!--/content tagline-->
                <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 wall whats_new_content" style="overflow:hidden;">
                    <div class="grid" id="masonry-grid" style="overflow-x: hidden; overflow-y: hidden; display: flex;">
                        <?php
                        $sr = 0;
                        foreach ($products as $product) {
                            ?>


                            <div class="wall-column grid-item">
                                <div class="wall-item">
                                    <div class="category <?php echo $product['category_color']; ?>">
                                        <div class="category_img" data-url="<?php echo base_url(); ?>product/<?php echo str_replace(' ','_',strtolower($product['product_Name'])); ?>/<?php echo $product['productId']; ?>/overview">

                                            <img src="<?php echo base_url(); ?><?php echo $product['product_image']; ?>" class="img-responsive">
                                            <!--<div class="hover-content-cat"> -->
                                            <div class="hover-content-cat" data-url="<?php echo base_url(); ?>product/<?php echo str_replace(" ","_",strtolower($product['product_Name'])); ?>/<?php echo $product['productId']; ?>/overview">
                                                <?php
                                                $regid = $this->session->userdata('registrationid');
                                                if($regid !='')
                                                { ?>
                                                    <div class="del-area">
                                                        <span class="hide-post"><i class="fa fa-angle-down"></i></span>
                                                        <div class="del-button hide-product" id="<?php echo $product['productId']; ?>">Hide Post </div>

                                                    </div>
                                                <?php } ?>

                                                <div class="tab">
                                                    <div class="tab-in">
                                                  <?php  $me = [];
															foreach($user_saveforlater as $saveforlater){
																
																	$me[] = $saveforlater['product_id'];
															}
															$flag=1;
															for($i=0;$i<count($me);$i++){
															if($product['productId'] ==$me[$i]){
																$flag=0;
																break;
															}
															}
															if($flag==0)
															{?>
                                                        <div class="cat-zoom-icon" style="display:none;" id="<?php echo $product['subCategoryId']; ?>"><span class="fa-plus icomoon  icon-fa-plus" id="<?php echo $product['productId']; ?>"></span> </div>
                                                        <div class="cat-plus-icon" id="<?php echo $product['subCategoryId']; ?>">
                                                            <span class="fa-minus icomoon icon-fa-minus" id="<?php echo $product['productId']; ?>"> </div>
                                                        <div class="cat-minus-icon" style="display:none;"><span class="fa-minus icomoon icon-fa-minus"></span> </div>
															<?php }
															 else{ ?>
                                                            <div class="cat-zoom-icon" id="<?php echo $product['subCategoryId']; ?>"><span class="fa-plus icomoon  icon-fa-plus" id="<?php echo $product['productId']; ?>"></span> </div>
                                                        <div class="cat-plus-icon" style="display:none;" id="<?php echo $product['subCategoryId']; ?>">
                                                            <span class="fa-minus icomoon icon-fa-minus" id="<?php echo $product['productId']; ?>"> </div>
                                                        <div class="cat-minus-icon" style="display:none;"><span class="fa-minus icomoon icon-fa-minus"></span> </div>
														<?php } ?> 

                                                        <h4><a href="<?php echo base_url(); ?>brand/<?php echo str_replace(" ","_",strtolower($product['brand_Name'])); ?>/<?php echo $product['brandId']; ?>/legacy"><?php echo $product['brand_Name']; ?></a></h4>
                                                        <a href="javascript:void(0)" class="myan follow-brand-sectn" id="<?php echo $product['brandId'];?>">
                                                            <?php
															$me = [];
															foreach($user_follow as $user_foll){
																
																	$me[] = $user_foll['brand_id'];
															}
															$flag=1;
															for($i=0;$i<count($me);$i++){
															if($product['brandId'] ==$me[$i]){
																$flag=0;
																break;
															}
															}
															if($flag==0)
															{?>
                                                            <button class="follow-brand-btn following" data-text = "Follow brand"><i class="fa fa-check"></i></button> </a>
                                                            <?php }
															else { ?>
                                                            <button class="follow-brand-btn follow" data-text = "Follow brand"><i class="fa fa-check"></i></button> </a>
															<?php } ?>

                                                    </div>
                                                    </a>
                                                </div> <!--/tab structure -->

                                            </div> <!--/hover-content-cat -->
                                        </div> </a> <!--/category_img-->
                                        <div class="category_content">
                                            <div class="relative-struct">
                                                <h5 class="hea"><a href="<?php echo base_url();?>category/<?php echo str_replace(" ","_",strtolower($product['category_Name']));?>/<?php echo $product['categoryId'];?>"><?php echo $product['category_Name'];?></a>
                                                    <div class="tooltip cat-follow-popup">
                                                    <div class="reverse-cross"> </div>
                                                        <div class="tab">
                                                            <div class="tab-cell">
                                                                <div class="img-part">  <span class="category-iconic icomoon <?php echo $product['category_color'];?> <?php echo $product['category_icon']; ?>"></span></div>
                                                                <div class="follow-con">
                                                                    <img src="<?php echo base_url();?>content/images/icons/follow-arrow.png" class="arrow">
                                                                    <h1 class=""><a href="<?php echo base_url();?>category/<?php echo str_replace(" ","-",strtolower($product['category_Name'])); ?>/<?php echo $product['categoryId'];?>"><?php echo $product['category_Name']; ?></a> </h1>
                                                                    

                                                                    <div class="follow-status" id="<?php echo $product['categoryId']; ?>">
                                                                       <?php
															$me = [];
															foreach($user_follow as $user_foll){
																
																	$me[] = $user_foll['category_id'];
															}
															$flag=1;
															for($i=0;$i<count($me);$i++){
															if($product['categoryId'] ==$me[$i]){
																$flag=0;
																break;
															}
															}
															if($flag==0)
															{?>
                                                                        <button data-text="Follow category"
                                                                                class="follow-cat-btn normal following-cat">
                                                                            <i class="fa fa-check"></i></button>
                                                                           <?php }
															else { ?>  
                                                            <button data-text="Follow category"
                                                                                class="follow-cat-btn normal follow-cat">
                                                                            <i class="fa fa-check"></i></button>
                                                                            <?php } ?>
                                                                    </div>


                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>  </h5>
                                            </div> <!--/relative-struct -->

                                            <!-- <h2>My Travel Diaries: Spellbinding Beauty Of Indonesian Archipelago Will Leave Your Jaw Dropped! </h2>-->
                                            <a href="<?php echo base_url(); ?>product/<?php echo str_replace(" ","_",strtolower($product['product_Name'])); ?>/<?php echo $product['productId']; ?>/overview"><h2><?php echo $product['productSmallDiscription']; ?></h2></a>
                                        </div> <!--/category_content-->
                                        <?php
										if($this->session->userdata('registrationid')){
                                        $count_people=[];
                                        $comment_people = [];
                                        foreach($this->getdata->crownuser_detail($product['productId']) as $count_no)
                                        {
                                            $count_people[]=$count_no;
                                        }
                                        foreach($this->getdata->commentuser_detail($product['productId']) as $countcomment_no)
                                        {
                                            $comment_people[]=$countcomment_no;
                                        }

                                        $sum_total = count($count_people) + count($comment_people);
                                        if ($sum_total != '0')
                                        {
                                            ?>

                                            <div class="hidden-status">
                                                <h6><?php
                                                    //print_r($count_people);
                                                    $m=[];
                                                    foreach($count_people as $me){

                                                        $m[]=$me['user_id'];
                                                    }

                                                    $m1=[];
                                                    foreach($comment_people as $me){

                                                        $m1[]=$me['user_id'];
                                                    }
                                                    // echo count(array_unique(array_merge($m,$m1)));
                                                    //print_r(array_merge($count_people,$comment_people));
                                                    //$array1 = array_merge ($count_people,$comment_people);
                                                    //$array2 = array_unique($array1);
                                                    //$array3 = array_keys($array2);
                                                    //print_r($array2);
                                                    echo count(array_unique(array_merge($m,$m1)));;  ?> People Reacted On This </h6>  <span class="profile-angle"><i class="fa fa-angle-down"></i></span></div>
                                        <?php  }
										}?>



                                        <div class="category_options">
                                            <table>
                                                <tr>
                                                    <td class="date-status <?php echo $product['category_color']; ?>"><p><?php $now = time(); $startDate=$product['created_on']; // or your date as well
                                                            $your_date = strtotime($startDate);
                                                            $datediff = $now - $your_date;
                                                            echo floor(($datediff/(60*60*24))+1);?> days ago</p> </td>
                                                     <?php
															$me = [];
															foreach($user_crown as $user_foll){
																
																	$me[] = $user_foll['product_id'];
															}
															$flag=1;
															for($i=0;$i<count($me);$i++){
															if($product['productId'] ==$me[$i]){
																$flag=0;
																break;
															}
															} 
															if($flag==0)
															{?>
                                                             <td class="crown-sectn active <?php echo $product['category_color']; ?>" id="<?php echo $product['productId']; ?>"><span id="crowncount"><?php foreach($this->getdata->count_crown($product['productId']) as $count_no){ echo $count_no['no'];  }?> </span> 
                                                             <?php } 
                                                           else { ?>
                                                    <td class="crown-sectn <?php echo $product['category_color']; ?>" id="<?php echo $product['productId']; ?>"><span id="crowncount"><?php foreach($this->getdata->count_crown($product['productId']) as $count_no){ echo $count_no['no'];  }?> </span> 
                                           <?php } ?>
                                                    <span class="icomoon icon-crown"> </span></td>
                                                    <td class="comment-sectn <?php echo $product['category_color']; ?>">
                                                    <a href="<?php echo base_url(); ?>product/<?php echo str_replace(" ", "_", strtolower($product['product_Name'])); ?>/<?php echo $product['productId']; ?>/overview#comment">
                                                    <span class="text"><?php foreach($this->getdata->count_comment($product['productId']) as $count_no){ echo $count_no['no'];  }?></span> <span class="icomoon icon-chat"></span> </a></td>
                                                    <td class="fwd-icon-sectn <?php echo $product['category_color']; ?>" id="fwd-id1"><span></span><span class="icomoon icon-sharing"></span> </td>
                                                </tr>
                                            </table>
                                            <div class="fwd-social-icons sec7" style="display:none;">
                                                <ul class="cat-follow-icons">
                                                    <li>
                                                        <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url();?>product/<?php echo str_replace(" ","_",strtolower($product['product_Name']));?>/<?php echo $product['productId'];?>/overview" class="fb"></a>
                                                    </li>
                                                    <li>


                                                        <a target="_blank" href="https://twitter.com/home?status=<?php echo base_url();?>product/<?php echo str_replace(" ","_",strtolower($product['product_Name']));?>/<?php echo $product['productId'];?>/overview" class="twitter" ></a>
                                                    </li>
                                                    <?php /* <li><a class="insta" href="javascript:void(0)" target="_blank"></a></li> */ ?>
                                                    <li class="show-more"><a class="more" href="javascript:void(0)"></a></li>
                                                </ul>
                                                <div class="hidden-more-icons" style="display:none;">
                                                    <ul class="follow-icons-more">
                                                        <li>

                                                            <a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo base_url();?>product/<?php echo str_replace(" ","_",strtolower($product['product_Name']));?>/<?php echo $product['productId'];?>/overview&title=Rigalio&summary=Rigalio%20content&source=<?php echo base_url();?>product/<?php echo str_replace(" ","_",strtolower($product['product_Name']));?>/<?php echo $product['productId'];?>/overview" class="linkdin"></a>
                                                        </li>
                                                        <li>
                                                            <a target="_blank" href="https://plus.google.com/share?url=<?php echo base_url();?>product/<?php echo str_replace(" ","_",strtolower($product['product_Name']));?>/<?php echo $product['productId'];?>/overview" class="gplus"></a>

                                                        </li>
                                                        <li>

                                                            <a target="_blank" href="https://pinterest.com/pin/create/button/?url=Testing&media=<?php echo base_url();?>product/<?php echo str_replace(" ","_",strtolower($product['product_Name']));?>/<?php echo $product['productId'];?>/overview&description=Testing" class="pini"></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div> <!--/inner forward icons social -->
                                           <?php /*?> <div class="write-comment" style="display: none;" id="<?php echo $product['productId']; ?>">
                                                <?php
                                                $regif = $this->session->userdata('registrationid');
                                                if(!$regif || $userdata[0]['profile_picture']=='')
                                                {
                                                    ?>
                                                    <span ><img src="<?php echo base_url();?>content/images/icons/user.png" class="img-responsive img-circle"> </span>
                                                <?php } else{ ?>
                                                    <span style="background:url(<?php echo base_url();?><?php echo $userdata[0]['profile_picture']; ?>);" class="pf-img" > </span>
                                                <?php }  ?>
                                                <span class="commentbox"><a href="javascript:void(0)"><input type="text" class="home_comment" name="home_comment" value=""  id="home_comment<?php echo $sr; ?>"></a> </span><a href="javascript:void(0)"><span class="icomoon icon-reply"></span> </a>
                                            </div> /write-comment <?php */?>
                                        </div> <!--/category_options-->

                                    </div> <!--/category-->
                                </div> <!--/wall-item -->
                            </div> <!--/wall-column -->
                            <?php $sr++ ;
                        }
                        $this->session->set_userdata('sr',$sr);
                        //$ses_var = $this->session->userdata('sr);
                        ?>
                    </div> <!--/grid ends -->
                </div>   <!--/whats_new_content -->

                <!--/column-->

                <!-- mywork place ends here-->
                <div id="total_product_count" style="display:none;"><?php print_r($all_products_count); ?></div>

                <div class="col-lg-12 nopadding nomore-tag loading-sectn" style="text-align: center"><button class="btn" id="load_more" style="background:none" data-val = "1"><img style="display: none" id="loader" src="<?php echo str_replace('index.php','',base_url()) ?>loader.gif" class="img-responsive"> </button>

                </div>
                <div id="show" style="display:none" class="nomore">Please be Patient. Luxury is currently being handcrafted.</div>
            </div> <!--/Whats_new_content-->
        </div> <!--/row-->
    </div> <!--/container-fluid-->
</div> <!--/main_content--> <!--/main_content-->
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->



<script type="text/javascript" src="<?php echo base_url(); ?>content/js/semantic.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>content/css/semantic.min.css">
<script type="text/javascript" src="<?php echo base_url(); ?>content/js/myflip.js"></script>

<script type="text/javascript">
    // jQuery
    var fixHeight = function(images){

        var w = 345;
        var h = 672;
        var W = window.innerWidth * images;
        var H = (h/w) * W;
        $('.main-slider').height(H)

    }
function issafari () {
        var ua = navigator.userAgent.toLowerCase();
        if (ua.indexOf('safari') != -1) {
            if (ua.indexOf('chrome') > -1) {
                return false;
            } else {
                return true;
            }
        }
    }
    function checkWidth() {

if (issafari() && $(window).width() > 1400) {
            $('.screen-flip-slider .ui.grid>.row').attr('class','five2 column row');
            fixHeight(.19)
            return
        }

        if ($(window).width() > 1800) {

            $('.screen-flip-slider .ui.grid>.row').attr('class','five1 column row');
            fixHeight(.19);
            $('.screen-flip-slider').show();
            $('.mobile-flip-slider').hide();
        }
        else if ($(window).width() <= 1400 && $(window).width() > 1000) {
            console.log('19');
            $('.screen-flip-slider .ui.grid>.row').attr('class','five2 column row gridabc');
            fixHeight(.19);
            $('.screen-flip-slider').show();
            $('.mobile-flip-slider').hide();
        }
        else if ($(window).width() <= 768 && $(window).width() > 568) {
            console.log('33');
            $('.screen-flip-slider .ui.grid>.row').attr('class','three1 column row gridabc');
            fixHeight(.33);
            $('.screen-flip-slider').show();
            $('.mobile-flip-slider').hide();
        }
        else if ($(window).width() <= 568 && $(window).width() > 300) {
            console.log('50');
            //$('.main-slider').removeAttr("style") ;

            //$('.screen-flip-slider .ui.grid>.row').attr('class','two column row gridabc');
            $('.screen-flip-slider').hide();
            $('.mobile-flip-slider').show();
            $('.mobile-flip-slider .ui.grid>.row').attr('class','two1 column row gridabc');
        }

        else{
            $('.screen-flip-slider .ui.grid>.row').attr('class','five column row');
            console.log('20');
            fixHeight(.20);
        }

    }
    window.onresize = checkWidth;
    checkWidth();
    var page = $("#load_more").data('val');
    var total_product_count="<?php print_r($all_products_count); ?>";
    var per_page=12;
    //alert(page);

    var total_pages=Math.ceil(parseInt(total_product_count)/parseInt(per_page));
    //alert(total_pages);
    if(page<=total_pages){
        //alert('onload');
       // getcountry(page);
    }



    $(window).scroll(function(e){

        //if ($(window).scrollTop() == $(document).height() - $(window).height()){
        if (parseInt($(window).scrollTop()) == $(document).height() - $(window).height()){
            //alert("hi");
            e.preventDefault();
            // $("#load_more").data('val');
            page = $("#load_more").data('val');
            var total_product_count="<?php print_r($all_products_count); ?>";
            var per_page=12;
            // alert(page);
            var total_pages=Math.ceil(parseInt(total_product_count)/parseInt(per_page));
            //alert(total_pages);
            if(page<=total_pages){
                getcountry(page);
            }else{
                $("#show").show();
            }
        }
    });
   
</script>
<script src="<?php echo base_url(); ?>content/js/hover.js"></script>


