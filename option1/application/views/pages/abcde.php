<?php
//print_r($userdata); exit;
?>
<div class="settings-tagline">
    <div class="col-lg-10 col-md-10 col-sm-11 col-xs-11 nopadding settings-info"> 
    <?php if($userpic[0]['profilepic_path'] == ''){ ?>
     <img src="<?php echo base_url(); ?>content/images/profile/profile_pcture.jpg" class="img-responsive"> 
    <?php } else{ ?>
     <img src="<?php echo base_url(); ?><?php echo $userpic[0]['profilepic_path'];?>" class="img-responsive"> 
     <?php } ?>
     <span class="user-name"><?php echo $getuserdataall[0]['firstname'];?> <?php echo $getuserdataall[0]['lastname'];?></span> </div>
</div>  <!--/settings-tagline -->
<!--settings page con -->
<div class="settings-pg col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding nomargin">
  <div class="container-fluid">
    <div class="row">
      <div class="settings-pg-con col-lg-6 col-md-6 col-sm-10 col-xs-12 nopadding">
        <div class="settings-tab-panel">
          <ul class="nav nav-pills">
            <li class="active"> <a href="#details" data-toggle="tab">details </a> </li>
            <li> <a href="#communication" data-toggle="tab"> communication</a> </li>
            <li> <a href="#privacy" data-toggle="tab">privacy </a> </li>
          </ul>
        </div> <!--/settings-tab-panel -->
        <div class="settings-blocks col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
         <div id="myTabContent2" class="tab-content">    
            <div class="details-tab-con tab-pane fade active in" id="details">
              <ul class="nopadding">
                <li> <label>Password</label><button class="edit">edit</button> 
                   <div class="password-info">
                     <ul class="nopadding">
                     <p id="usererror"></p>
                       <li><input type="text" id="currentpass" name="currentpass" value="" placeholder="CURRENT PASSWORD" onBlur="existpassword()" class="textboxd"></li>
                       <li class="passwrdshow"><input type="password" name="newpass" placeholder="NEW PASSWORD" id="newpass" value="" class="textboxd " /><span class="text">Show</span><input id="test2" type="checkbox" class="hiden-chck" />  </li>
                     </ul>
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                     <!--<input type="button" class="save-btn" value="save" id="savepass">-->
                     <button type="button" class="save-btn" id="savepass123">save</button>
                     </div>
                   </div> <!--/Password-info -->
                </li>
                <li> <label>Email address</label> <button class="edit">edit </button> 
                  <div class="email-settings">
                  	<h5>We already have your following email addresses </h5>
                  	<ul class="nopadding">
                  	  <li><label><?php echo $getuserdataall[0]['email'];?> </label> <button class="primary">primary</button></li>
                      <?php
					  foreach ($useremail as $emailid) 
					  {
					  ?>
                  	  <li><label><?php echo $emailid['useremail'];?></label><a href="<?php echo base_url();?>usersetting/makeprimary/<?php echo $emailid['useremailid']; ?>"> <button class="make-primary">make primary</button></a> <a href="<?php echo base_url();?>usersetting/deleteemail/<?php echo $emailid['useremailid']; ?>"><button class="remove-btn">Remove</button></a> </li>
                      <?php }?>
                  	</ul>
                  	<h4 class="add-email-click"> Add new email address</h4>
                  	<div class="add-email">
                    <input type="text" name="addemail" value="" id="addemail" placeholder="NEW EMAIL ADDRESS" class="textboxd">
                    <!--<button class="verify-btn">verify</button>
                    <p class="verify-msg" style="display: none;">Check your email. New email address is updated </p>-->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                    <button class="save-btn" id="saveemail">save</button></div>
                  	</div>

                  </div> <!--/email-settings -->
                </li>
                <li> <label>date of birth</label> <button class="edit">edit </button> 
                   <div class="dob-info">
                     <ul class="nopadding">
                       <li><input type="text" name="currentdob" id="currentdob" value="<?php echo $getuserdataall[0]['dob'];?>" class="textboxd"></li>
                       <li><input type="text" name="newdob" value="" placeholder="ENTER NEW DATE" id="newdob" class="textboxd"></li>
                     </ul>
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding"><button id="savedob" class="save-btn">save</button></div>
                   </div> <!--/dob-info -->
                </li>
                <li> <label>Phone number</label> <button class="edit">edit </button>
                  <div class="phone-info">
                  <?php
				  if($getuserdataall[0]['phone'] == '')
				  {
				   ?>
                   <h5>Please Add Your Phone Number</h5>
                   <?php
				  }else {
				    ?>
                     <h5>We already have your following phone numbers </h5>
                  	<ul class="nopadding">
                  	  <li><label>+91 <?php echo $getuserdataall[0]['phone'];?></label> <button class="primary">primary</button></li>
                      <?php
					  foreach ($userphoneno as $phoneno) 
					  {
					  ?>
                  	  <li><label>+91 <?php echo $phoneno['phoneno'];?> </label><a href="<?php echo base_url();?>usersetting/makeprimaryphone/<?php echo $phoneno['phoneid']; ?>"> <button class="make-primary">make primary</button></a> <a href="<?php echo base_url();?>usersetting/deletephoneno/<?php echo $phoneno['phoneid']; ?>"><button class="remove-btn">Remove</button> </a></li>
                       <?php }?>
                  	</ul>
                    <?php } ?>
                  	<h4 class="add-ph-click"> Add new Phone no.</h4>
                  	<div class="add-ph">
                  	  <input type="text" name="addphone" id="addphone" value="" placeholder="NEW PHONE NO." class="textboxd">
                     <!-- <button class="verify-btn">verify</button>-->
                  	  <!--<p class="verify-ph" style="display: none;">Check your phone. New phone no. is updated </p>-->
                  	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                    <button class="save-btn" id="savephone">save</button>
                    </div>
                  </div> <!--/phone-info --> 
                </li>
              </ul>
            </div> <!--/details-tab-con -->
            <div class="comm-tab-con tab-pane" id="communication">
              <ul>
                <li> <label>Frequency of information</label><button class="edit">edit </button> 
                  <div class="freq-info">
                    <ul class="nopadding">
		                <li><input id="radio1" type="radio" name="info" value="Weekly" <?php echo ($getuserdataall[0]['reminder_type']=='Weekly')?"checked":"" ;?> ><label for="radio1">Weekly</label></li>
		                <li><input id="radio2" type="radio" name="info" value="Fortnightly" <?php echo ($getuserdataall[0]['reminder_type']=='Fortnightly')?"checked":"" ;?>><label for="radio2">Fortnightly</label></li>
		                <li><input id="radio3" type="radio" name="info" value="Monthly" <?php echo ($getuserdataall[0]['reminder_type']=='Monthly')?"checked":"" ;?> ><label for="radio1">Monthly</label></li>
                       
		            </ul>
		            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding"><button class="save-btn" id="saveinfo">save</button> <span class="alert-msg">You can choose any one of the option </span> </div>
                  </div> <!--/freq-info-inner -->
                </li>
                <li> <label>mode of communication </label> <button class="edit">edit </button> 
                   <div class="mode-comm">
                      <ul class="nopadding">
		                <li><input id="check8" type="checkbox" name="keepyou[]" value="Send me information via text messages"                        <?php 
						$exp_val=explode(',',$getuserdataall[0]['contact_type']);
						for($i=0;$i<count($exp_val);$i++){
						if ($exp_val[$i]=='Send me information via text messages') {echo "checked"; } }?>>
                    <label for="check8">Send me information via text messages </label></li>
                 	    <li><input id="check9" type="checkbox" name="keepyou[]" value="Call me to update" 
						<?php
						//$exp_val=explode(',',$userdata[0]['contact_type']);
						for($i=0;$i<count($exp_val);$i++){
						if ($exp_val[$i]=='Call me to update') { echo "checked";} }?>><label for="check9">Call me to update </label></li>
                 	    <li><input id="check10" type="checkbox" name="keepyou[]" value="E-mail me the information" <?php for($i=0;$i<count($exp_val);$i++){ if ($exp_val[$i]=='E-mail me the information'){ echo "checked"; }}?>><label for="check10">E-mail me the information </label></li>
		            </ul>
		            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding"><button class="save-btn" id="savecommunication">save</button> <span class="alert-msg">You can choose any one of the option </span> </div>
                  </div> <!--/mode-comm-inner -->
                </li>
              </ul>
            </div> <!--/communication-tab-con -->
            <div class="privacy-tab-con tab-pane" id="privacy">
              <ul>
                <li> <label>profile visibility</label> <button class="edit">edit </button> 
                  <div class="profile-visibile">
                     <h5>Who can see your profile on Rigalio </h5>
                     <ul class="nopadding">
		                <li><input id="radio1" type="radio" name="seeprofile" value="0" checked><label for="radio1">private</label></li>
		                <li><input id="radio2" type="radio" name="seeprofile" value="1"><label for="radio2">members</label></li>
		             </ul>
                   </div> <!--/profile-visibile -->
                </li>
                <li> <label>crowns</label> <button class="edit">edit </button>
                  <div class="crown-info">
                     <h5>Who can see your crowns on Rigalio </h5>
                     <?php
					 if(count($usercrown)==0){
					  ?>
                      <ul class="nopadding">
		                <li><input id="radio1" type="radio" name="seecrown" value="0" class="radio1" checked><label for="radio1">Only me</label></li>
		                <li><input id="radio2" type="radio" name="seecrown" value="1" class="radio1"><label for="radio2">Followers</label></li>
		                <li><input id="radio3" type="radio" name="seecrown" value="2" class="radio1"><label for="radio1">Members</label></li>
		             </ul>
                      <?php } else{ ?>
                     <ul class="nopadding">
		                <li><input id="radio1" type="radio" name="seecrown" value="0" <?php echo ($usercrown[0]['isActive']=='0')?"checked":"" ;?> class="radio1"><label for="radio1">Only me</label></li>
		                <li><input id="radio2" type="radio" name="seecrown" value="1" <?php echo ($usercrown[0]['isActive']=='1')?"checked":"" ;?> class="radio1"><label for="radio2">Followers</label></li>
		                <li><input id="radio3" type="radio" name="seecrown" value="2" <?php echo ($usercrown[0]['isActive']=='2')?"checked":"" ;?> class="radio1"><label for="radio1">Members</label></li>
		             </ul>
                      <?php }?>
                   </div>
                </li>
                <li> <label>comments</label> <button class="edit">edit </button> 
                  <div class="comments-setting">
                     <h5>Who can see your comments on Rigalio </h5>
                     <?php
					 if(count($usercomment)==0){
					  ?>
                      <ul class="nopadding">
		                <li><input id="radio1" type="radio" name="seecomment" value="0" class="radio1" checked><label for="radio1">Only me</label></li>
		                <li><input id="radio2" type="radio" name="seecomment" value="1" class="radio1"><label for="radio2">Followers</label></li>
		                <li><input id="radio3" type="radio" name="seecomment" value="2" class="radio1"><label for="radio1">Members</label></li>
		             </ul>
                     <?php } else{ ?>
                     <ul class="nopadding">
		                <li><input id="radio1" type="radio" name="seecomment" value="0" <?php echo ($usercomment[0]['is_active']=='0')?"checked":"" ;?> class="radio1"><label for="radio1">Only me</label></li>
		                <li><input id="radio2" type="radio" name="seecomment" value="1" <?php echo ($usercomment[0]['is_active']=='1')?"checked":"" ;?> class="radio1"><label for="radio2">Followers</label></li>
		                <li><input id="radio3" type="radio" name="seecomment" value="2" <?php echo ($usercomment[0]['is_active']=='2')?"checked":"" ;?> class="radio1"><label for="radio1">Members</label></li>
		             </ul>
                     <?php }?>
                   </div> <!-- /comments-setting-->
                </li>
                <li> <label>share</label> <button class="edit">edit </button>
                  <div class="share-setting">
                     <h5>Who can see your share on Rigalio </h5>
                     <ul class="nopadding">
		                <li><input id="radio1" type="radio" name="radio1" value="Item 1" class="radio1"><label for="radio1">Only me</label></li>
		                <li><input id="radio2" type="radio" name="radio2" value="Item 2" class="radio1"><label for="radio2">Followers</label></li>
		                <li><input id="radio3" type="radio" name="radio3" value="Item 3" class="radio1"><label for="radio1">Members</label></li>
		             </ul>
                   </div> <!-- /share-setting--> 
                </li>
                <li> <label>status</label> <button class="edit">edit </button>
                <div class="status-setting">
                     <h5>Who can see your status on Rigalio </h5>
                     <?php
					 if(count($userpost)==0){
					  ?>
                      <ul class="nopadding">
		                <li><input id="radio1" type="radio" name="seestatus" value="0" class="radio1" checked><label for="radio1">Only me</label></li>
		                <li><input id="radio2" type="radio" name="seestatus" value="1" class="radio1"><label for="radio2">Followers</label></li>
		                <li><input id="radio3" type="radio" name="seestatus" value="2" class="radio1"><label for="radio1">Members</label></li>
		             </ul>
                      <?php } else{ ?>
                     <ul class="nopadding">
		                <li><input id="radio1" type="radio" name="seestatus" value="0" <?php echo ($userpost[0]['is_active']=='0')?"checked":"" ;?> class="radio1"><label for="radio1">Only me</label></li>
		                <li><input id="radio2" type="radio" name="seestatus" value="1" <?php echo ($userpost[0]['is_active']=='1')?"checked":"" ;?> class="radio1"><label for="radio2">Followers</label></li>
		                <li><input id="radio3" type="radio" name="seestatus" value="2" <?php echo ($userpost[0]['is_active']=='2')?"checked":"" ;?> class="radio1"><label for="radio1">Members</label></li>
		             </ul>
                      <?php }?>
                   </div> <!-- /status-setting-->  
                </li>
                <li> <label>photos</label><button class="edit">edit </button> 
                <div class="photos-setting">
                     <h5>Who can see your photos on Rigalio </h5>
                     <?php
					 if(count($getuserpostimage)==0){
					  ?>
                       <ul class="nopadding">
		                <li><input id="radio1" type="radio" name="seephotos" value="0" class="radio1" checked><label for="radio1">Only me</label></li>
		                <li><input id="radio2" type="radio" name="seephotos" value="1" class="radio1"><label for="radio2">Followers</label></li>
		                <li><input id="radio3" type="radio" name="seephotos" value="2" class="radio1"><label for="radio1">Members</label></li>
		             </ul>
                      <?php } else{ ?>
                     <ul class="nopadding">
		                <li><input id="radio1" type="radio" name="seephotos" value="0" <?php echo ($getuserpostimage[0]['is_active']=='0')?"checked":"" ;?> class="radio1"><label for="radio1">Only me</label></li>
		                <li><input id="radio2" type="radio" name="seephotos" value="1" <?php echo ($getuserpostimage[0]['is_active']=='1')?"checked":"" ;?> class="radio1"><label for="radio2">Followers</label></li>
		                <li><input id="radio3" type="radio" name="seephotos" value="2" <?php echo ($getuserpostimage[0]['is_active']=='2')?"checked":"" ;?> class="radio1"><label for="radio1">Members</label></li>
		             </ul>
                     <?php }?>
                   </div> <!-- /photos-setting-->
                </li>
                <li> <label>people</label><button class="edit">edit </button> 
                  <div class="ppl-fol-setting">
                     <h5>Who can see your people following and followers on Rigalio </h5>
                      <?php
					 if(count($getfollower)==0){
					  ?>
                      <ul class="nopadding">
                    <li><input id="radio1" type="radio" name="seefollower" value="0" class="radio1" checked><label for="radio1">Only me</label></li>
                    <li><input id="radio2" type="radio" name="seefollower" value="1" class="radio1"><label for="radio2">Followers</label></li>
                    <li><input id="radio3" type="radio" name="seefollower" value="2" class="radio1"><label for="radio1">Members</label></li>
                 </ul>
                  <?php } else{ ?>
                     <ul class="nopadding">
                    <li><input id="radio1" type="radio" name="seefollower" value="0" <?php echo ($getfollower[0]['is_active']=='0')?"checked":"" ;?> class="radio1"><label for="radio1">Only me</label></li>
                    <li><input id="radio2" type="radio" name="seefollower" value="1" <?php echo ($getfollower[0]['is_active']=='1')?"checked":"" ;?> class="radio1"><label for="radio2">Followers</label></li>
                    <li><input id="radio3" type="radio" name="seefollower" value="2" <?php echo ($getfollower[0]['is_active']=='2')?"checked":"" ;?> class="radio1"><label for="radio1">Members</label></li>
                 </ul>
                  <?php }?>
                   </div> <!-- /people follow-setting-->
                </li>
                <li><label>Brand</label> <button class="edit">edit </button>
                 <div class="brandfol-setting">
                     <h5>Who can see brand following and followers on Rigalio </h5>
                     <ul class="nopadding">
                    <li><input id="radio1" type="radio" name="seebrand" value="0" class="radio1"><label for="radio1">Only me</label></li>
                    <li><input id="radio2" type="radio" name="seebrand" value="1" class="radio1"><label for="radio2">Followers</label></li>
                    <li><input id="radio3" type="radio" name="seebrand" value="2" class="radio1"><label for="radio1">Members</label></li>
                 </ul>
                   </div> <!-- /brandfolowing-setting--> 
                </li>
              </ul>
            </div> <!--/privacy-tab-con -->
         </div>   
        </div> <!--/settings-blocks -->
          
      </div> <!--/settings-pg-con -->
    </div>
  </div>  
</div>
<!--settings-pg-con ends -->
  </body>
<script>
// date picker script
$(function() {
 //$( "#datepicker2" ).datepicker();
});

// code for who can see your profile 
$('input:radio[name=seeprofile]').click(function(){
    profile()
});

function profile()
{
	var seeprofile= $('input[name=seeprofile]:checked').val();
	alert(seeprofile); 
	var data={
			  "seeprofile":seeprofile
			  };
	  $.ajax({
			 type: "POST",
			 url: ""+base_url+"usersetting/userpassexist",
			 // url: "http://localhost/finalrigalio/main/inviteuserlogin?username=" + username + "&password=" + password,
		      data:data,
				//crossDomain:true,
				success: function(html){
					//alert(html);
				var inviteuser=html; 
				if(inviteuser==0){
					//alert(html);
					document.getElementById('usererror').innerHTML= "Password Not matched!!";
					$("#usererror").html("Password Not matched!!");
					}
					else{
							//$("#usererror").html("Password Not matched!!");
							$("#usererror").html("");
						}
		   }
		});
    
}

// code for who can see your crown 
$('input:radio[name=seecrown]').click(function(){
    crown()
});
function crown()
{
	var seecrown= $('input[name=seecrown]:checked').val();
	//alert(seecrown);
	var data={
			  "seecrown":seecrown
			  }; 
	 $.ajax({
			 type: "POST",
			 url: ""+base_url+"usersetting/canseecrown",
			 // url: "http://localhost/finalrigalio/main/inviteuserlogin?username=" + username + "&password=" + password,
		      data:data,
				//crossDomain:true,
				success: function(html){
					//alert("thank you");
					window.location.href='http://rigalio.com/x/no_access/usersetting/setting';
		   }
		});
}

// code for who can see your comment 
$('input:radio[name=seecomment]').click(function(){
    comment()
});
function comment()
{
	var seecomment= $('input[name=seecomment]:checked').val();
	//alert(seecomment);
	var data={
			  "seecomment":seecomment
			  }; 
	 $.ajax({
			 type: "POST",
			 url: ""+base_url+"usersetting/canseecomment",
			 // url: "http://localhost/finalrigalio/main/inviteuserlogin?username=" + username + "&password=" + password,
		      data:data,
				//crossDomain:true,
				success: function(html){
					//alert("thank you");
					window.location.href='http://rigalio.com/x/no_access/usersetting/setting';
		   }
		});
}

// code for who can see your status 
$('input:radio[name=seestatus]').click(function(){
    status()
});
function status()
{
	var seestatus= $('input[name=seestatus]:checked').val();
	//alert(seestatus);
	var data={
			  "seestatus":seestatus
			  }; 
	 $.ajax({
			 type: "POST",
			 url: ""+base_url+"usersetting/canseestatus",
			 // url: "http://localhost/finalrigalio/main/inviteuserlogin?username=" + username + "&password=" + password,
		      data:data,
				//crossDomain:true,
				success: function(html){
					//alert(html);
					window.location.href='http://rigalio.com/x/no_access/usersetting/setting';
		   }
		});
}
// code for who can see your photos 
$('input:radio[name=seephotos]').click(function(){
    photos()
});
function photos()
{
	var seephotos= $('input[name=seephotos]:checked').val();
	alert(seephotos);
	var data={
			  "seephotos":seephotos
			  }; 
	 $.ajax({
			 type: "POST",
			 url: ""+base_url+"usersetting/canseephotos",
			 // url: "http://localhost/finalrigalio/main/inviteuserlogin?username=" + username + "&password=" + password,
		      data:data,
				//crossDomain:true,
				success: function(html){
				    //alert(html);
					window.location.href='http://rigalio.com/x/no_access/usersetting/setting';
		   }
		});
}
// code for who can see your followers 
$('input:radio[name=seefollower]').click(function(){
    follower()
});
function follower()
{
	var seefollower= $('input[name=seefollower]:checked').val();
	alert(seefollower);
	var data={
			  "seefollower":seefollower
			  }; 
	 $.ajax({
			 type: "POST",
			 url: ""+base_url+"usersetting/canseefollower",
			 // url: "http://localhost/finalrigalio/main/inviteuserlogin?username=" + username + "&password=" + password,
		      data:data,
				//crossDomain:true,
				success: function(html){
				   // alert(html);
					window.location.href='http://rigalio.com/x/no_access/usersetting/setting';
		   }
		});
}

//check password exist or not
function existpassword() {
	var currentpass =$("#currentpass").val();
	var data={
			  "currentpass":currentpass
			  };
    $.ajax({
			 type: "POST",
			 url: ""+base_url+"usersetting/userpassexist",
			 // url: "http://localhost/finalrigalio/main/inviteuserlogin?username=" + username + "&password=" + password,
		      data:data,
				//crossDomain:true,
				success: function(html){
					//alert(html);
				var inviteuser=html; 
				if(inviteuser==0){
					//alert(html);
					document.getElementById('usererror').innerHTML= "Password Not matched!!";
					$("#usererror").html("Password Not matched!!");
					}
					else{
							//$("#usererror").html("Password Not matched!!");
							$("#usererror").html("");
						}
		   }
		}); 
}

 $(document).ready(function(){
	 // save new password 
	  $("#savepass123").click(function(){
		  var newpass =$("#newpass").val();
		  var currentpass =$("#currentpass").val();
		 // var dob =$("#datepicker1").val();
		 // alert(newpass);
	  if(currentpass== '')
		{
		alert("Please Enter Current Password");
		return false;
		}
	  
      if(newpass== '')
		{
		alert("Please Enter New Password");
		return false;
		}
	else{
		var data={
			  newpass : newpass
		};
		$.ajax({
			 type: "POST",
			 url: ""+base_url+"usersetting/passwordchange",
			 // url: "http://localhost/finalrigalio/main/inviteuserlogin?username=" + username + "&password=" + password,
		      data:data,
			success: function(html){
			 alert("successfully");
			 //alert(html);
			//window.location.href='http://localhost/finalrigalio/main/profile';
		   }
		});   
	}
		  });	
		  
// change DOB
 $("#savedob").click(function(){
		  var newdob =$("#newdob").val();
		 // var currentpass =$("#currentpass").val();
		 //alert(newdob);
	  if(newdob== '')
		{
		alert("Please Enter Current DOB");
		return false;
		}
      
	else{
		var data={
			  newdob : newdob
		};
		$.ajax({
			 type: "POST",
			 url: ""+base_url+"usersetting/dobchange",
			 // url: "http://localhost/finalrigalio/main/inviteuserlogin?username=" + username + "&password=" + password,
		      data:data,
			success: function(html){
			 alert("successfully");
			 window.location.href='http://rigalio.com/x/no_access/usersetting/setting';
			 //alert(html);
			//window.location.href='http://localhost/finalrigalio/main/profile';
		   }
		});   
		
	}
		  });	
// add phone no.
 $("#savephone").click(function(){
		  var addphone =$("#addphone").val();
		 // var currentpass =$("#currentpass").val();
		 //alert(addphone);
	  if(addphone== '')
		{
		alert("Please Enter Your Phone No.");
		return false;
		}
	else{
		var data={
			  addphone : addphone
		};
		$.ajax({
			 type: "POST",
			 url: ""+base_url+"usersetting/addphoneno",
			 // url: "http://localhost/finalrigalio/main/inviteuserlogin?username=" + username + "&password=" + password,
		      data:data,
			success: function(html){
			 alert("successfully");
			 window.location.href='http://rigalio.com/x/no_access/usersetting/setting';
			 //alert(html);
			//window.location.href='http://localhost/finalrigalio/main/profile';
		   }
		});   
		
	}
		  });

// add E-mail id 
 $("#saveemail").click(function(){
		  var addemail =$("#addemail").val();
		 // var currentpass =$("#currentpass").val();
		//alert(addemail);
	  if(addemail== '')
		{
		alert("Please Enter Your Email");
		return false;
		}
      
	else{
		var data={
			  addemail : addemail
		};
		$.ajax({
			 type: "POST",
			 url: ""+base_url+"usersetting/addemail",
			 // url: "http://localhost/finalrigalio/main/inviteuserlogin?username=" + username + "&password=" + password,
		      data:data,
			success: function(html){
			 alert("successfully");
			 window.location.href='http://rigalio.com/x/no_access/usersetting/setting';
			 //alert(html);
			//window.location.href='http://localhost/finalrigalio/main/profile';
		   }
		});   
		
	}
		  });

//update information
$("#saveinfo").click(function(){
	  
	var updatedinfo= $('input[name=info]:checked').val();
	//alert(currentpass);
	  //alert(updatedinfo);
	  if(updatedinfo== '')
		{
		alert("Please Enter Your Frequency Of Information");
		return false;
		}
	else
	{
		var data={
			  updatedinfo : updatedinfo
		};
		$.ajax({
			 type: "POST",
			 url: ""+base_url+"usersetting/updateinfo",
			 // url: "http://localhost/finalrigalio/main/inviteuserlogin?username=" + username + "&password=" + password,
		      data:data,
			success: function(html){
			// alert("successfully");
			 window.location.href='http://rigalio.com/x/no_access/usersetting/setting';
		   }
		});   
		
	}
		  });
		  
//update mode of communication 
$("#savecommunication").click(function(){
	var keepyou = new Array();
	var keepyouElems = document.getElementsByName("keepyou[]");
	var keepyoucount = 0; 
	for (var i=0; i<keepyouElems.length; i++) 
	   {       
		  if (keepyouElems[i].type == "checkbox" && keepyouElems[i].checked == true) 
		  {
			  keepyoucount++;
			  keepyou.push(keepyouElems[i].value);
		  }
	  }
	 // alert(keepyou);
	  if(keepyoucount < 1 )
		{
		//document.getElementById('msg1').innerHTML="Please select atleast 1 Keep you";
		alert("Please select atleast 1 Keep you");
		return false;
		}
	  else
	{
		$.ajax({
			 type: "POST",
			// url: ""+base_url+"usersetting/updateinfo",
			 url: "http://rigalio.com/x/no_access/usersetting/updatecommunication?keepyou=" + keepyou,
		     // data:data,
			success: function(html){
				//var me = html
			 //alert(html);
			 window.location.href='http://rigalio.com/x/no_access/usersetting/setting';
		   }
		});   
	}
		  });
		  
	 })
// script for the adding new email address on settings page
$(function() {
   $('.add-email-click, .add-ph-click').click(function() {
     $('.add-email, .add-ph').slideToggle("slow");
   });
});
  
	 
</script>
<!--<script>
$.toggleShowPassword({
   field: '#test1',
   control: '#test2'
});
</script>-->


</html>