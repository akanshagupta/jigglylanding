<?php
//print_r($brand_details); exit;
$brand_id = $brand;
//print_r($brand); exit;
//print_r($brand_id); exit;
?>

<div class="brand-pg-banner col-lg-12 nopadding">
  <span class="brand-bg-banner" style="background:url(<?php echo base_url().$brand_details[0]['cover_image']; ?>);"> </span>
  
  <div class="col-lg-10 col-sm-11 col-xs-12 brand-divison nopadding">
    <div class="brand-img">
    <span style="background:url(<?php echo base_url() . $brand_details[0]['brand_image']; ?>) no-repeat top center;"> </span>
    </div>
  </div>
  
</div>  


<div class="brand_pg_tagline">
  <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 brand-logo-sec">
     <div class="col-lg-6 col-sm-12 nopadding">
       <span class="brand-intro"><?php echo $brand_details[0]['brand_Name']; ?></span>
     </div>
     <div class="col-lg-6 col-sm-12 nopadding inner-up">
       <span class="followers"><img src="<?php echo base_url(); ?>content/images/icons/followers-icon.png"> <?php foreach($this->getdata->count_brandfollower($brand_details[0]['brandId']) as $count_no){
					if($count_no['no'] != ''){
					 echo $count_no['no']; 
					}
					else
					{
						echo '0';
					}
					 }?> </span>
       <span class="brand-follow up-brand" id="<?php echo $brand_details[0]['brandId'];?>">
                    <?php
															$me = [];
															foreach($user_follow as $user_foll){
																
																	$me[] = $user_foll['brand_id'];
															}
															$flag=1;
															for($i=0;$i<count($me);$i++){
															if($brand_details[0]['brandId'] ==$me[$i]){
																$flag=0;
																break;
															}
															}
															if($flag==0)
															{?>
                                                            <button class="follow-brand-btn following" data-text = "Follow brand"><i class="fa fa-check"></i></button>
                                                            <?php }
															else { ?>
                    <button class="follow-brand-btn follow" data-text="Follow brand"><i class="fa fa-check"></i></button>
     				<?php } ?>
      </span>
     </div>
     
  </div>      
</div> <!--/brand_pg_tagline -->

<div class="brand_pg_tagline" style="display:none;">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 content_headline">
            <div class="brand-info">
                <div class="brand-img col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <img src="<?php echo base_url() . $brand_details[0]['brand_image']; ?>" class="img-responsive">
                </div> <!--/ brand-img -->
                <!--<div class="brand-name col-lg-7 col-md-7 col-sm-6 col-xs-12">
              <h2><?php echo $brand_details[0]['brand_Name']; ?> </h2>
           </div> --> <!--/brand-name -->
            </div> <!--brand-info -->
            <div class="product-info col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <ul>
                    <li><label>Brand <span class="brand-name"><?php echo $brand_details[0]['brand_Name']; ?>
                                <div class="tooltip cat-follow-pqopup">
                                    <div class="tab">
                                        <div class="tab-cell">
                                            <div class="img-part"><div class="img-partdiv"><span style="background:url(<?php echo base_url(); ?><?php echo $brand_details[0]['brand_image']; ?>);"></span></div></span>
                                            </div>
                                            <div class="follow-con">
                                                <img
                                                    src="<?php echo base_url(); ?>content/images/icons/follow-arrow.png"
                                                    class="arrow">
                                                <h1 class=""><a
                                                        href="<?php echo base_url(); ?>brand/<?php echo str_replace(" ", "-", strtolower($brand_details[0]['brand_Name'])); ?>/<?php echo $brand_details[0]['brandId']; ?>/legacy"><?php echo $brand_details[0]['brand_Name']; ?></a>
                                                </h1>
                                                <div class="myan follow-status"
                                                     id="<?php echo $brand_details[0]['brandId']; ?>">
                                                     <?php
															$me = [];
															foreach($user_follow as $user_foll){
																
																	$me[] = $user_foll['brand_id'];
															}
															$flag=1;
															for($i=0;$i<count($me);$i++){
															if($brand_details[0]['brandId'] ==$me[$i]){
																$flag=0;
																break;
															}
															}
															if($flag==0)
															{?>
                                                            <button class="follow-brand-btn following" data-text = "Follow brand"><i class="fa fa-check"></i></button>
                                                            <?php }
															else { ?>
                                                    <button class="follow-brand-btn follow" data-text="Follow brand"><i
                                                            class="fa fa-check"></i></button>
                                                            <?php } ?>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
             </span> </label></li>
                    <li><label>Established<span><?php echo $brand_details[0]['Establish']; ?> </span> </label></li>
                </ul>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 brand-follow">
                    <button class="follow-cat-btn normal brand-page-follow"
                            id="<?php echo $brand_details[0]['brandId']; ?>">follow brand
                    </button>
                </div>
            </div> <!--/product-info -->
        </div>
    </div>
</div> <!--/brand_pg_tagline -->
<div class="basic-cat-info brand-pg bg-white col-lg-12 col-md-12 col-sm-12 col-xs-12" style="display:none;">
    <div class="basic-cat-info-inner col-lg-10 col-md-10 col-sm-12 col-xs-12">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 follow-list">
            <table>
                <tr>
                    <td class="fwd-icon-sectn"><span> </span><span class="icomoon icon-sharing"></span></td>
                    <td><span><?php foreach ($this->getdata->count_brandfollower($brand_details[0]['brandId']) as $count_no) {
                                if ($count_no['no'] != '') {
                                    echo $count_no['no'];
                                } else {
                                    echo '0';
                                }
                            } ?>  </span> <span><img
                                src="<?php echo base_url(); ?>content/images/icons/followers-icon.png"> </span></td>
                </tr>
            </table>
            <div class="fwd-social-icons sec7" style="display:none;">
                <ul class="cat-follow-icons">
                    <li>
                        <a target="_blank"
                           href="https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url(); ?>brand/<?php echo str_replace(" ", "-", strtolower($brand_details[0]['brand_Name'])); ?>/<?php echo $brand_details[0]['brandId']; ?>"
                           class="fb"></a>
                    </li>
                    <li>

                        <a target="_blank"
                           href="https://twitter.com/home?status=<?php echo base_url(); ?>brand/<?php echo str_replace(" ", "-", strtolower($brand_details[0]['brand_Name'])); ?>/<?php echo $brand_details[0]['brandId']; ?>"
                           class="twitter"></a>
                    </li>
                    <?php /*?> <li><a class="insta" href="javascript:void(0)" target="_blank"></a></li><?php */ ?>
                    <li class="show-more"><a class="more" href="javascript:void(0)"></a></li>
                </ul>
                <div class="hidden-more-icons" style="display:none;">
                    <ul class="follow-icons-more">
                        <li>

                            <a target="_blank"
                               href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo base_url(); ?>brand/<?php echo str_replace(" ", "_", strtolower($brand_details[0]['brand_Name'])); ?>/<?php echo $brand_details[0]['brandId']; ?>&title=Rigalio&summary=Rigalio%20content&source=<?php echo base_url(); ?>brand/<?php echo str_replace(" ", "_", strtolower($brand_details[0]['brand_Name'])); ?>/<?php echo $brand_details[0]['brandId']; ?>"
                               class="linkdin"></a>
                        </li>
                        <li>
                            <a target="_blank"
                               href="https://plus.google.com/share?url=<?php echo base_url(); ?>brand/<?php echo str_replace(" ", "-", strtolower($brand_details[0]['brand_Name'])); ?>/<?php echo $brand_details[0]['brandId']; ?>"
                               class="gplus"></a>

                        </li>
                        <li>

                            <a target="_blank"
                               href="https://pinterest.com/pin/create/button/?url=Testing&media=<?php echo base_url(); ?>brand/<?php echo str_replace(" ", "-", strtolower($brand_details[0]['brand_Name'])); ?>/<?php echo $brand_details[0]['brandId']; ?>&description=Testing" class="pini"></a>
                        </li>
                    </ul>
                </div>
            </div> <!--/inner forward icons social -->
        </div>

    </div>  <!--/basic-cat-info-inner -->
</div>  <!--/basic-cat-info -->
<!--category-pg-con -->
<div class="category-pg-con brand-pg-con col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
    <div class="container-fluid nomargin nopadding">
        <div class="row">
 <div class="upbrand-bg col-lg-12 nopadding">
            <div class="brand-tab-panel col-lg-10 col-md-10 col-sm-11 col-xs-12">
                <ul class="nav nav-pills">
                    <?php if (($management_details) != '' && ($brand_details[0]['legacy']) != '') { ?>
                        <li class="<?php if($this->uri->segment(4, 0)=="legacy"){  echo "active"; } ?>"><a href="<?php echo base_url()."brand/".str_replace(" ", "-", strtolower($brand_details[0]['brand_Name']))."/".$brand_details[0]['brandId']; ?>/legacy">Legacy</a></li>
                    <?php } ?>
                  
                    <?php if ($brand_details[0]['upcoming'] != '') { ?>
                        <li><a href="#brand-upcoming" data-toggle="tab">Upcoming</a></li>
                    <?php } ?>
                     <?php if ($brand_details[0]['fb_userid'] != '' || $brand_details[0]['twitter_userid'] != '') { ?>
                    <li class="<?php if($this->uri->segment(4, 0)=="social-feed"){ echo "active"; } ?>"><a href="<?php echo base_url()."brand/".str_replace(" ", "-", strtolower($brand_details[0]['brand_Name']))."/".$brand_details[0]['brandId']; ?>/social-feed">Social Feed</a></li>
      			 <?php } ?>
  <?php if(count($brand_news) != '0' ){ ?>
                    <li class="<?php if($this->uri->segment(4, 0)=="latest-news"){ echo "active"; } ?>"><a href="<?php echo base_url()."brand/".str_replace(" ", "-", strtolower($brand_details[0]['brand_Name']))."/".$brand_details[0]['brandId']; ?>/latest-news">Latest News</a></li>
 <?php } ?>
                     <?php if (count($brand_stores) != "0") { 
				
					?>
                      <?php /*?>  <li style="display:none;"><a href="#store-loc" data-toggle="tab">Store Locator</a></li><?php */?>
 <li><a href="#" data-toggle="tab">Store Locator</a></li>
                    <?php } ?>
                </ul>
</div>
            </div> <!--/category-tab-panel -->

            <div id="myTabContent1" class="tab-content" <?php if($this->uri->segment(4, 0)=="legacy"){  "<scr'+'ipt> $('#brand-legacy').addClass('active in'); </scr'+'ipt>"; } ?>>

                <?php
                echo $brand_details[0]['legacy'];
                if ($management_details) {
                    foreach ($management_details as $review) {
                        ?>
                        <div class="legacy-sectn col-lg-12 col-md-12 col-sm-11 col-xs-11 nopadding">
                            <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12 content_headline2">
                                <h2><?php echo $review['heading']; ?></h2>
                                <hr>
                            </div>
                            <div class="legacy-tagline col-lg-10 col-md-10 col-sm-11 col-xs-12 nopadding">
                                <div class="cxo-image col-lg-4 col-md-4 col-sm-4 col-xs-12 nopadding">
                                    <div class="cxo-img-border"><img
                                            src="<?php echo base_url() . $review['author_image']; ?>"></div>
                                    <h5> <?php echo $review['author_name']; ?></h5>
                                    <h6> <?php echo $review['author_designation']; ?></h6>
                                </div> <!--/cxo-image -->
                                <div class="legacy-text col-lg-8 col-md-8 col-sm-8 col-xs-12 nopadding">
                                    <p class="nomargin"><?php echo $review['description']; ?></p>
                                </div> <!--/legacy-text -->
                            </div> <!-- /legacy-tagline -->
                        </div>
                    <?php }
                } ?> <!-- /legacy-sectn ends -->
</div>
            </div> <!-- brand legacy tab content ends -->
            <div class="brand-product-con tab-pane fade <?php if($this->uri->segment(4, 0)=="Products"){ echo "active in"; } ?>" id="brand-product">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12 content_headline2" style="display:none;">
                        <h2><?php if ($this->uri->segment(3, 0) == "43") {
                                echo "Services";
                            } else {
                                echo "More posts like this one";
                            } ?> </h2>
                        <hr>
                    </div>
                </div>
            
                <!--/column-->

                <!-- mywork place ends here-->
                <div id="brand_count" style="display:none;"><?php echo $brand_count[0]['no']; ?></div>
                <div class="container loading-sectn" style="text-align: center">
                    <button class="btn" id="load_more" style="background:none" data-val="1"><img style="display: none"
                                                                                                 id="loader"
                                                                                                 src="<?php echo str_replace('index.php', '', base_url()) ?>loader.gif"
                                                                                                 class="img-responsive">
                    </button>
                </div>
                <div id="show" style="display:none" class="nomore">No more products to showcase</div>

            </div> <!-- brand products tab content ends -->
            <div class="upcoming-con tab-pane fade" id="brand-upcoming">
                this is upcoming part
            </div> <!-- brand products tab content ends -->

            <div class="social-feed-con tab-pane fade <?php if($this->uri->segment(4, 0)=="social-feed"){ echo "active in"; } ?>" id="social-feed">
                <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12 content_headline2" style="display:none;">
                    <h2 onclick="correct();">social feed</h2>
                    <hr>
                </div>
                <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 wall whats_new_content12" id="feed">
                    <div class="grid" id="masonry-grid1">


                    </div>
                </div>
                <!--/whats_new_content -->
            </div><!--/social-feed-con tab content ends -->
            <div class="latest-news-con tab-pane fade <?php if($this->uri->segment(4, 0)=="latest-news"){ echo "active in"; } ?>" id="latest-news">
                <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12 content_headline2" style="display:none;">
                    <h2>latest news</h2>
                    <hr>
                </div>
                <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 latest-tab-in wall whats_new_content">
                    <div class="grid" id="masonry-grid2">
                        <?php
                        foreach ($brand_news as $news) {
                            ?>

                            <div class="wall-column grid-item">
                                <div class="wall-item">
                                    <div class="category">
                                        <a href="<?php echo base_url(); ?>main/brandnews/<?php echo $news['brand_id']; ?>/<?php echo $news['news_id']; ?>">
                                            <div class="category_img">

                                                <img src="<?php echo base_url(); ?><?php echo $news['news_img']; ?>"
                                                     class="img-responsive">

                                            </div> <!--/category_img-->
                                        </a>
                                        <div class="category_content">
                                            <div class="news-cat"><span><?php echo $news['type']; ?> </span></div>
                                            <a href="<?php echo base_url(); ?>main/brandnews/<?php echo $news['brand_id']; ?>/<?php echo $news['news_id']; ?>">
                                                <h3 class="news-headline"><?php echo $news['headline']; ?></h3></a>
                                            <a href="<?php echo base_url(); ?>main/brandnews/<?php echo $news['brand_id']; ?>/<?php echo $news['news_id']; ?>">
                                                <h2><?php echo $news['news_small_desc']; ?> </h2></a>
                                        </div> <!--/category_content-->
                                        <div class="news-links">
                                            <a href="#"><span
                                                    class="brand-name"><?php echo $news['brand_Name']; ?></span></a> <a target="_blank"
                                                href="<?php echo $news['news_link']; ?>"><span class="read-more">Read More </span></a>
                                        </div>
                                        <div class="category_options">
                                            <table>
                                                <tbody>
                                                <tr>
                                                    <td class="date-status ">
                                                        <p><?php $now = time();
                                                            $startDate = $news['created_on']; // or your date as well
                                                            $your_date = strtotime($startDate);
                                                            $datediff = $now - $your_date;
                                                            echo floor(($datediff / (60 * 60 * 24)) + 1); ?>days ago</p>
                                                    </td>
                                                    <td class="crown-news" id="<?php echo $news['news_id']; ?>"><span class="text" id="crowncount"><?php foreach($this->getdata->count_crown_news($news['news_id']) as $count_no){ echo $count_no['no'];  }?> </span><span


                                                            class="icomoon icon-crown"> </span></td>
                                                    <td class="fwd-icon-sectn"><span class="text"> </span><span
                                                            class="icomoon icon-sharing"></span></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <div style="display:none;" class="fwd-social-icons sec7">
                                                <ul class="cat-follow-icons">
                                                    <li><a target="_blank"
                                                           href="https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url(); ?>main/brandnews/<?php echo $news['brand_id']; ?>/<?php echo $news['news_id']; ?>"
                                                           class="fb"></a></li>
                                                    <li><a target="_blank"
                                                           href="https://twitter.com/home?status=<?php echo base_url(); ?>main/brandnews/<?php echo $news['brand_id']; ?>/<?php echo $news['news_id']; ?>"
                                                           class="twitter"></a></li>
                                                    <?php /*?><li><a target="_blank" href="javascript:void(0)" class="insta"></a></li><?php */ ?>
                                                    <li class="show-more"><a href="javascript:void(0)" class="more"></a>
                                                    </li>
                                                </ul>
                                                <div style="display:none;" class="hidden-more-icons">
                                                    <ul class="follow-icons-more">
                                                        <li><a target="_blank" class="linkdin"
                                                               href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo base_url(); ?>main/brandnews/<?php echo $news['brand_id']; ?>/<?php echo $news['news_id']; ?>&title=Rigalio&summary=Rigalio%20content&source=<?php echo base_url(); ?>main/brandnews/<?php echo $news['brand_id']; ?>/<?php echo $news['news_id']; ?>"> </a>
                                                        </li>
                                                        <li><a target="_blank" class="gplus"
                                                               href="<?php echo base_url(); ?>main/brandnews/<?php echo base_url(); ?>main/brandnews/<?php echo $news['brand_id']; ?>/<?php echo $news['news_id']; ?>"> </a>
                                                        </li>
                                                        <li><a target="_blank" class="pini"
                                                               href="<?php echo base_url(); ?>main/brandnews/<?php echo base_url(); ?>main/brandnews/<?php echo $news['brand_id']; ?>/<?php echo $news['news_id']; ?>"> </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div> <!--/inner forward icons social -->
                                            <div style="display: none;" class="write-comment">

                                                <div class="few-comm">
                                                    <table>
                                                        <tr>
                                                            <td><span class="pf-img"
                                                                      style="background:url(content/images/timeline/profile-pic-small.png);"></span>
                                                            </td>
                                                            <td class="post-com5">Is this better than Audi</td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="pf-img"
                                                                      style="background:url(content/images/timeline/profile-pic-small.png);"></span>
                                                            </td>
                                                            <td class="post-com5">Is this better than Audi</td>
                                                        </tr>
                                                        <tr>
                                                            <td><span class="pf-img"
                                                                      style="background:url(content/images/timeline/profile-pic-small.png);"></span>
                                                            </td>
                                                            <td class="post-com5">Is this better than Audi</td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class="see-all-com"> see all</div>
                                                <span class="pf-img"
                                                      style="background:url(content/images/timeline/profile-pic-small.png);"></span>
                                                <span class="commentbox"><a><input type="text"
                                                                                   placeholder="Write a Comment"
                                                                                   value="" name=""></a> </span><a
                                                    href="#"><span class="icomoon icon-reply"></span> </a>

                                            </div> <!--/write-comment -->
                                        </div><!--/category_options-->
                                    </div> <!--/category-->
                                </div> <!--/wall-item -->
                            </div> <!--/wall-column -->
                        <?php }
                        ?>

                    </div>
                </div>
            </div><!--/latest-news-con tab content ends -->
            <div class="store-loc-con tab-pane fade" id="store-loc">
                <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12 content_headline2">
                    <h2>store locator</h2>
                    <hr>
                </div>
                <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 nopadding store-loc-inner" id="container">
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 nopadding storelist">
                        <h3>Locate a store </h3>
                        <div>
                            <hr>
                        </div>
                        <address class="line1"><?php echo $brand_details[0]['brand_Name']; ?> stores</address>
                        <div class="find-store col-lg-12 nopadding">
                            <input type="text" value="<?php echo $brand_details[0]['brand_Name']; ?>" id="brandname"
                                   style="display:none">
                            <input type="text" value="" placeholder="Enter City" name="" id="txtAddress"
                                   class="store-box">
                            <select id="txtdistance">
                                <option value="1">select range</option>
                                <option value="10">10 km</option>
                                <option value="200">200 km</option>
                                <option value="400">400 km</option>
                                <option value="600">600 km</option>
                                <option value="800">800 km</option>
                                <option value="1000">1000 km</option>
                                <option value="5500">All Stores</option>
                            </select>
                            <button class="store-btn" id="btnSearch"><span><i class="fa fa-search"
                                                                              aria-hidden="true"></i></span></button>
                        </div>
                        <hr>
                        <div class="inner-stores-list scrollbar" id="ex3">
                            <div class="store1" id="divStores">

                            </div> <!-- /store1-->

                        </div> <!-- /inner-stores-list-->


                    </div> <!--/storelist -->
                    <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12 nopadding storemap">
                        <div id="map_canvas"></div>
                    </div> <!--/storemap -->

                </div> <!--/store-loc-inner -->
            </div>


        </div> <!--/tab-content -->
        <div class="product-share-sectn brand-pg col-lg-7 col-md-7 col-sm-11 col-xs-12">
            <h5>Share This </h5>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                <div class="product-inner-social col-lg-5 col-md-5 col-sm-12 col-xs-12 nopadding">
                    <ul class="follow-icons">
                        <li><a target="_blank" class="facebook-follow"
                               href="https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url(); ?>brand/<?php echo str_replace(" ", "-", strtolower($brand_details[0]['brand_Name'])); ?>/<?php echo $brand_details[0]['brandId']; ?>"></a>
                        </li>
                        <li><a target="_blank" class="twitter-follow"
                               href="https://twitter.com/home?status=<?php echo base_url(); ?>brand/<?php echo str_replace(" ", "-", strtolower($brand_details[0]['brand_Name'])); ?>/<?php echo $brand_details[0]['brandId']; ?>"></a>
                        </li>
                        <?php /*?> <li><a class="instagram-follow" href="" target="_blank"></a></li><?php */ ?>
                        <li><a target="_blank" class="linkedin-follow"
                               href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo base_url(); ?>brand/<?php echo str_replace(" ", "_", strtolower($brand_details[0]['brand_Name'])); ?>/<?php echo $brand_details[0]['brandId']; ?>&title=Rigalio&summary=Rigalio%20content&source=<?php echo base_url(); ?>brand/<?php echo str_replace(" ", "_", strtolower($brand_details[0]['brand_Name'])); ?>/<?php echo $brand_details[0]['brandId']; ?>"></a>
                        </li>
                        <li><a target="_blank" class="rss-follow"
                               href="https://plus.google.com/share?url=<?php echo base_url(); ?>brand/<?php echo str_replace(" ", "-", strtolower($brand_details[0]['brand_Name'])); ?>/<?php echo $brand_details[0]['brandId']; ?>"></a>
                        </li>

                    </ul>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 nopadding btn-sectn"><a target="_blank"
                        href="<?php echo $brand_details[0]['official_link']; ?>">
                        <button class="goto-web-btn">Go to official website</button>
                    </a>
                    <button class="query-btn" id="btn-sendform">Send a query</button>
                </div>

            </div>
        </div> <!-- /product-share-sectn -->
        <div class="sendquery-form" style="display: none;">
            <form class="col-lg-6 col-md-7 col-sm-10 col-xs-12" name="sendaquery" method="post">
                <label>Send a query</label>
                <p id="msgbrand"></p>
                <div id="myhide1" class="myhide1">


                    <input type="text" name="name" id="namebrand" value="" placeholder="NAME" class="textbox textbox2">
                    <input type="text" name="contactno" id="contactnobrand" value="" placeholder="CONTACT NO."
                           class="textbox textbox2">
                    <input type="text" name="emailid" id="emailidbrand" value="" placeholder="EMAIL ID" class="textbox">
                    <!--<input type="text" name="date" id="date" value="" placeholder="CHOOSE A SUITABLE DATE" class="textbox textbox3">
                    <input type="text" name="time" id="time" value="" placeholder="AND TIME" class="textbox textbox4">
                    <input type="text" name="query" id="query" value="" placeholder="DEFAULT" class="textbox textbox5">-->
                    <textarea class="textbox" id="querybrand" name="query" placeholder="QUERY" rows="5"> </textarea>
                    <input type="text" name="formname" id="formnamebrand" value="Send A Query" style="display:none;">
                    <button name="submit" class="try-now-submit1" type="button">SUBMIT</button>
                </div>
            </form>
        </div> <!--/try-now-con -->

    </div>
</div>
</div>
<!--barnd pg-con ends -->

<footer>
    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 footer-inner">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <p>All rights reserved. All content belongs to respective owners </p>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <ul>
                <li><a href="<?php echo base_url(); ?>main/aboutus">About us </a></li>
                <li><a href="<?php echo base_url(); ?>main/contactus">Contact us</a></li>
                <li><a href="<?php echo base_url(); ?>main/privacy">Privacy </a></li>
                <li><a href="<?php echo base_url(); ?>main/faq">Faq </a></li>
            </ul>
        </div>
    </div>
</footer>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo base_url(); ?>content/js/jaliswall.js"></script>

<script type="text/javascript">
   // var $container = jQuery('#masonry-grid');
//    var $grid = $container.masonry({
//        percentPosition: true,
//        itemSelector: '.grid-item'
//    });

    $(document).ready(function (e) {
        $('#masonry-grid').masonry({
            itemSelector: '.grid-item',
            percentPosition: true
        });
        var page = $("#load_more").data('val');
        var brand_count = "<?php echo $brand_count[0]['no']; ?>";
        var brand_id = "<?php print_r($brand); ?>";
        var per_page = 4;
        //alert(page);

        var total_pages = Math.ceil(parseInt(brand_count) / parseInt(per_page));
        //alert(total_pages);


    });

    $(window).scroll(function (e) {

        if ($(window).scrollTop() == $(document).height() - $(window).height()) {
            //alert("hi");
            e.preventDefault();
            // $("#load_more").data('val');
            page = $("#load_more").data('val');
            var brand_count = "<?php echo $brand_count[0]['no']; ?>";
            var brand_id = "<?php print_r($brand); ?>";
            var per_page = 4;
            // alert(page);
            var total_pages = Math.ceil(parseInt(brand_count) / parseInt(per_page));
            //alert(total_pages);
            if (page <= total_pages) {
                getbrand(page, brand_id);
            }
            else {
               // $("#show").show();
            }


        }
    });

    var getbrand = function (page, brand_id) {
        var data = {
            "page": page,
            "brand_id": brand_id
        };
        $("#loader").show();
        $('#load_more').data('val', ($('#load_more').data('val') + 1));
        $.ajax({
            url: "<?php echo base_url() ?>myscroll/getbrand",
            type: 'GET',
            data: data
        }).done(function (response) {

            response = response.trim();
            var dom = $('<div/>').html(response).contents();
            $(dom).each(function () {
                if ($(this).hasClass('wall-column')) {
                    $grid.append($(this));
                    var el = this;
                    el.getElementsByTagName('img')[0].onload = function () {
                        console.log(el);
                        $grid.masonry('appended', $(el))
                    };

                }
            })
            onsload();
            $("#loader").hide();
        });
    };


</script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>googlemaps/stylegoogle.css"/>
<script type="text/javascript"src="http://maps.googleapis.com/maps/api/js?sensor=false&key=AIzaSyD0X4v7eqMFcWCR-VZAJwEMfb47id9IZao">
</script>
<script type="text/javascript">
$('#masonry-grid2').masonry({
  // options
  itemSelector: '.grid-item',
  percentPosition: true
  //columnWidth: 200
});
</script>
<script type="text/javascript">
 $('#brand-legacy').removeClass('active in'); 
    var map;
    var latlng;
    var myOptions;
    $(document).ready(function () {

        //draw a map centered at Empire State Building Newyork
        latlng = new google.maps.LatLng(40.748492, -73.985496);
        myOptions = {
            zoom: 5,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

        $("#btnSearch").click(function () {
            //Convert Address Into LatLng and Retrieve Address Near by
            convertAddressToLatLng($("#txtAddress").val());
        });
    });

    function convertAddressToLatLng(address) {
        var geocoder = new google.maps.Geocoder();

        geocoder.geocode({'address': address}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                //Empty div before re-populating
                $("#divStores").html('');
                searchStores(results[0].geometry.location);
            } else {
                $("#divStores").html(getEmbedHTML('No Stores Found', '', ''));
            }
        });
    }

    function searchStores(location) {
        var latlng = new google.maps.LatLng(location.lat(), location.lng());
        var distance = $("#txtdistance").val();
        //salert(distance);
        if (distance == '1') {
            //alert("hi");
            distance = "5500";
        }
        else {
            distance = $("#txtdistance").val();
        }
        //alert(distance);
        var brandname = $("#brandname").val();
        var myOptions = {
            zoom: 5,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

        //Marker at the address typed in
        var image = '<?php echo base_url();?>googlemaps/images/townhouse.png'
        var marker = new google.maps.Marker({
            position: latlng,
            map: map,
            icon: image
        });

        //hard coded the radius to 10 miles, if you get the value from a field if required
        var parameters = '&lat=' + location.lat() + '&lng=' + location.lng() + '&radius=' + distance + '&brandname=' + brandname;
        //console.log(parameters);
        $.ajax({
            type: "POST",
            dataType: 'json',
            url: "http://rigalio.com/x/no_access/main/store_locator",
            data: parameters,
            success: function (msg) {
                //alert(msg);
                displayStores(msg);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                //alert(thrownError);
            }
        });
        /* $.ajax({  */
    }

    function displayStores(result) {
        if (result.length > 0) {
            for (i = 0; i < result.length; i++) {
                //Append Store Address on Sidebar
                var html = getEmbedHTML(i + 1, result[i].name, result[i].address, result[i].distance, result[i].city, result[i].phone_no);
                $("#divStores").append(html);
                //place a marker
                var image = '<?php echo base_url();?>googlemaps/images/number_' + parseInt(i + 1) + '.png';
                var latlng = new google.maps.LatLng(result[i].lat, result[i].lng);
                var marker = new google.maps.Marker({
                    position: latlng,
                    map: map,
                    icon: image
                });

                var msg = 'Location : ' + result[i].name + '<br/> ';
                msg = msg + 'Address : ' + result[i].address + '<br/> ';
                attachMessage(marker, msg);
            }
        } else {
            $("#divStores").html(getEmbedHTML('No Stores Found', '', ''));
        }
    }

    function attachMessage(marker, msg) {
        var infowindow = new google.maps.InfoWindow({
            content: msg,
            size: new google.maps.Size(120, 150)
        });
        google.maps.event.addListener(marker, 'click', function () {
            infowindow.open(map, marker);
        });
    }

    function getEmbedHTML(seqno, name, address, distance, city, phone_no) {
        var strhtml = '<div class="row">';
        strhtml = strhtml + '<img src="<?php echo base_url();?>googlemaps/images/number_' + seqno + '.png" border="0" width="24" height="24" style="padding-right:10px;" /><span class="line2">' + name + '</span><br/>'
        strhtml = strhtml + '<span class="line3">' + address + '<span><br/>' //address
        strhtml = strhtml + '<span class="code">' + city + '<span><br/>' //pin plus city
        strhtml = strhtml + '<span class="code">' + phone_no + '<span><br/>' //pin plus city
        //strhtml  =  strhtml + '<span class="tel-no"> Distance : ' + parseFloat(distance).toFixed(2) + ' miles<span><br/>' //telphone no
        strhtml = strhtml + '</div><div class="separator"></div>';

        return strhtml;
    }

    $(window).load(function () {

  onsload();
        convertAddressToLatLng("india");


        var data = {
            "count": 25,
            "fb_userid": '<?php echo $brand_details[0]['fb_userid']; ?>',
            "twitter_userid": '<?php echo $brand_details[0]['twitter_userid']; ?>'
        };

        $.ajax({
            type: "GET",
            url: "http://www.rigalio.com/x/",

            //dataType: 'json',
            data: data,
            success: function (html) {

              $(".whats_new_content12").html(html);
$('.whats_new_content12').jaliswall({item: '.wall-item'});
                //correct();

                


                // alert(html);
                // $("#masonry-grid1").html(html);
                // alert(html);
                //$("#"+id).next().css( "background", "yellow" );
                //alert(html);
            }

        });  //ajax ends here


    });


   


/*
 $('a[href="#latest-news"]').on('show.bs.tab', function(){
   // $('#latest-news').click(function(e){

        console.log("clicked");
$('.wall').jaliswall();

       // $('.latest-news').jaliswall({item: '.wall-item'});
    });
*/
/*
    $('a[href="#social-feed"]').on('show.bs.tab', function(){

$(".whats_new_content12").html("")
 var data = {
            "count": 25,
            "fb_userid": '<?php echo $brand_details[0]['fb_userid']; ?>',
            "twitter_userid": '<?php echo $brand_details[0]['twitter_userid']; ?>'
        };


        $.ajax({
            type: "GET",
            url: "http://www.rigalio.com/x/",

            //dataType: 'json',
            data: data,
            success: function (html) {

                $(".masonry-grid1").html(html);
//$('.whats_new_content12').jaliswall({item: '.wall-item'});
             

                // alert(html);
                // $("#masonry-grid1").html(html);
                // alert(html);
                //$("#"+id).next().css( "background", "yellow" );
                //alert(html);
            }

        });  //ajax ends here
        $('.whats_new_content12').jaliswall({item: '.wall-item'});
        
        
        
    });

    */

$(window).load(function(){
 $('.whats_new_content').jaliswall({item: '.wall-item'})
 var check="<?php echo $this->uri->segment(4, 0); ?>";

 if(check=="legacy"){

 $('#brand-legacy').addClass('active in'); 
}
 else{

 $('#brand-legacy').removeClass('active in'); 
}

});
    
/*
var $grid = $('#latest-news').masonry({
  itemSelector: '.wall',
  //columnWidth: 160,
  // disable initial layout
  initLayout: false
});
// add event listener for initial layout
$grid.on( 'layoutComplete', function( event, items ) {
  console.log( items.length );
//alert("test");
$grid.masonry();

});
// trigger initial layout
*/
function myurl(url){
//alert(url);

    window.location.href=url;

}


</script>

<style>
.content_headline2{
display:none;

}
</style>

<script src="<?php echo base_url(); ?>content/js/hover.js"></script>
