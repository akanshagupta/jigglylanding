    <!-- Carousel
    ================================================== -->
<div style="width: 100%; display: block; clear: both;">
<div style="width: 100%; display: block; clear: both;">
  <div class="main-slider-container hidden-xs screen-flip-slider">  
    <div class=" main-slider">
      <div class="ui grid ">
        <div class="five column row ">
          <div class="column">
            <div class="ui people shape">
              <div class="sides">
                <div class="active side">
                  <div class="content">
                    <div class="center">
                       <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1.png" class='ui image'>
                       <div class="banner-hover-con">
                         <div class="tab">
                          <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                          </div>
                         </div>
                       </div> <!--/banner-hover-con -->
                    </div>
                  </div>
                </div>
                <div class="side">
                  <div class="content">
                    <div class="center">
                       <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy.png" class='ui image'>
                       <div class="banner-hover-con">
                         <div class="tab">
                          <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                          </div>
                         </div>
                       </div> <!--/banner-hover-con -->
                    </div>
                  </div>
                </div>
                <div class="side">
                  <div class="content">
                    <div class="center">
                      <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 2.png" class='ui image'>
                      <div class="banner-hover-con">
                         <div class="tab">
                          <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                          </div>
                         </div>
                       </div> <!--/banner-hover-con -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="ui people shape">
              <div class="sides">
                <div class="active side">
                  <div class="content">
                    <div class="center">
                       <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 3.png" class='ui image'>
                       <div class="banner-hover-con">
                         <div class="tab">
                          <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                          </div>
                         </div>
                       </div> <!--/banner-hover-con -->
                    </div>
                  </div>
                </div>
                <div class="side">
                  <div class="content">
                    <div class="center">






                       <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 4.png" class='ui image'>
                      <div class="banner-hover-con">
                         <div class="tab">
                          <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                          </div>
                         </div>
                       </div> <!--/banner-hover-con -->
                    </div>
                  </div>
                </div>
                <div class="side">
                  <div class="content">
                    <div class="center">
                       <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 5.png" class='ui image'>
                       <div class="banner-hover-con">
                         <div class="tab">
                          <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                          </div>
                         </div>
                       </div> <!--/banner-hover-con -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class=" column">
            <div class="ui people shape">
              <div class="sides">
                <div class="active side">
                  <div class="content">
                       <img src="<?php echo base_url(); ?>content/images/banner/large/Artboard 1.png" class='ui image'>
                       <div class="banner-hover-con">
                         <div class="tab">
                          <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                          </div>
                         </div>
                       </div> <!--/banner-hover-con -->
                  </div>
                </div>
                <div class="side">
                  <div class="content">
                      <img src="<?php echo base_url(); ?>content/images/banner/large/Artboard 1 copy.png" class='ui image'>
                      <div class="banner-hover-con">
                         <div class="tab">
                          <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                          </div>
                         </div>
                       </div> <!--/banner-hover-con -->
                  </div>
                </div>
                <div class="side">
                  <div class="content">
                       <img src="<?php echo base_url(); ?>content/images/banner/large/Artboard 1 copy 2.png" class='ui image'>
                       <div class="banner-hover-con">
                         <div class="tab">
                          <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                          </div>
                         </div>
                       </div> <!--/banner-hover-con -->
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="column">
            <div class="ui people shape">
              <div class="sides">
                <div class="active side">
                  <div class="content">
                    <div class="center">
                       <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 6.png" class='ui image'>
                       <div class="banner-hover-con">
                         <div class="tab">
                          <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                          </div>
                         </div>
                       </div> <!--/banner-hover-con -->
                    </div>
                  </div>
                </div>
                <div class="side">
                  <div class="content">
                    <div class="center">
                       <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 7.png" class='ui image'>
                       <div class="banner-hover-con">
                         <div class="tab">
                          <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                          </div>
                         </div>
                       </div> <!--/banner-hover-con -->
                    </div>
                  </div>
                </div>
                <div class="side">
                  <div class="content">
                    <div class="center">
                       <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 8.png" class='ui image'>
                       <div class="banner-hover-con">
                         <div class="tab">
                          <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                          </div>
                         </div>
                       </div> <!--/banner-hover-con -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="ui people shape">
              <div class="sides">
                <div class="active side">
                  <div class="content">
                    <div class="center">
                       <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 9.png" class='ui image'>
                       <div class="banner-hover-con">
                         <div class="tab">
                          <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                          </div>
                         </div>
                       </div> <!--/banner-hover-con -->
                    </div>
                  </div>
                </div>
                <div class="side">
                  <div class="content">
                    <div class="center">
                       <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 10.png" class='ui image'>
                       <div class="banner-hover-con">
                         <div class="tab">
                          <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                          </div>
                         </div>
                       </div> <!--/banner-hover-con -->
                    </div>
                  </div>
                </div>
                <div class="side">
                  <div class="content">
                    <div class="center">
                       <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 11.png" class='ui image'>
                       <div class="banner-hover-con">
                         <div class="tab">
                          <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                          </div>
                         </div>
                       </div> <!--/banner-hover-con -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class=" column hidden-sm">
            <div class="ui people shape">
              <div class="sides">
                <div class="active side">
                  <div class="content">
                       <img src="<?php echo base_url(); ?>content/images/banner/large/Artboard 1 copy 3.png" class='ui image'>
                       <div class="banner-hover-con">
                         <div class="tab">
                          <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                          </div>
                         </div>
                       </div> <!--/banner-hover-con -->
                  </div>
                </div>
                <div class="side">
                  <div class="content">
                      <img src="<?php echo base_url(); ?>content/images/banner/large/Artboard 1 copy 4.png" class='ui image'>
                      <div class="banner-hover-con">
                         <div class="tab">
                          <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                          </div>
                         </div>
                       </div> <!--/banner-hover-con -->
                  </div>
                </div>
                <div class="side">
                  <div class="content">
                      <img src="<?php echo base_url(); ?>content/images/banner/large/Artboard 1 copy 5.png" class='ui image'>
                      <div class="banner-hover-con">
                         <div class="tab">
                          <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                          </div>
                         </div>
                       </div> <!--/banner-hover-con -->
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          <div class="column hidden-sm">
            <div class="ui people shape">
              <div class="sides">
                <div class="active side">
                  <div class="content">
                    <div class="center">
                       <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 12.png" class='ui image'>
                       <div class="banner-hover-con">
                         <div class="tab">
                          <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                          </div>
                         </div>
                       </div> <!--/banner-hover-con -->
                    </div>
                  </div>
                </div>
                <div class="side">
                  <div class="content">
                    <div class="center">
                       <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 13.png" class='ui image'>
                       <div class="banner-hover-con">
                         <div class="tab">
                          <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                          </div>
                         </div>
                       </div> <!--/banner-hover-con -->
                    </div>
                  </div>
                </div>
                <div class="side">
                  <div class="content">
                    <div class="center">
                       <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 14.png" class='ui image'>
                       <div class="banner-hover-con">
                         <div class="tab">
                          <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                          </div>
                         </div>
                       </div> <!--/banner-hover-con -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="ui people shape">
              <div class="sides">
                <div class="active side">
                  <div class="content">
                    <div class="center">
                       <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 15.png" class='ui image'>
                       <div class="banner-hover-con">
                         <div class="tab">
                          <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                          </div>
                         </div>
                       </div> <!--/banner-hover-con -->
                    </div>
                  </div>
                </div>
                <div class="side">
                  <div class="content">
                    <div class="center">
                       <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 16.png" class='ui image'>
                       <div class="banner-hover-con">
                         <div class="tab">
                          <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                          </div>
                         </div>
                       </div> <!--/banner-hover-con -->
                    </div>
                  </div>
                </div>
                <div class="side">
                  <div class="content">
                    <div class="center">
                       <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 17.jpg" class='ui image'>
                       <div class="banner-hover-con">
                         <div class="tab">
                          <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                          </div>
                         </div>
                       </div> <!--/banner-hover-con -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
           
        </div>

        <ul class="progress-buttons">
          <li class="button active"></li>
          <li class="button"></li>
          <li class="button"></li>
        </ul>
      </div> <!--/ui grid flip slider -->


    </div>
  </div> 


  <!--slider for mobile -->
   <div class="main-slider-container mobile-flip-slider hidden-lg hidden-md hidden-sm">  
     <div class=" main-slider">
      <div class="ui grid ">
        <div class="two column row ">
          <div class="column">
            <div class="ui people shape">
              <div class="sides">
                <div class="active side">
                  <div class="content">
                    <div class="center">
                       <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1.png" class='ui image'>
                       <div class="banner-hover-con">
                         <div class="tab">
                          <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                          </div>
                         </div>
                       </div> <!--/banner-hover-con -->
                    </div>
                  </div>
                </div>
                <div class="side">
                  <div class="content">
                    <div class="center">
                       <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy.png" class='ui image'>
                       <div class="banner-hover-con">
                         <div class="tab">
                          <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                          </div>
                         </div>
                       </div> <!--/banner-hover-con -->
                    </div>
                  </div>
                </div>
                <div class="side">
                  <div class="content">
                    <div class="center">
                      <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 2.png" class='ui image'>
                      <div class="banner-hover-con">
                         <div class="tab">
                          <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                          </div>
                         </div>
                       </div> <!--/banner-hover-con -->

                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="ui people shape">
              <div class="sides">
                <div class="active side">
                  <div class="content">
                    <div class="center">
                       <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 3.png" class='ui image'>
                       <div class="banner-hover-con">
                         <div class="tab">
                          <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                          </div>
                         </div>
                       </div> <!--/banner-hover-con -->
                    </div>
                  </div>
                </div>
                <div class="side">
                  <div class="content">
                    <div class="center">
                       <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 4.png" class='ui image'>
                      <div class="banner-hover-con">
                         <div class="tab">
                          <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                          </div>
                         </div>
                       </div> <!--/banner-hover-con -->
                    </div>
                  </div>
                </div>
                <div class="side">
                  <div class="content">
                    <div class="center">
                       <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 5.png" class='ui image'>
                       <div class="banner-hover-con">
                         <div class="tab">
                          <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                          </div>
                         </div>
                       </div> <!--/banner-hover-con -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="ui people shape">
              <div class="sides">
                <div class="active side">
                  <div class="content">
                    <div class="center">
                       <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 6.png" class='ui image'>
                       <div class="banner-hover-con">
                         <div class="tab">
                          <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                          </div>
                         </div>
                       </div> <!--/banner-hover-con -->
                    </div>
                  </div>
                </div>
                <div class="side">
                  <div class="content">
                    <div class="center">
                       <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 7.png" class='ui image'>
                      <div class="banner-hover-con">
                         <div class="tab">
                          <div class="tab-in">

                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                          </div>
                         </div>
                       </div> <!--/banner-hover-con -->
                    </div>
                  </div>
                </div>
                <div class="side">
                  <div class="content">
                    <div class="center">
                       <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 8.png" class='ui image'>
                       <div class="banner-hover-con">
                         <div class="tab">
                          <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                          </div>
                         </div>
                       </div> <!--/banner-hover-con -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class=" column">
            <div class="ui people shape">
              <div class="sides">
                <div class="active side">
                  <div class="content">
                        <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 9.png" class='ui image'>
                       <div class="banner-hover-con">
                         <div class="tab">
                          <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                          </div>
                         </div>
                       </div> <!--/banner-hover-con -->
                  </div>
                </div>
                <div class="side">
                  <div class="content">
                       <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 10.png" class='ui image'>
                      <div class="banner-hover-con">
                         <div class="tab">
                          <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                          </div>
                         </div>
                       </div> <!--/banner-hover-con -->
                  </div>
                </div>
                <div class="side">
                  <div class="content">
                        <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 11.png" class='ui image'>
                       <div class="banner-hover-con">
                         <div class="tab">
                          <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                          </div>
                         </div>
                       </div> <!--/banner-hover-con -->
                  </div>
                </div>
              </div>
            </div>
            <div class="ui people shape">
              <div class="sides">
                <div class="active side">
                  <div class="content">
                        <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 12.png" class='ui image'>
                       <div class="banner-hover-con">
                         <div class="tab">
                          <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                          </div>
                         </div>
                       </div> <!--/banner-hover-con -->
                  </div>
                </div>
                <div class="side">
                  <div class="content">
                       <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 13.png" class='ui image'>
                      <div class="banner-hover-con">
                         <div class="tab">
                          <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                          </div>
                         </div>
                       </div> <!--/banner-hover-con -->
                  </div>
                </div>
                <div class="side">
                  <div class="content">
                        <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 14.png" class='ui image'>
                       <div class="banner-hover-con">
                         <div class="tab">
                          <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                          </div>
                         </div>
                       </div> <!--/banner-hover-con -->
                  </div>
                </div>
              </div>
            </div>
            <div class="ui people shape">
              <div class="sides">
                <div class="active side">
                  <div class="content">
                        <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 15.png" class='ui image'>
                       <div class="banner-hover-con">
                         <div class="tab">
                          <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                          </div>
                         </div>
                       </div> <!--/banner-hover-con -->
                  </div>
                </div>
                <div class="side">
                  <div class="content">
                       <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 16.png" class='ui image'>
                      <div class="banner-hover-con">
                         <div class="tab">
                          <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                          </div>
                         </div>
                       </div> <!--/banner-hover-con -->
                  </div>
                </div>
                <div class="side">
                  <div class="content">
                        <img src="<?php echo base_url(); ?>content/images/banner/small/Artboard 1 copy 17.jpg" class='ui image'>
                       <div class="banner-hover-con">
                         <div class="tab">
                          <div class="tab-in">
                             <span><h4>Add a pop of yellow to black and gray </h4>
                             <hr>
                             <div> <img src="<?php echo base_url(); ?>content/images/icons/hovered-img-icon.png"> </div> </span>
                          </div>
                         </div>
                       </div> <!--/banner-hover-con -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <ul class="progress-buttons">
          <li class="button active"></li>
          <li class="button"></li>
          <li class="button"></li>
        </ol>
      </div> <!--/ui grid flip slider -->


    </div>
  </div> 

  <!--slider for mobile ends -->
</div>      
</div>
    <div class="main_content"> 
<div class="">  
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12 content_headline">
            <h2 ng-model="heading"><?php print_r($heading); ?></h2>
            <hr>
         </div> 
      </div> <!--/content tagline-->
      <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 wall whats_new_content" id="abc">
        

      </div>   <!--/whats_new_content --> 
      
      <!--/column-->

            <!-- mywork place ends here--> 
     <div id="total_product_count" style="display:none;"><?php print_r($all_products_count); ?></div>
    <div class="container loading-sectn" style="text-align: center"><button class="btn" id="load_more" data-val = "2"><img style="display: none" id="loader" src="<?php echo str_replace('index.php','',base_url()) ?>loader.gif"> </button></div>


      </div> <!--/Whats_new_content-->
    </div> <!--/row-->
  </div> <!--/container-fluid-->
</div> <!--/main_content--> <!--/main_content-->
    <!-- Bootstrap core JavaScript
    ================================================== -->
<div class="abc" id="abc2">
                 
                      <?php 
$ses_var = $this->session->userdata('sr');
if($ses_var>0){
$sr=$ses_var;
}
else {
	$sr = 0;
}
		foreach ($products as $product) { 
                 ?>
      <div class="wall-column">
          <div class="wall-item">
            <div class="category <?php echo $product['category_color']; ?>">
                   <div class="category_img">
                    
                       <img src="<?php echo base_url(); ?>/<?php echo $product['product_image']; ?>" class="img-responsive">
                        <!--<div class="hover-content-cat"> -->
                        <div onclick = 'location.href = "<?php echo base_url(); ?>/product/<?php echo str_replace(" ","_",strtolower($product['product_Name'])); ?>/<?php echo $product['productId']; ?>"' class="hover-content-cat">
                         <div class="tab">
                           <div class="tab-in">
                            
                               <div class="cat-zoom-icon" id="<?php echo $product['subCategoryId']; ?>"><span class="fa fa-plus" id="<?php echo $product['productId']; ?>"></span> </div>
                               <div class="cat-plus-icon" style="display:none;" id="<?php echo $product['subCategoryId']; ?>">
                                <span class="fa fa-minus" id="<?php echo $product['productId']; ?>"> </div>
                               <div class="cat-minus-icon" style="display:none;"><span class="fa fa-minus"></span> </div>
                           
                              
                         <a href="<?php echo base_url(); ?>brand/<?php echo str_replace(" ","_",strtolower($product['brand_Name'])); ?>/<?php echo $product['brandId']; ?>"><h4><?php echo $product['brand_Name']; ?> </h4></a>
                         <a href="javascript:void(0)" class="myan follow-brand-sectn" id="<?php echo $product['brandId'];?>">
                         <button class="follow-brand-btn follow">Follow brand</button><!--<button class="follow-brand-btn follow-hover" style="display:none;">Follow brand</button> --> 
                         <button class="follow-brand-btn following" style="display:none;"><span><i class="fa fa-check"></i>
                         </span>Following</button>
                         <button class="follow-brand-btn unfollow"><span><i class="fa fa-times"> </i> </span>unfollow</button> </a>

                        
                           </div>
                         </div> <!--/tab structure -->
                         
                      </div> <!--/hover-content-cat -->
                   </div> </a> <!--/category_img--> 
                   <div class="category_content">
                     <div class="relative-struct">
                        <h1 class="hea"><a href="<?php echo base_url();?>category/<?php echo str_replace(" ","_",strtolower($product['category_Name']));?>/<?php echo $product['categoryId'];?>"><?php echo $product['category_Name'];?></a>
                         <div class="tooltip cat-follow-popup">
                          <div class="tab">
                            <div class="tab-cell">
                              <div class="img-part">  <span class="category-iconic icomoon <?php echo $product['category_color'];?> <?php echo $product['category_icon']; ?>"></span></div>
                             <div class="follow-con">
                                <img src="<?php echo base_url();?>content/images/icons/follow-arrow.png" class="arrow">
                                <h1 class=""><a href="<?php echo base_url();?>category/<?php echo str_replace(" ","-",strtolower($product['category_Name'])); ?>/<?php echo $product['categoryId'];?>"><?php echo $product['category_Name']; ?></a> </h1> 
                               <div class="follow-status" id="<?php echo $product['categoryId']; ?>"> 
                                  <a href="javascript:void(0)"><button class="follow-cat">Follow category</button>
                                 <button class="following-cat" style="display:none !important;"><span><i class="fa fa-check"></i></span>following</button></a>
                                  <button class="unfollow-cat" style="display:none;"><span><i class="fa fa-times"> </i> </span>unfollow</button></a>
                              </div>
                             </div>
                            </div>
                          </div>   
                             
                         </div>  </h1>
                     </div> <!--/relative-struct -->
                    
                    <!-- <h2>My Travel Diaries: Spellbinding Beauty Of Indonesian Archipelago Will Leave Your Jaw Dropped! </h2>-->
                   <h2><?php echo $product['productSmallDiscription']; ?></h2>
                   </div> <!--/category_content-->
                   
                   <div class="category_options">
                        <table>
                         <tr>
                           <td class="date-status <?php echo $product['category_color']; ?>"><p><?php $now = time(); $startDate=$product['created_on']; // or your date as well
                                  $your_date = strtotime($startDate);
                                 $datediff = $now - $your_date;
                                  echo floor(($datediff/(60*60*24))+1);?>days ago</p> </td>
<td class="crown-sectn <?php echo $product['category_color']; ?>" id="<?php echo $product['productId']; ?>"><span id="crowncount"><?php foreach($this->getdata->count_crown($product['productId']) as $count_no){ echo $count_no['no'];  }?> </span> <span class="icomoon icon-crown"> </span></td>
<td class="comment-sectn <?php echo $product['category_color']; ?>"><span class="text"><?php foreach($this->getdata->count_comment($product['productId']) as $count_no){ echo $count_no['no'];  }?></span> <span class="icomoon icon-chat"></span> </td>
                           <td class="fwd-icon-sectn <?php echo $product['category_color']; ?>" id="fwd-id1"><span>5 </span><span class="icomoon icon-sharing"></span> </td>
                        </tr>
                        </table>
                        <div class="fwd-social-icons sec7" style="display:none;">
                           <ul class="cat-follow-icons">
                              <li>
                              <a href="https://www.facebook.com/sharer/sharer.php?u=http%3A//lyxellabs.com/" class="fb"></a>
                             </li>
                              <li>


                             <a href="https://twitter.com/home?status=http%3A//lyxellabs.com/" class="twitter" ></a>
                              </li>
                              <li><a class="insta" href="javascript:void(0)" target="_blank"></a></li>
                              <li class="show-more"><a class="more" href="javascript:void(0)"></a></li>
                            </ul>
                            <div class="hidden-more-icons" style="display:none;">
                              <ul class="follow-icons-more">
                                <li>

<a href="https://www.linkedin.com/shareArticle?mini=true&url=http%3A//lyxellabs.com/&title=Rigalio&summary=Testing%20content&source=http%3A//lyxellabs.com/" class="linkdin"></a>
                             </li>
                                <li>
<a href="https://plus.google.com/share?url=http%3A//lyxellabs.com/" class="gplus"></a>

                               </li>
                                <li>

<a href="https://pinterest.com/pin/create/button/?url=Testing&media=https%3A//encrypted-tbn2.gstatic.com/images?q=tbn%3AANd9GcRWnEWQ-9qlQAdlD4a5sRYTWAiWblI8NMfw5bBS5buGrPdYoRxbIgQdAUY&description=Testing" class="pini"></a>
                               </li>
                              </ul>
                            </div>
                       </div> <!--/inner forward icons social -->
                       <div class="write-comment" style="display: none;" id="<?php echo $product['productId']; ?>">
                                     <?php
					    $regif = $this->session->userdata('registrationid');
						if(!$regif) 
					   {
					   ?>
                                    <span><img src="<?php echo base_url();?>content/images/icons/user.png" class="img-responsive img-circle"> </span>
                                    <?php } else{ ?>
                                     <span><img src="<?php echo base_url();?><?php echo $userdata[0]['profile_picture']; ?>" class="img-responsive img-circle"> </span>
                                    <?php }  ?>
                                    <span class="commentbox"><a href="javascript:void(0)"><input type="text" class="home_comment" name="home_comment" value=""  id="home_comment<?php echo $sr; ?>"></a> </span><a href="javascript:void(0)"><span class="icomoon icon-reply"></span> </a>
                     </div> <!--/write-comment --> 
                   </div> <!--/category_options-->
                      
         </div> <!--/category-->
    </div> <!--/wall-item -->
  </div> <!--/wall-column -->
     <?php $sr++ ; 
     }
	  $this->session->set_userdata('sr',$sr);
	 //$ses_var = $this->session->userdata('sr);
	  ?>






</div>
    <!-- Placed at the end of the document so the pages load faster -->
    
    <!--<script type="text/javascript" src="https://code.jquery.com/jquery-1.2.6.pack.js"></script>-->
    <script>
 //var page1=[];

 $(document).ready(function(e){
      //alert("hi");
      //e.preventDefault();
       // e.preventDefault();

var w=$('.abc').width();
console.log(w);
if(w>1000){
var mywidth=w/5;
console.log(mywidth);
}
//$(".abc").hide();

document.querySelector('.abc').gridify({srcNode: '.wall-column', margin: '20px', width:+mywidth+'px', resizable: true}); 

 
          // $("#load_more").data('val');
          page = $("#load_more").data('val');
          var total_product_count="<?php print_r($all_products_count);?>";
          var per_page=4;
           // alert(page);
           var total_pages=Math.ceil(parseInt(total_product_count)/parseInt(per_page));
            //alert(total_pages);
            if(page<total_pages){
              //getcountry(page);
            }
            
          });

 $(window).scroll(function(e){

  if ($(window).scrollTop() == $(document).height() - $(window).height()){
        //alert("hi");
        e.preventDefault();

//document.querySelector('.abc').gridify({srcNode: '.wall-column', margin: '20px', width:+mywidth+'px', resizable: true}); 
          // $("#load_more").data('val');
          page = $("#load_more").data('val');
          var total_product_count="<?php print_r($all_products_count);?>";
          var per_page=4;
           // alert(page);
           var total_pages=Math.ceil(parseInt(total_product_count)/parseInt(per_page));
            //alert(total_pages);
            if(page<total_pages){
              getcountry(page);
            }
            
            
            
          }
        }); 
 
 var getcountry = function(page){

   $("#loader").show();
   $('#load_more').data('val', ($('#load_more').data('val')+1));
   $.ajax({
    url:"<?php echo base_url() ?>myscroll/getCountry",
    type:'GET',
async:true,

    data: {page:page}
  }).done(function(response){
              
    //$('.wall').jaliswall({item:'.wall-item'});
var w=$('.abc').width();
console.log(w);
if(w>1000){
var mywidth=w/5;
console.log(mywidth);
}
//$(".abc").hide();

document.querySelector('.abc').gridify({srcNode: '.wall-column', margin: '20px', width:+mywidth+'px', resizable: true}); 
$(".abc").append(response).fadeIn();
  //  $("#loader").hide();
     //$(".abc").show(); 
         //scroll();
    });
  
};
 /*
    var scroll  = function(){
        $('html, body').animate({
            scrollTop: $('#load_more').offset().top
        }, 6000);
    };
    */
    
  </script>
<script type="text/javascript" src="<?php echo base_url(); ?>content/js/semantic.min.js"></script>
 <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>content/css/semantic.min.css"> 
<script type="text/javascript">
  //$('.shape').shape();
function myflip() {
  $(this).shape('flip over').delay(100);
}
  // for individual flips
$(document).ready(function() {  
  var i = 0,
      maxi = 8,// TOTAL NUMBER OF IMAGES
      timeperflip = 2000; // time PER FLIP
      setInterval(function(){
        $('.shape').each(function(i, idx){
          x(this,i*timeperflip)
        });
        
    i++;
    if(i==maxi){ i=0;}
    console.log(i)
    $('.progress-buttons .active').removeClass('active')
    $('.progress-buttons .button:eq('+i+')').addClass('active')
  },(maxi) * timeperflip)     
  function x(el, time){
      setTimeout(function(){
      $(el).shape('flip over')
    },time)
  }
  // for complete slide transitions
 $('.progress-buttons .button').click(function(){
     var i = $(this).index();
     // alert(i)
     // $('.slider').show();
     // $('.slider').shape('set next side', '.sides .side:eq('+i+')')
     $('.sides .side.active').removeClass('active');
     $('.sides .side:nth-child('+(i+1)+')').addClass('active');
	 $(this).addClass('active').siblings().removeClass('active');
     // $('.sides .side.active').removeClass('active');
     // // $('.slider').transition('flip over');
   });
});




'use strict';
Element.prototype.imagesLoaded = function (cb){
    var images = this.querySelectorAll('img');
    var count = images.length;
    if (count == 0) cb();
    for (var i= 0, length = images.length; i < length; i++)
    {
        var image = new Image();
        image.onload = function(e){
            count --;
            if (count == 0) cb()
        }
        image.onerror = function(e){
            count --;
            if (count == 0) cb()
        }
        image.src = images[i].getAttribute('src');
    }
}
Element.prototype.gridify = function (options)
{
    var self = this,
        options = options || {},
        indexOfSmallest = function (a) {
            var lowest = 0;
            for (var i = 1, length = a.length; i < length; i++) {
                if (a[i] < a[lowest]) lowest = i;
            }
            return lowest;
        },
        attachEvent = function(node, event, cb)
        {
            if (node.attachEvent)
                node.attachEvent('on'+event, cb);
            else if (node.addEventListener)
                node.addEventListener(event, cb);
        },
        detachEvent = function(node, event, cb)
        {
            if(node.detachEvent) {
                node.detachEvent('on'+event, cb);
            }
            else if(node.removeEventListener) {
                node.removeEventListener(event, render);
            }
        },
        render = function()
        {
            self.style.position = 'relative';
            var items = self.querySelectorAll(options.srcNode),
                transition = (options.transition || 'all 0.5s ease') + ', height 0, width 0',
                width = self.clientWidth,
                item_margin = parseInt(options.margin || 0),
                item_width = parseInt(options.max_width || options.width || 220),
                column_count = Math.max(Math.floor(width/(item_width + item_margin)),1),
                left = column_count == 1 ? item_margin/2 : (width % (item_width + item_margin)) / 2,
                columns = [];
            if (options.max_width)
            {
                column_count = Math.ceil(width/(item_width + item_margin));
                item_width = (width - column_count * item_margin - item_margin)/column_count;
                left = item_margin/2;
            }
            for (var i = 0; i < column_count; i++)
            {
                columns.push(0);
            }
            for (var i= 0, length = items.length; i < length; i++)
            {
                var idx = indexOfSmallest(columns);
                items[i].setAttribute('style', 'width: ' + item_width + 'px; ' +
                    'position: absolute; ' +
                    'margin: ' + item_margin/2 + 'px; ' +
                    'top: ' + (columns[idx] + item_margin/2) +'px; ' +
                    'left: ' + ((item_width + item_margin) * idx + left) + 'px; ' +
                    'transition: ' + transition);

                columns[idx] += items[i].clientHeight + item_margin;
            }
        };
    this.imagesLoaded(render);
    if (options.resizable)
    {
        attachEvent(window, 'resize', render);
        attachEvent(self, 'DOMNodeRemoved', function(){
            detachEvent(window, 'resize', render);
        })
    }
}


     
</script>
<style>
 .hover-content-cat div {
  z-index: 9999999;
 } 

.category_img img {
    position: relative !important;
    left: auto !important;
    top: auto !important;
    margin: 0 !important;
}
.awall {
width:25% !important;
}



</style>
</body>
