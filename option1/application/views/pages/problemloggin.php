<!--problem log in page -->
<div class="login-pg col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
  <div class="container-fluid">
    <div class="row">
      <div class="signin-popup-inner probsign signinvia col-lg-10 col-md-10 col-sm-11 col-xs-11 nopadding">
          <div class="problem-logging" style="padding-top:40px">
          <h3>Problem In Login </h3>
              <div class="tab">
                  <div class="tab-cell">
                   <div id="afterprblmsubmit">
                  <div class="problemloginhide">
                   <div id="problemloginmsg"></div>
                      <ul class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <li><input type="text" name="problemloginusername" id="problemloginusername" placeholder="USERNAME" class="textbox-sign"></li>
                          <li><span class="or-brk"> OR</span></li>
                          <li><input type="text" name="problemloginemail" id="problemloginemail" placeholder="PRIMARY EMAIL" class="textbox-sign"></li>
                      </ul>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                          <button onclick="return problemlogin()" class="start-btn" type="button" name="submit">
                              continue
                          </button>
                      </div>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                          <a href="<?php echo base_url(); ?>main/loginpage" class="go-back-href logging-continue">
                              <button class="continue-btn go-back-btn">go back</button>
                          </a>
                      </div>
                      
                  </div>
              </div>

          </div>
      </div> <!--/login-pg-inner -->
    </div>
  </div>  
</div>
<!--problem log in page ends -->

  </body>
</html>
<script>
    function problemlogin() {
        var username = $("#problemloginusername").val();
        var email = $("#problemloginemail").val();
		var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        //alert(username);
		if (username == '' && email== '') {
            //alert(fname);
            //alert("Please Enter username Name");
			 $("#alert-msg").text("Please Enter Username OR Primary email id to recover password");
        $(".alert-sectn").fadeIn();
        setTimeout(function () {
            $('.alert-sectn').fadeOut();
        }, 3000);
           // document.getElementById('problemloginmsg').innerHTML = "Please Enter Username OR Primary email id to recover password";
			return false;
		}
		if(email != ''){
			if (reg.test(email) == false) {
				 $("#alert-msg").text("Invalid Email Address");
        $(".alert-sectn").fadeIn();
        setTimeout(function () {
            $('.alert-sectn').fadeOut();
        }, 3000);
       // document.getElementById('problemloginmsg').innerHTML = "Invalid Email Address";
        return false;
    }
			
		}
		 
        var data = {
            "username": username,
            "email": email,
        };
        $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>main/sendEmail",
            data: data,
            //crossDomain:true,
            success: function (html) {
                var msg = html;
                //alert(msg);
                if (msg == 1) {
					 $('.problemloginhide').removeClass('problemloginhide').hide()
					 //document.getElementById('afterprblmsubmit').innerHTML = "Email sent successfully";
					   $("#alert-msg").text("Email sent successfully");
        $(".alert-sectn").fadeIn();
        setTimeout(function () {
            $('.alert-sectn').fadeOut();
        }, 3000);
                   // alert("Email sent successfully"); 
                }
				else if(msg == 2){
					$("#alert-msg").text("This user detail is not in our database");
        $(".alert-sectn").fadeIn();
        setTimeout(function () {
            $('.alert-sectn').fadeOut();
        }, 3000);
					//document.getElementById('problemloginmsg').innerHTML = "This user detail is not in our database";
					}
                else {
					$("#alert-msg").text("Error in sending Email.");
        $(".alert-sectn").fadeIn();
        setTimeout(function () {
            $('.alert-sectn').fadeOut();
        }, 3000);
					//document.getElementById('problemloginmsg').innerHTML = "Error in sending Email.";
                    //alert("Error in sending Email.");
                }
            }
        });
    }
</script>
