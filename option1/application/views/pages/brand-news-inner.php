<div class="brand-banner col-lg-12 nopadding">
  <span class="brand-bg-banner" style="background:url(<?php echo base_url();?><?php echo $brandnews_detail[0]['banner_img'];?>);"> </span>
  
</div>   
<div class="brand-inner-tag">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
     <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12 content_headline">
            <h3><?php echo $brandnews_detail[0]['headline'];?></h3>
            <hr>
        <div class="product-info col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
          <ul>
             <li><label>Brand <hr class="seprator"><span class="brand-name"><a href="<?php echo base_url(); ?>brand/<?php echo str_replace(" ", "-", strtolower($brand_details[0]['brand_Name'])); ?>/<?php echo $brand_details[0]['brandId']; ?>/legacy"><?php echo $brand_details[0]['brand_Name'];?> </a>
               <?php /*?> <div class="tooltip cat-follow-popup"> <div class="img-part"> <img src="<?php echo base_url();?>content/images/follow-specs.jpg"></div>
                     <div class="follow-con">
                        <img src="<?php echo base_url();?>content/images/icons/follow-arrow.png" class="arrow">
                        <h1>Travel and Hotels </h1> 
                        <div class="follow-status"> 
                          <a href="javascript:void(0)"><button class="follow" data-text ="Follow category"><i class="fa fa-check"></i></button></a>
                          
                        </div>
                     </div>
                    </div><?php */?>
                    
             </span> </label>
                    <div class="tooltip cat-follow-popup tooltipfirst">
                  <div class="tab">
                      <div class="tab-cell">
                          <div class="img-part"><div class="img-partdiv"><span style="background:url(<?php echo base_url();?><?php  echo $brand_details[0]['brand_image']; ?>);"></span></div></span>
                          </div>
                           <div class="follow-con">
                                                <img src="<?php echo base_url();?>content/images/icons/follow-arrow.png" class="arrow">
                                                <h1 class=""><a href="<?php echo base_url();?>brand/<?php echo str_replace(" ","-",strtolower($brand_details[0]['brand_Name'])); ?>/<?php echo $brand_details[0]['brandId'];?>/legacy"><?php  echo $brand_details[0]['brand_Name']; ?></a></h1>
                                                <div class="follow-status" id="<?php echo $brand_details[0]['brandId']; ?>">
                                                       <?php
                              $me = [];
                              foreach($user_follow as $user_foll){
                                
                                  $me[] = $user_foll['brand_id'];
                              }
                              $flag=1;
                              for($i=0;$i<count($me);$i++){
                              if($brand_details[0]['brandId'] ==$me[$i]){
                                $flag=0;
                                break;
                              }
                              }
                              if($flag==0)
                              {?>
                                <button class="follow-brand-btn following" data-text="Follow brand"><i class="fa fa-check"></i></button>
                                <?php }
                  else { ?>
                              <button class="follow-brand-btn follow" data-text="Follow brand"><i class="fa fa-check"></i></button>
                                <!--<button class="follow-brand-btn follow-hover" style="display:none;">Follow brand</button> -->
                  <?php } ?>
                            </div>


                        </div>
                      </div>
                  </div>
                  
              </div>
</li>
             <li><label>Posted in<span><?php echo $brandnews_detail[0]['type'];?> </span> </label> </li>
          </ul>
        </div> <!--/product-info -->  
      </div>
      
  </div>      
</div> <!--/product_pg_tagline -->
<div class="basic-cat-info col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="basic-cat-info-inner col-lg-10 col-md-10 col-sm-12 col-xs-12">
       <div class="col-lg-6 col-sm-12 breadcrumb"> 
          <a href="<?php echo base_url(); ?>brand/<?php echo str_replace(" ","-",strtolower($brand_details[0]['brand_Name'])); ?>/<?php echo $brand_details[0]['brandId']; ?>/latest-news"><span>Back to News </span> </a>
       </div>
       <div class="col-lg-6 col-sm-12 col-xs-12 follow-list">
           <table>
              <tr>
                <td><?php 
        $startDate=$brandnews_detail[0]['created_on']; // or your date as well
              // $your_date = strtotime($startDate);
         $date_exploded = explode(' ',$startDate);
         $explode = explode('-',$date_exploded[0]);
        echo $explode[2].'/'.$explode[1].'/'.$explode[0];
        ?> </td>
                <td class="crown-news" id="<?php echo $brandnews_detail[0]['news_id']; ?>"><span class="text" id="crowncount"><?php foreach($this->getdata->count_crown_news($brandnews_detail[0]['news_id']) as $count_no){ echo $count_no['no'];  }?> </span> <span class="icomoon icon-crown"> </span></td>
                <td class="fwd-icon-sectn"> <span> </span> <span class="icomoon icon-sharing"></span></td>
                <td class="saveas-icon"><span class="icomoon icon-save-latr cat_auto"><i class="fa fa-plus" style="display: none;"></i><i class="fa fa-check"></i></span></td>
              </tr>
           </table>
                       <div class="fwd-social-icons sec7" style="display:none;">
                <ul class="cat-follow-icons">
                    <li>
                        <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url();?>main/brandnews/<?php echo $brand_details[0]['brandId'];?>/<?php echo $brandnews_detail[0]['news_id'];?>" class="fb"></a>
                    </li>
                    <li>

                        <a target="_blank" href="https://twitter.com/home?status=<?php echo base_url();?>main/brandnews/<?php echo $brand_details[0]['brandId'];?>/<?php echo $brandnews_detail[0]['news_id'];?>" class="twitter" ></a>
                    </li>
                    <?php /*?> <li><a class="insta" href="javascript:void(0)" target="_blank"></a></li><?php */?>
                    <li class="show-more"><a class="more" href="javascript:void(0)"></a></li>
                </ul>
                <div class="hidden-more-icons" style="display:none;">
                    <ul class="follow-icons-more">
                        <li>

                            <a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo base_url();?>main/brandnews/<?php echo $brand_details[0]['brandId'];?>/<?php echo $brandnews_detail[0]['news_id'];?>&title=Rigalio&summary=Rigalio%20content&source=<?php echo base_url();?>main/brandnews/<?php echo $brand_details[0]['brandId'];?>/<?php echo $brandnews_detail[0]['news_id'];?>" class="linkdin"></a>
                        </li>
                        <li>
                            <a target="_blank" href="https://plus.google.com/share?url=<?php echo base_url();?>main/brandnews/<?php echo $brand_details[0]['brandId'];?>/<?php echo $brandnews_detail[0]['news_id'];?>" class="gplus"></a>

                        </li>
                        <li>

                            <a target="_blank" href="https://pinterest.com/pin/create/button/?url=Testing&media=<?php echo base_url();?>main/brandnews/<?php echo $brand_details[0]['brandId'];?>/<?php echo $brandnews_detail[0]['news_id'];?>&description=Testing" class="pini"></a>
                        </li>
                    </ul>
                </div>
            </div> <!--/inner forward icons social -->
       </div>
       <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 cat-navigation"></div>
    </div>  <!--/basic-cat-info-inner -->      
</div>  <!--/basic-cat-info -->
<div class="brand-news-inner col-lg-12 nopadding">
  <div class="news-brief col-lg-8 col-sm-11 col-xs-12">
    <p><?php echo $brandnews_detail[0]['newslong_desc'];?></p>
    <a target="_blank" href="<?php echo $brandnews_detail[0]['news_link']; ?> "><button class="read-morenews-btn"><?php echo $brandnews_detail[0]['button_name'];?></button></a>
  </div> <!--/news-brief -->

  <div class="related-news col-lg-12 nopadding">
    <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12 content_headline2"><h2>related news</h2><hr>
    </div>
    <div class="col-lg-10 col-sm-11 col-xs-12 related-news-box wall whats_new_content">
      <div class="grid" id="masonry-grid">
       <?php
              foreach ($brandnews_type as $news) {
                            ?>
                            <div class="wall-column grid-item">
                  <div class="wall-item">
                    <div class="category">
                     <a href="<?php echo base_url();?>main/brandnews/<?php echo $news['brand_id'];?>/<?php echo $news['news_id'];?>">
                      <div class="category_img">
                     
                       <img src="<?php echo base_url();?><?php echo $news['news_img'];?>" class="img-responsive">
                      
                      </div> <!--/category_img-->
                       </a>
                      <div class="category_content">
                       <div class="news-cat"><span><?php echo $news['type'];?> </span> </div>
                       <a href="<?php echo base_url();?>main/brandnews/<?php echo $news['brand_id'];?>/<?php echo $news['news_id'];?>"><h3 class="news-headline"><?php echo $news['headline'];?></h3></a>
                       <a href="<?php echo base_url();?>main/brandnews/<?php echo $news['brand_id'];?>/<?php echo $news['news_id'];?>"><h2><?php echo $news['news_small_desc'];?> </h2></a>
                      </div> <!--/category_content-->
                      <div class="news-links">
                        <a href="<?php echo base_url(); ?>brand/<?php echo str_replace(" ","-",strtolower($news['brand_Name'])); ?>/<?php echo $news['brand_id']; ?>"><span class="brand-name"><?php echo $news['brand_Name'];?></span></a> <a target="_blank" href="<?php echo $news['news_link'];?>"><span class="read-more">Read More </span></a>
                      </div>
                      <div class="category_options">
                          <table>
                             <tbody>
                              <tr>
                               <td class="date-status"><p><?php $now = time(); $startDate=$news['created_on']; // or your date as well
                                                            $your_date = strtotime($startDate);
                                                            $datediff = $now - $your_date;
                                                            echo floor(($datediff/(60*60*24))+1);?> days ago</p> </td>
                               <td class="crown-news" id="<?php echo $news['news_id']; ?>"><span class="text" id="crowncount"><?php foreach($this->getdata->count_crown_news($news['news_id']) as $count_no){ echo $count_no['no'];  }?></span><span class="icomoon icon-crown"> </span> </td>
                               <td class="fwd-icon-sectn"><span class="text"> </span><span class="icomoon icon-sharing"></span> </td>
                             </tr>
                            </tbody>
                          </table>
                          <div style="display:none;" class="fwd-social-icons sec7">
                             <ul class="cat-follow-icons">
                                <li><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url();?>main/brandnews/<?php echo $news['brand_id'];?>/<?php echo $news['news_id'];?>" class="fb"></a></li>
                                <li><a target="_blank" href="href="https://twitter.com/home?status=<?php echo base_url();?>main/brandnews/<?php echo $news['brand_id'];?>/<?php echo $news['news_id'];?>" class="twitter"></a></li>
                               <?php /*?> <li><a target="_blank" href="javascript:void(0)" class="insta"></a></li><?php */?>
                                <li class="show-more"><a href="javascript:void(0)" class="more"></a></li>
                              </ul>
                              <div style="display:none;" class="hidden-more-icons">
                                <ul class="follow-icons-more">
                                  <li><a target="_blank" class="linkdin" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo base_url();?>main/brandnews/<?php echo $news['brand_id'];?>/<?php echo $news['news_id'];?>&title=Rigalio&summary=Rigalio%20content&source=<?php echo base_url();?>main/brandnews/<?php echo $news['brand_id'];?>/<?php echo $news['news_id'];?>"> </a> </li>
                                  <li><a target="_blank" class="gplus" href="https://plus.google.com/share?url=<?php echo base_url();?>main/brandnews/<?php echo $news['brand_id'];?>/<?php echo $news['news_id'];?>"> </a> </li>
                                  <li><a target="_blank" class="pini" href="https://pinterest.com/pin/create/button/?url=Testing&media=<?php echo base_url();?>main/brandnews/<?php echo $news['brand_id'];?>/<?php echo $news['news_id'];?>&description=Testing"> </a> </li>
                                </ul>
                              </div>
                          </div> <!--/inner forward icons social -->
                          <div style="display: none;" class="write-comment">
                              
                              <div class="few-comm">
                                <table>
                                  <tr> <td><span class="pf-img" style="background:url(content/images/timeline/profile-pic-small.png);"></span></td> <td class="post-com5">Is this better than Audi </td> </tr>
                                  <tr> <td><span class="pf-img" style="background:url(content/images/timeline/profile-pic-small.png);"></span> </td> <td class="post-com5">Is this better than Audi </td> </tr>
                                  <tr> <td><span class="pf-img" style="background:url(content/images/timeline/profile-pic-small.png);"></span></td> <td class="post-com5">Is this better than Audi </td> </tr>
                                </table>
                              </div>
                              <div class="see-all-com"> see all  </div>
                              <span class="pf-img" style="background:url(content/images/timeline/profile-pic-small.png);"></span>
                              <span class="commentbox"><a><input type="text" placeholder="Write a Comment" value="" name=""></a> </span><a href="#"><span class="icomoon icon-reply"></span> </a>

                          </div> <!--/write-comment -->
                       </div><!--/category_options-->
                    </div> <!--/category-->
                  </div> <!--/wall-item -->
                </div>
                            
         <!--/wall-column -->
<?php }
?>
      </div>
    </div>
  </div> <!--/related-news -->
  <?php if(count($products)!='0'){ ?>
  <div class="related-products col-lg-12 nopadding">
    <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12 content_headline2"><h2>related products</h2><hr>
    </div>
    <div class="col-lg-10 col-sm-11 col-xs-12 related-prod-box wall whats_new_content">
      <div class="grid" id="masonry-grid1">
          <?php
                        $sr = 0;
                        foreach ($products as $product) {
                            ?>
                            <div class="wall-column grid-item">
                                <div class="wall-item">
                                    <div class="category <?php echo $product['category_color']; ?>">
                                        
                                        <div class="category_img" data-url="<?php echo base_url(); ?>product/<?php echo str_replace(" ","_",strtolower($product['product_Name'])); ?>/<?php echo $product['productId']; ?>/overview">
                                            <img src="<?php echo base_url(); ?><?php echo $product['product_image']; ?>" class="img-responsive">
                                           <div class="hover-content-cat" data-url="<?php echo base_url(); ?>product/<?php echo str_replace(" ","_",strtolower($product['product_Name'])); ?>/<?php echo $product['productId']; ?>/overview">
                                                 <?php
                                               $regid = $this->session->userdata('registrationid');
                                               if($regid !='')
                                               { ?>
                                                   <div class="del-area">
                                                       <span class="hide-post"><i class="fa fa-angle-down"></i></span>
                                                       <div class="del-button hide-product" id="<?php echo $product['productId']; ?>">Hide Post </div>

                                                   </div>
                                               <?php } ?>


                                                    <div class="tab">
                                                        <div class="tab-in">
                                                         <?php  $me = [];
                              foreach($user_saveforlater as $saveforlater){
                                
                                  $me[] = $saveforlater['product_id'];
                              }
                              $flag=1;
                              for($i=0;$i<count($me);$i++){
                              if($product['productId'] ==$me[$i]){
                                $flag=0;
                                break;
                              }
                              }
                              if($flag==0)
                              {?>
                                                            <div class="cat-zoom-icon" style="display:none;" id="<?php echo $product['subCategoryId']; ?>"><span class="fa fa-plus" id="<?php echo $product['productId']; ?>"></span> </div>
                                                            <div class="cat-plus-icon" id="<?php echo $product['subCategoryId']; ?>">
                                                                <span class="fa fa-minus" id="<?php echo $product['productId']; ?>"></div>
                                                            <div class="cat-minus-icon" style="display:none;"><span class="fa fa-minus"></span></div>
                                                               <?php } else{ ?>
                                                               <div class="cat-zoom-icon" id="<?php echo $product['subCategoryId']; ?>"><span class="fa fa-plus" id="<?php echo $product['productId']; ?>"></span> </div>
                                                            <div class="cat-plus-icon" style="display:none;" id="<?php echo $product['subCategoryId']; ?>">
                                                                <span class="fa fa-minus" id="<?php echo $product['productId']; ?>"></div>
                                                            <div class="cat-minus-icon" style="display:none;"><span class="fa fa-minus"></span></div>
                                                              <?php } ?>
                                                            <h4><a href="<?php echo base_url(); ?>brand/<?php //echo str_replace(" ","-",strtolower($product['brand_Name'])); ?>/<?php echo $product['brandId']; ?>/legacy"><?php echo $product['brand_Name']; ?></a></h4>
                                                            <a href="javascript:void(0)" class="myan follow-brand-sectn" id="<?php echo $product['brandId'];?>">
                                                 <a href="javascript:void(0)" class="myan follow-brand-sectn" id="<?php echo $product['brandId'];?>">
                                                            <?php
                              $me = [];
                              foreach($user_follow as $user_foll){
                                
                                  $me[] = $user_foll['brand_id'];
                              }
                              $flag=1;
                              for($i=0;$i<count($me);$i++){
                              if($product['brandId'] ==$me[$i]){
                                $flag=0;
                                break;
                              }
                              }
                              if($flag==0)
                              {?>
                                                            <button class="follow-brand-btn following" data-text = "Follow brand"><i class="fa fa-check"></i></button> </a>
                                                            <?php }
                              else { ?>
                                                            <button class="follow-brand-btn follow" data-text = "Follow brand"><i class="fa fa-check"></i></button> </a>
                              <?php } ?>
                                                        </div>  <!--/tab-in -->
                                                    </div> <!--/tab -->
                                              
                                            </div> <!--/hover-content-cat -->
                                        </div>
                                        </a>
                                        <div class="category_content">
                                            <div class="relative-struct">
                                                <h1 class="hea"><a href="<?php echo base_url(); ?>category/<?php echo str_replace(" ","-",strtolower($product['category_Name']));?>/<?php echo $product['categoryId'];?>"><?php echo $product['category_Name'];?></a>
                                                    <div class="tooltip cat-follow-popup">
                                                        <div class="tab">
                                                            <div class="tab-cell">
                                                                <div class="img-part"><span class="category-iconic icomoon <?php echo $product['category_color'];?> <?php echo $product['category_icon']; ?>"></span></div>
                                                                <div class="follow-con">
                                                                    <img src="<?php echo base_url();?>content/images/icons/follow-arrow.png" class="arrow">
                                                                    <h1 class=""><a href="<?php echo base_url(); ?>category/<?php echo str_replace(" ","-",strtolower($product['category_Name']));?>/<?php echo $product['categoryId'];?>"><?php echo $product['category_Name']; ?></a> </h1>
                                                                    <div class="follow-status" id="<?php echo $product['categoryId']; ?>">
                                                                          <?php
                              $me = [];
                              foreach($user_follow as $user_foll){
                                
                                  $me[] = $user_foll['category_id'];
                              }
                              $flag=1;
                              for($i=0;$i<count($me);$i++){
                              if($product['categoryId'] ==$me[$i]){
                                $flag=0;
                                break;
                              }
                              }
                              if($flag==0)
                              {?>
                                                                        <button data-text="Follow category"
                                                                                class="follow-cat-btn normal following-cat">
                                                                            <i class="fa fa-check"></i></button>
                                                                           <?php }
                              else { ?>  
                                                            <button data-text="Follow category"
                                                                                class="follow-cat-btn normal follow-cat">
                                                                            <i class="fa fa-check"></i></button>
                                                                            <?php } ?>
                                                                    </div>
                                                                </div>


                                                            </div>
                                                        </div>
                                                    </div> </h1>
                                            </div> <!--/relative-struct -->

                                            <a href="<?php echo base_url(); ?>product/<?php echo str_replace(" ","_",strtolower($product['product_Name'])); ?>/<?php echo $product['productId']; ?>/overview"><h2><?php echo $product['productSmallDiscription']; ?></h2></a>
                                        </div> <!--/category_content-->
                                        <?php
                    if($this->session->userdata('registrationid')){
                                        $count_people=[];
                                        $comment_people = [];
                                        foreach($this->getdata->crownuser_detail($product['productId']) as $count_no)
                                        {
                                            $count_people[]=$count_no;
                                        }
                                        foreach($this->getdata->commentuser_detail($product['productId']) as $countcomment_no)
                                        {
                                            $comment_people[]=$countcomment_no;
                                        }

                                        $sum_total = count($count_people) + count($comment_people);
                                        if ($sum_total != '0')
                                        {
                                            ?>

                                            <div class="hidden-status">
                                                <h6><?php
                                                    //print_r($count_people);
                                                    $m=[];
                                                    foreach($count_people as $me){

                                                        $m[]=$me['user_id'];
                                                    }

                                                    $m1=[];
                                                    foreach($comment_people as $me){

                                                        $m1[]=$me['user_id'];
                                                    }
                                                    // echo count(array_unique(array_merge($m,$m1)));
                                                    //print_r(array_merge($count_people,$comment_people));
                                                    //$array1 = array_merge ($count_people,$comment_people);
                                                    //$array2 = array_unique($array1);
                                                    //$array3 = array_keys($array2);
                                                    //print_r($array2);
                                                    echo count(array_unique(array_merge($m,$m1)));;  ?> People Reacted On This </h6>  <span class="profile-angle"><i class="fa fa-angle-down"></i></span></div>
                                        <?php  } }?>

                                        <div class="category_options">
                                            <table>
                                                <tr>
                                                    <td class="date-status <?php echo $product['category_color']; ?>"><p><?php $now = time(); $startDate=$product['created_on']; // or your date as well
                                                            $your_date = strtotime($startDate);
                                                            $datediff = $now - $your_date;
                                                            echo floor(($datediff/(60*60*24))+1);?> days ago</p> </td>
                                                             <?php
                              $me = [];
                              foreach($user_crown as $user_foll){
                                
                                  $me[] = $user_foll['product_id'];
                              }
                              $flag=1;
                              for($i=0;$i<count($me);$i++){
                              if($product['productId'] ==$me[$i]){
                                $flag=0;
                                break;
                              }
                              } 
                              if($flag==0)
                              {?>
                                                             <td class="crown-sectn active <?php echo $product['category_color']; ?>" id="<?php echo $product['productId']; ?>"><span id="crowncount"><?php foreach($this->getdata->count_crown($product['productId']) as $count_no){ echo $count_no['no'];  }?> </span> 
                                                             <?php } 
                                                           else { ?>
                                                    <td class="crown-sectn <?php echo $product['category_color']; ?>" id="<?php echo $product['productId']; ?>"><span id="crowncount"><?php foreach($this->getdata->count_crown($product['productId']) as $count_no){ echo $count_no['no'];  }?> </span>
                                                    <?php } ?>
                                                     <span class="icomoon icon-crown"> </span></td>
                                                    <td class="comment-sectn <?php echo $product['category_color']; ?>">
                                                   <a href="<?php echo base_url(); ?>product/<?php echo str_replace(" ", "_", strtolower($product['product_Name'])); ?>/<?php echo $product['productId']; ?>/overview#comment"> <span class="text"><?php foreach($this->getdata->count_comment($product['productId']) as $count_no){ echo $count_no['no'];  }?> </span> 
                                                    <span class="icomoon icon-chat"> </span></a> </td>
                                                    <td class="fwd-icon-sectn <?php echo $product['category_color']; ?>" id="fwd-id1"><span> </span> <span class="icomoon icon-sharing"></span> </td>
                                                </tr>
                                            </table>

                                            <div class="fwd-social-icons sec7" style="display:none;">
                                                <ul class="cat-follow-icons">
                                                    <li>
                                                        <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url();?>product/<?php echo str_replace(" ","_",strtolower($product['product_Name']));?>/<?php echo $product['productId'];?>/overview" class="fb"></a>
                                                    </li>
                                                    <li>


                                                        <a target="_blank" href="https://twitter.com/home?status=<?php echo base_url();?>product/<?php echo str_replace(" ","_",strtolower($product['product_Name']));?>/<?php echo $product['productId'];?>/overview" class="twitter" ></a>
                                                    </li>
                                                    <?php /*?><li><a class="insta" href="javascript:void(0)" target="_blank"></a></li><?php */?>
                                                    <li class="show-more"><a class="more" href="javascript:void(0)"></a></li>
                                                </ul>
                                                <div class="hidden-more-icons" style="display:none;">
                                                    <ul class="follow-icons-more">
                                                        <li>

                                                            <a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo base_url();?>product/<?php echo str_replace(" ","_",strtolower($product['product_Name']));?>/<?php echo $product['productId'];?>/overview&title=Rigalio&summary=Rigalio%20content&source=<?php echo base_url();?>product/<?php echo str_replace(" ","_",strtolower($product['product_Name']));?>/<?php echo $product['productId'];?>/overview" class="linkdin"></a>
                                                        </li>
                                                        <li>
                                                            <a target="_blank" href="https://plus.google.com/share?url=<?php echo base_url();?>product/<?php echo str_replace(" ","_",strtolower($product['product_Name']));?>/<?php echo $product['productId'];?>/overview" class="gplus"></a>

                                                        </li>
                                                        <li>

                                                            <a target="_blank" href="https://pinterest.com/pin/create/button/?url=Testing&media=<?php echo base_url();?>product/<?php echo str_replace(" ","_",strtolower($product['product_Name']));?>/<?php echo $product['productId'];?>/overview&description=Testing" class="pini"></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div> <!--/inner forward icons social -->

                                        </div> <!--/category_options-->

                                    </div> <!--/category-->
                                </div> <!--/wall-item -->
                            </div> <!--/wall-column -->
                            <?php $sr++ ;
                        }
                        $this->session->set_userdata('sr',$sr);
                        //$ses_var = $this->session->userdata('sr);
                        ?>
      </div>
    </div>
  </div> <!--/related-products -->
  <?php } ?>
</div> <!-- /brand-news-inner-->


  </body>
  <script type="text/javascript">
 $('#masonry-grid1').masonry({
  // options
  itemSelector: '.grid-item',
  //columnWidth: 200
percentPosition: true
});

 $('#masonry-grid').masonry({
  // options
  itemSelector: '.grid-item',
  //columnWidth: 200
percentPosition: true
});
</script>
 <script src="<?php echo base_url(); ?>content/js/hover.js"></script>
 
</html>
