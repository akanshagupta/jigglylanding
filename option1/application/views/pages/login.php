
<!--log in page -->
<div class="login-pg col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
  <div class="container-fluid">
    <div class="row">
      <div class="login-pg-inner col-lg-10 col-md-10 col-sm-11 col-xs-11 nopadding">
          <div class="signin-popup">
            <div class="signin-popup-inner">
                <div class="tab">
                    <div class="tab-cell">
                        <div class="signinvia col-lg-8 col-md-8 col-sm-12 col-xs-12">
                        
                          <div class="registered-user current">
                            <h3>Log In </h3>
                                <div id="loginalert"></div>
                                <form method="post" id="myloginform" name="myloginform">
                                    <ul class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <li><input type="text" name="loginusername" id="loginusername" placeholder="USERNAME" class="textbox-sign"></li>
                                        <li><input type="password" name="loginpassword" id="loginpassword" placeholder="PASSWORD" class="textbox-sign"></li>
                                    </ul>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                                        <a href="<?php echo base_url(); ?>main/resetpasspage"><span class="problem-btn">Problem logging in? </span></a>
                                        <button onclick="return loginvalidation()" class="start-btn" type="button" name="submit">continue
                                        </button>
                                    </div>
                                </form>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                                        <a href="<?php echo base_url(); ?>main/requestinvitepage"><button class="continue-btn">request an invite</button></a>
                                  
                                </div>
                            </div> <!--/registered-user -->
                            
                        </div>
                    </div>
                </div>
            </div>
</div>
      </div> <!--/login-pg-inner -->
    </div>
  </div>  
</div>
<!--log in page ends -->

  </body>

</html>
<script>

 $(document).on('keydown',"#loginpassword",function(e){
   // $(".textbox-sign").keypress(function(e){
        if(e.which == 13){//Enter key pressed

           // alert("hi");
            loginvalidation(); //Trigger search button click event
        }
    });
	
	
function loginvalidation() {
        var username = $("#loginusername").val();
        var password = $("#loginpassword").val();
        if (username == '') {
          //  alert(fname);
            //alert("Please Enter username Name");
			 $("#alert-msg").text("Please Enter User Name");
        $(".alert-sectn").fadeIn();
        setTimeout(function () {
            $('.alert-sectn').fadeOut();
        }, 3000);
            //document.getElementById('loginalert').innerHTML = "Please Enter User Name";
            //document.myform.username.focus();
            return false;
        }

        if (password == '') {
            //alert(fname);
            //alert("Please Enter username Name");
			 $("#alert-msg").text("Please Enter password");
        $(".alert-sectn").fadeIn();
        setTimeout(function () {
            $('.alert-sectn').fadeOut();
        }, 3000);
           // document.getElementById('loginalert').innerHTML = "Please Enter password";
           // document.myform.password.focus();
            return false;
        }
        if (username || password) {
            var data = {
                "username": username,
                "password": password
            };

            $.ajax({
                type: "POST",
                url: "<?php echo base_url();?>main/inviteuserlogin",
                // url: "http://localhost/finalrigalio/main/inviteuserlogin?username=" + username + "&password=" + password,
                data: data,
                //crossDomain:true,
                success: function (html) {
                    //alert(html);
                    var inviteuser = html;
                    if (inviteuser == 1) {
                        //alert(html);
                        //window.location.href='http://localhost/finalrigalio/main/signup';
                        window.location.href = '<?php echo base_url(); ?>home';
                    }
                    else if (inviteuser == 2) {
						 $("#alert-msg").text("User Name or Password are Incorrect !");
        $(".alert-sectn").fadeIn();
        setTimeout(function () {
            $('.alert-sectn').fadeOut();
        }, 3000);
                        //document.getElementById('loginalert').innerHTML = "User Name or Password are Incorrect !";
                        // document.getElementById('myhide').style.display="none";
                        //window.location.href='http://localhost/finalrigalio/home';
                        //	alert(html);
                    }
                    else if (inviteuser == 3) {
                        //alert(html);
                        window.location.href = '<?php echo base_url(); ?>home';
                    }
                    else if (inviteuser == 4) {
                        //alert(html);
						 $("#alert-msg").text("Invalid username and password");
        $(".alert-sectn").fadeIn();
        setTimeout(function () {
            $('.alert-sectn').fadeOut();
        }, 3000);
                      //  document.getElementById('loginalert').innerHTML = "Invalid username and password";
                    }
                    else if (inviteuser == 5) {
                        //alert(html);
                        window.location.href = '<?php echo base_url(); ?>main/signup';
                    }

                }
            });
            return false;
        } else {
			 $("#alert-msg").text("Please Enter All fields");
        $(".alert-sectn").fadeIn();
        setTimeout(function () {
            $('.alert-sectn').fadeOut();
        }, 3000);
           // document.getElementById('loginalert').innerHTML = "Please Enter All fields";
            return false;
        }

    }
</script>
