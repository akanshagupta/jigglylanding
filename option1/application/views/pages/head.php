<!DOCTYPE html>
<html lang="en">
<head>
    <script type="text/javascript" src="https://platform.linkedin.com/in.js">
        api_key: 756jk5lm9azphk
        //scope: r_basicprofile r_emailaddress r_contactinfo
        onLoad: onLinkedInLoad
        authorize: true
    </script>
    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
    <meta name="description" content="<?php if ($seo) {
        echo $seo[0]['title'];
    } ?>">
    <meta name="author" content="">
    <meta name="keyword" content="<?php if ($seo) {
        echo $seo[0]['keywords'];
    }
else if($this->uri->segment(1)=='product'){  
  echo $prod_cat[0]['keywords'];

} ?>">


   <title><?php if ($seo) {
            echo $seo[0]['title'];
        }
else{

if($this->uri->segment(1)=='product'){
			echo $prod_cat[0]['product_Name']." ". ucfirst($this->uri->segment(4))." - "."Rigalio";
}else {

//echo $this->uri->segment(2)." ".$this->uri->segment(4)." - "."Rigalio";
echo "Rigalio";

}
		} ?></title>
    <?php if (1) { ?>
        <script src="<?php echo base_url(); ?>content/js/jquery-1.9.1.min.js"></script>
        <script src="<?php echo base_url(); ?>content/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>content/js/masonry.pkgd.min.js"></script>

        <script src="<?php echo base_url(); ?>content/js/bootstrap-hover-dropdown.js"></script>
        <script src="<?php echo base_url(); ?>content/js/mycustom.js"></script>


    <link rel="stylesheet" href="<?php echo base_url(); ?>content/css/font-awesome.min.css">
    <link href="<?php echo base_url(); ?>content/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>content/css/header.css"/>
    <link href="<?php echo base_url(); ?>content/css/category.css" rel="stylesheet">

    <link href="<?php echo base_url(); ?>content/css/style.css" rel="stylesheet">
    <?php } else{ ?>
        <script src="<?php echo base_url(); ?>content/js/output.min.js"></script>

    <link rel="stylesheet" href="<?php echo base_url(); ?>content/css/output.min.css">
    <?php } ?>


    <!-- Custom styles for this template -->
    <link href="<?php echo base_url(); ?>content/css/carousel.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>content/css/jquery-ui.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>content/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>content/css/font-awesome.css">
    <script src="<?php echo base_url(); ?>content/js/jquery-ui.js"></script>
    <script src="<?php echo base_url(); ?>content/js/classie.js"></script>
    <link href="<?php echo base_url(); ?>content/css/timeline.css" rel="stylesheet">
    <link rel="icon" type="image/png" href="<?php echo base_url(); ?>content/images/favicon.png">
    <script type="text/javascript" src="<?php echo base_url(); ?>/content/js/fblogin.js"></script>
<meta name="google-site-verification" content="ogF45Qvh8qsdS1X02QCV0LqqHeobclmGJXMZi-Ug0CY" />
<script id="lyxel-dmp-09123" src="//iodata.lyxellabs.com/basic.js" ></script>
<?php if($this->uri->segment(1)=='product') {
	
	//print_r($prod_cat);
//print_r($products);
 // echo $prod_cat[0]['productlongDiscription'];

	
	 ?>
<!--fb-->
<meta property="og:title" content="<?php echo $prod_cat[0]['product_Name']; ?>" />
<meta property="og:type" content="product" />
<?php foreach($prod_image as $prod_image1) { ?>
<meta property="og:image" content="<?php echo base_url().$prod_image1['image_Name']; ?>" />
<?php } ?>
<meta property="og:url" content=" https://www.facebook.com/rigalio " />
<meta property="og:description" content="<?php echo $prod_cat[0]['productlongDiscription']; ?>" />

<!--google -->
    <meta property="og:title" content="<?php echo $prod_cat[0]['product_Name']; ?>" />
    <meta property="og:type" content="website" />
<?php foreach($prod_image as $prod_image1){ ?>
    <meta property="og:url" content="<?php echo base_url().$prod_image1['image_Name']; ?>" />
<?php } ?>
<meta property="og:image" content="http://rigalio.com/content/images/main-logo.png" />
    <meta property="og:description" content="<?php echo $prod_cat[0]['productlongDiscription']; ?>" />
    <meta property="og:site_name" content=" Rigalio.com" />
    
    <!--twitter-->
    
    <meta name="twitter:card" content="website">
    <meta name="twitter:site" content="<?php echo $prod_cat[0]['product_Name']; ?>">
    <meta name="twitter:title" content="<?php echo $prod_cat[0]['product_Name']; ?>"/>
    <meta name="twitter:description" content="<?php echo $prod_cat[0]['productlongDiscription']; ?>"/>
    <meta name="twitter:creator" content="@RigalioLuxury ">
<?php foreach($prod_image as $prod_image1) { ?>
    <meta name="twitter:image:src" content="<?php echo base_url().$prod_image1['image_Name']; ?>">
<?php } ?>

<?php } else if($this->uri->segment(1)=='brand'){ ?>



<?php } ?>
</head>
<!-- NAVBAR

================================================== -->
<body>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-80647225-1', 'auto');
    ga('send', 'pageview');

</script>
<div class="static-sectn">
    <div class="filter-icon"><a href="javascript:void(0)"><img
                src="<?php echo base_url(); ?>content/images/icons/filter_white.png"> </a></div>
    <div class="goto-top-icon"><a href="javascript:void(0)" class="scrollup"><img
                src="<?php echo base_url(); ?>content/images/icons/go-to-top_white.png"> </a></div>
</div>
<div id="fb-root"></div>
<div id="myid"
     style="display:none;"><?php echo $userregistrationid = $this->session->userdata('registrationid'); ?></div>
<div class="header-collapse-btn">
    <a href="javascript:void(0)" class="popup-btn" id="nav-icon3">
        <span> </span> <span></span> <span></span> <span> </span>
    </a>
</div>
<nav class="navbar navbar-inverse navbar-fixed-top" id="header_nav">
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
    </div>
    <div class="main-logo col-lg-6 col-md-6 col-sm-6 col-xs-6">
        <a href="<?php echo base_url(); ?>"> <img src="<?php echo base_url(); ?>content/images/main-logo.png"
                                                  class="img-responsive"> </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 drop-search-wrap navbar-right">
        <ul>
            <li><a href="javascript:void(0)" class="search-icon"> </a></li>
            <?php
            $userregistrationid = $this->session->userdata('registrationid');

            if (!$userregistrationid) { ?>

                <li><a href="javascript:void(0)" class="signin-popup-btn"> </a>
                </li>
            <?php } else {
                if ($userdata[0]['profile_picture'] == '') {
                    ?>
                    <li><a href="<?php echo base_url(); ?>home" class="userhome">
                        </a></li>
                    <li><a href="javascript:void(0)" class="settings-popup-btn">

                        </a></li>

                <?php } else { ?>
                    <li><a href="<?php echo base_url(); ?>home" class="userhome">
                        </a></li>
                    <li><a href="javascript:void(0)" class="settings-popup-btn"><span class="pg-img"
                                                                                      style="background: url(<?php echo base_url(); ?><?php echo $userdata[0]['profile_picture']; ?>);"> </span>
                        </a></li>

                <?php }
            } ?>
        </ul>

    </div>

    <div class="search-hidden col-lg-12 col-md-12 col-sm-12 col-xs-12 dropdown-search " style="display:none;">
        <form id="searchform" role="search" name="search" method="post" action="<?php echo base_url(); ?>search">
            <input type="text" onblur="search();" name="serach_text" id="serach_text" class="search-field form-control"
                   placeholder="I am Looking for">
        </form>
    </div>
    <div class="alert-sectn" id="alert-sectn" style="display: none;">
        <h4 id="alert-msg"></h4>
    </div>
</nav>
<div class="popup" id="popup" style="display: none;">
    <div class="popup-inner">
        <!--<a href="javascript:void(0)" class="close">CLOSE</a>-->
        <div class="tab">
            <div class="tab-cell">
                <div class="main-menu">
                    <ul class="main">

                        <?php

                        $srm = 0;
                        foreach ($avali_cat as $category) {
                            ?>

                            <li class="<?php if ($srm == 0) {
                                echo "active";
                            } ?>">
                                <a href="javascript:void(0);" class="btn"><?php echo $category['category_Name']; ?></a>
                                <ul class="dropdown-menu submenu">
                                    <?php foreach ($this->getdata->get_avail_subcategory($category['categoryId']) as $sub_cat) {
                                        ?>
                                        <li>
                                            <a href="<?php echo base_url(); ?>subcategory/<?php echo str_replace(" ", "-", strtolower($sub_cat['subCategory_Name'])); ?>/<?php echo $sub_cat['subCategoryId']; ?>"><span
                                                    class="icomoon <?php echo $sub_cat['headersubcat_icon']; ?>"></span>
                                                <div
                                                    class="subcat-name"><?php echo $sub_cat['subCategory_Name']; ?></div>
                                            </a></li>
                                    <?php } ?>
                                    <a href="<?php echo base_url(); ?>category/<?php echo str_replace(" ", "-", strtolower($category['category_Name'])); ?>/<?php echo $category['categoryId']; ?>"
                                       class="gotocat">
                                        <button>view all</button>
                                    </a>
                                </ul>
                                <span class="indicator"> <img
                                        src="<?php echo base_url(); ?>content/images/active-list.png"
                                        class="img-responsive"> </span>
                            </li>


                            <?php $srm++;
                        } ?>


                    </ul>
                    <hr class="line-hori">
                    <div class="main-social-sectn">
                        <ul>
                            <li><a href="https://www.facebook.com/rigalio" target="_blank"><span
                                        class="icomoon icon-facebook"> </span>
                                </a></li>
                            <li><a href="http://www.twitter.com/rigalioluxury" target="_blank"> <span
                                        class="icomoon icon-twitter"> </span></a></li>
                            <li><a href="http://www.instagram.com/rigalio" target="_blank"><span
                                        class="icomoon icon-instagram"> </span></a>
                            </li>
                            <li><a href="https://plus.google.com/u/0/116609420281563249981/posts" target="_blank"><span
                                        class="icomoon icon-google-plus"> </span></a></li>
                            <li><a href="https://www.linkedin.com/company/7604532" target="_blank"><span
                                        class="icomoon icon-linkedin"> </span></a></li>
                        </ul>
                    </div> <!--/main-social-sectn" -->
                </div> <!-- /main-menu -->
            </div>
        </div>

    </div><!-- popup -inner-->
</div>  <!--/header popup -->

<!--new header-->


<!-- new header ends here -->


<script>

    $('.btn').click(function () {
        $('.btn').parent().removeClass('active');
        $(this).parent().addClass('active');
    });

</script>




<div class="signin-popup abcsignpopup" id="signin-popup">
    <div class="signin-popup-inner">
        <a href="javascript:void(0)" class="close">CLOSE</a>
        <div class="tab">
            <div class="tab-cell">
                <div class="signinvia col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <div class="golden-logo">
                        <img src="<?php echo base_url(); ?>content/images/logo/logo-golden.png" class="img-responsive">
                    </div>

                    <?php /*?>     <ul class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
      <?php
$ses_user=$this->session->userdata('User');
$userregistrationid=$this->session->userdata('registrationid');
if(empty($ses_user))   {

//echo "<li id='facebook'>".img(array('src'=>base_url().'images/facebook.png','id'=>'facebook','style'=>'cursor:pointer;float:left;margin-left:550px;'))."</li>";

echo "<li id='facebook'>"."<a><span><i class='fa fa-facebook'></i></span>"."Log in with facebook"."</a></li>";

 }  else{

  echo $ses_user['email']."<br>";
// echo '<img src="https://graph.facebook.com/'. $ses_user['id'] .'/picture" width="30" height="30"/><div>'.$ses_user['name'].'</div></li>';
  echo '<a href="http://localhost/finalrigalio/fblogin/logout">Logout</a></li>';
}
  ?>
        <!--<li> <a href="#"> <span><i class="fa fa-facebook"></i></span>Log in with facebook </a> </li>-->
        <li><a href="#"> <span><i class="fa fa-twitter"></i></span>Log in with twitter </a> </li>
 <?php
//$sessnlinkedin=$this->session->userdata('sessnlinkedin');
//print_r($sessnlinkedin);exit;
$userregistrationid=$this->session->userdata('registrationid');
// $userregistrationid;
if(empty($userregistrationid))   {

	 echo "<li>"."<a href='http://localhost/finalrigalio/linkedin_signup/initiate'><span><i class='fa fa-linkedin'></i></span>"."Log in with linkedin "."</a></li>";
	}  else{
		 //echo $userregistrationid['email']."<br>";
 echo '<a href="http://localhost/finalrigalio/Linkedin_signup/logout">Logout</a></li>';
 }
  ?>
      <!-- <li> <a href=""><span><i class="fa fa-linkedin"></i></span>Log in with linkedin </a> </li>-->
         <li> <a href="<?php echo base_url(); ?>main/signup"><span><i class="fa fa-linkedin"></i></span>Custom login </a> </li>
      </ul><?php */ ?>
                    <div class="registered-user current">
                        <h3>Log In </h3>
                        <hr>
                        <div id="msg1"></div>
                        <form method="post" id="myform" name="myform">
                            <!--<form action="<?php echo base_url(); ?>main/inviteuserlogin" method="post" id="register-form">-->
                            <ul class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <li><input type="text" name="username" id="username" placeholder="USERNAME"
                                           class="textbox-sign"></li>
                                <li><input type="password" name="password" id="password" placeholder="PASSWORD"
                                           class="textbox-sign"></li>
                            </ul>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                                <a href="<?php echo base_url(); ?>main/resetpasspage"><span class="problem-btn">Problem logging in? </span></a>
                                <button onClick="return profilevalidation()" class="start-btn" type="button"
                                        name="submit">continue
                                </button>
                            </div>
                        </form>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                          
                               <!-- <button class="continue-btn">request an invite</button>-->
                              <a href="<?php echo base_url(); ?>main/requestinvitepage"> <button class="continue-btn">request an invite</button></a>
                          
                        </div>
                    </div> <!--/registered-user -->
                    <div class="problem-logging" style="display: none;">
                        <h3>Problem in Login </h3>
                        <hr>
                        <div id="msg2"></div>
                        <div class="tab">
                            <div class="tab-cell">
                                <ul class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <li><input type="text" name="forget-username" id="forget-username"
                                               placeholder="USERNAME" class="textbox-sign"></li>
                                    <li><span class="or-brk"> OR</span></li>
                                    <li><input type="text" name="forget-email" id="forget-email"
                                               placeholder="PRIMARY EMAIL" class="textbox-sign"></li>
                                </ul>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                                    <button onClick="return getpass()" class="start-btn" type="button" name="submit">
                                        continue
                                    </button>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                                    <a href="javascript:void(0)" class="go-back-href logging-continue">
                                        <button class="continue-btn go-back-btn">go back</button>
                                    </a>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="invite-request" style="display: none;">
                        <h3>Request an Invite </h3>
                        <hr>
                        <p id="msg2" class="msg2"></p>
                        <div class="aftersubmit">
                            <ul class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <!--<li><input type="text" name="name" id="name" value="" placeholder="NAME"
                                           class="textbox-sign"></li>
                                <li><input type="text" name="email" id="email" value="" placeholder="EMAIL ID"
                                           class="textbox-sign"></li>
                                <li><input type="text" name="userinkedin" id="userinkedin" value=""
                                           placeholder="LINKEDIN/FACEBOOK/PROFILE LINK" class="textbox-sign"></li>
                                <li><input type="text" name="phoneno" id="phoneno" value="" placeholder="PHONE NO."
                                           class="textbox-sign"></li>
                                <li><input type="text" name="company" id="company" value="" placeholder="COMPANY"
                                           class="textbox-sign"></li>
                                <li><input type="text" name="designation" id="designation" value=""
                                           placeholder="DESIGNATION" class="textbox-sign"></li>-->
                                <!-- <li id="fbreqndinvite">facebook </li>-->
                                <li id="fbreqndinvite"><img
                                        src="<?php echo base_url(); ?>content/images/social-icons/facebook-invite-button.jpg"
                                        class="img-responsive">
                                    <div class="register-fb">
                                        <fb:login-button size="xlarge" max_rows="7"
                                                         scope="email,user_friends,user_birthday"
                                                         data-auto-logout-link="true"
                                                         onlogin="checkLoginState();"></fb:login-button>
                                        <a class="register-fb fb_login" onlogin="checkLoginState();"></a></div>
                                </li>
                                <li class="sep"><span class="or-brk"> OR</span></li>
                                <li id="linkdinreqndinvite"><img
                                        src="<?php echo base_url(); ?>content/images/social-icons/linkedin-invite-button.jpg"
                                        class="img-responsive">
                                    <div class="linkedin-log">
                                        <script type="in/Login"></script>
                                    </div>
                                <li>


                                    <div id="profiles"></div>
                            </ul>
                            <!--<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                                <a href="javascript:void(0)">
                                    <button class="start-btn userinvite" type="button" id="userinvite" name="submit">
                                        submit
                                    </button>
                                </a>
                            </div>-->
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                            <a href="javascript:void(0)" class="go-back-href">
                                <button class="continue-btn go-back-btn">go back</button>
                            </a>
                        </div>
                    </div> <!--/invite-request section -->

                </div>
            </div>
        </div>
    </div>
</div> <!-- /signin-popup -->

<div class="settings-popup" id="settings-popup">
    <div class="settings-inner">
        <a href="javascript:void(0)" class="close">CLOSE</a>
        <div class="tab">
            <div class="tab-cell">
                <div class="settings-con col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <div class="golden-logo">
                        <img src="<?php echo base_url(); ?>content/images/logo/logo-golden.png" class="img-responsive">
                    </div>
                    <h3> my account </h3>
                    <hr>
                    <?php if(count($userdata)>0){
                        ?>
                        <div class="myprofile">
                            <?php if ($userdata[0]['profile_picture'] == '') { ?>
                                <img src="<?php echo base_url(); ?>content/images/icons/userbig.png" class="img-responsive">
                            <?php } else { ?>
                                <span class="pf-img"
                                      style="background: url(<?php echo base_url(); ?><?php echo $userdata[0]['profile_picture']; ?>);"> </span>
                            <?php } ?>
                            <h4 id="updated_name"><?php echo $userdata[0]['firstname']; ?></h4>
                            <h5><a id="updated_usr" href="<?php echo base_url(); ?>home">@<?php echo strtolower($userdata[0]['username']); ?></a></h5>
                            <ul class="col-lg-6 col-md-6 col-sm-10 col-xs-12">
                                <li>
                                    <a href="<?php echo base_url(); ?>home" class="profile-link"><span>www.rigalio.com/<?php echo $userdata[0]['username']; ?> </span>
                                    </a></li>
                                <a href="<?php echo base_url(); ?>usersetting/setting" class="accont-btn">
                                    <li> manage account</li>
                                </a>
                                <a href="<?php echo base_url(); ?>main/logout" class="logout-btn">
                                    <li> logout</li>
                                </a>
                            </ul>
                        </div>
                    <?php } ?>
                </div> <!--/settings-con -->
            </div>
        </div>
    </div> <!--/settings-inner -->
</div>  <!--/settings-popup -->

<script>
    function getpass() {
        var username = $("#forget-username").val();
        var email = $("#forget-email").val();
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        //alert(username);
        if (username == '' && email == '') {
            //alert(fname);
            //alert("Please Enter username Name");
            document.getElementById('msg2').innerHTML = "Please Enter Username OR Primary email id to recover password";
            return false;
        }
        if (email != '') {
            if (reg.test(email) == false) {
                document.getElementById('msg2').innerHTML = "Invalid Email Address";
                return false;
            }

        }

        var data = {
            "username": username,
            "email": email,
        };
        $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>main/sendEmail",
            data: data,
            //crossDomain:true,
            success: function (html) {
                var msg = html;
                //alert(msg);
                if (msg == 1) {
                    alert("Email sent successfully");
                }
                else {
                    alert("Error in sending Email.");
                }
            }
        });
    }
</script>
<script>

    $(document).on('keydown', "#password", function (e) {
        // $(".textbox-sign").keypress(function(e){
        if (e.which == 13) {//Enter key pressed

            // alert("hi");
            profilevalidation(); //Trigger search button click event
        }
    });


    function profilevalidation() {
        var username = $("#username").val();
        var password = $("#password").val();
        if (username == '') {
            //alert(fname);
            //alert("Please Enter username Name");
            document.getElementById('msg1').innerHTML = "Please Enter First Name";
            document.myform.username.focus();
            return false;
        }

        if (password == '') {
            //alert(fname);
            //alert("Please Enter username Name");
            document.getElementById('msg1').innerHTML = "Please Enter password";
            document.myform.password.focus();
            return false;
        }
        if (username || password) {
            var data = {
                "username": username,
                "password": password
            };

            $.ajax({
                type: "POST",
                url: "<?php echo base_url();?>main/inviteuserlogin",
                // url: "http://localhost/finalrigalio/main/inviteuserlogin?username=" + username + "&password=" + password,
                data: data,
                //crossDomain:true,
                success: function (html) {
                    //alert(html);
                    var inviteuser = html;
                    if (inviteuser == 1) {
                        //alert(html);
                        //window.location.href='http://localhost/finalrigalio/main/signup';
                        window.location.href = '<?php echo base_url(); ?>home';
                    }
                    else if (inviteuser == 2) {
                        document.getElementById('msg1').innerHTML = "User Name or Password are Incorrect !";
                        // document.getElementById('myhide').style.display="none";
                        //window.location.href='http://localhost/finalrigalio/home';
                        //	alert(html);
                    }
                    else if (inviteuser == 3) {
                        //alert(html);
                        window.location.href = '<?php echo base_url(); ?>home';
                    }
                    else if (inviteuser == 4) {
                        //alert(html);
                        document.getElementById('msg1').innerHTML = "Invalid username and password";
                    }
                    else if (inviteuser == 5) {
                        //alert(html);
                        window.location.href = '<?php echo base_url(); ?>main/signup';
//window.location.href='http://rigalio.com/x/no_access/main/user_profile';
                    }
                    //var msg = count(inviteuser);
                    // alert(inviteuser);

                }
            });
            return false;
        } else {
            document.getElementById('msg1').innerHTML = "Please Enter All fields";
            return false;
        }

    }
	
	function onLinkedInLoad() {
		 var refnameinkdin = '';
 
    IN.Event.on(IN, "auth", onLinkedInAuth);
  }
  
  // 2. Runs when the viewer has authenticated
  function onLinkedInAuth() {
      refnameinkdin = $('#refvariable').html();
	 // console.log(refnameinkdin);
   var myinfo =  IN.API.Profile("me").fields("id","first-name", "last-name", "email-address", "headline", "siteStandardProfileRequest", "picture-url", "industry").result(displayProfiles);
    //displayProfiles(myinfo);
   //console.log(myinfo);
  }

  // 2. Runs when the Profile() API call returns successfully
   function displayProfiles(profiles) {
//console.log(profiles);
  var member = profiles.values[0];
//  $('.loader-part').show();
  $('body').addClass('loading');
  refnameinkdin = $('#refvariable').html();
  //console.log(refnameinkdin);
//console.log(profiles);
//console.log(member);
if(refnameinkdin)
{
	//console.log(refnameinkdin);
	 var data = {
		"refnameinkdin":  refnameinkdin,
        "fname":  member.firstName,
        "lname":  member.lastName,
		"email": member.emailAddress,
		"headline": member.headline, 
		"industry": member.industry, 
		"picture": member.pictureUrl,
		"link": member.siteStandardProfileRequest.url
    };
//console.log(data);
	$.ajax({
        type: "POST",
        url: "" + base_url + "main/frendreflinkedin",
        data: data,
        success: function (html) {
           // var me = html
		   //alert(html);
if(html==1){
//$('.loader-part').hide();
 $('body').removeClass('loading');
$('#refermsg2').html("You have already requested an invite.");
 $('.aftersubmitrefer').hide();
}else {
 //$('.loader-part').hide();
  $('body').removeClass('loading');
$('#refermsg2').html("Thank you for your interest, We will facililate your invite and get back to you soon.");
 $('.aftersubmitrefer').hide();
}
   
        
        }
    });  //ajax ends here
	
	
}
else{
    var data = {
        "fname":  member.firstName,
        "lname":  member.lastName,
		"email": member.emailAddress,
		//"phoneno": member.phoneNumbers,
		"headline": member.headline, 
		"industry": member.industry, 
		"picture": member.pictureUrl,
		"link": member.siteStandardProfileRequest.url
    };
	
   $.ajax({
        type: "POST",
        url: "" + base_url + "main/reqinvitelinkedin",
        data: data,
        success: function (html) {
           // var me = html
		   //alert(html);
if(html==1){
//$('.loader-part').hide();
 $('body').removeClass('loading');
$('.msg2').html("You have already requested an invite.");
 $('.aftersubmit').hide();
}else {
//$('.loader-part').hide();
 $('body').removeClass('loading');
$('.msg2').html("Thank you for your interest, We will facililate your invite and get back to you soon.");
 $('.aftersubmit').hide();
}
        
        }
    });  //ajax ends here
}

    }

function mysend(url){

FB.ui({
  method: 'send',
  link: url,
});
}

  


    </script>
