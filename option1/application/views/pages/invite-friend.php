<!--invite page -->
<div class="invite-pg col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="container-fluid">
        <div class="row">
            <div class="invite-pg-inner col-lg-7 col-sm-10 col-xs-12 nopadding">
                <h3>The Rigalio Inner Circle Invite.</h3>
                <h2>Refer your friend to the Inner Circle by choosing any one of the
following methods.</h2>
                <hr>
                <div class="col-lg-12 invite-via-con">
                    <div class="col-lg-6 col-xs-12 via-fb-con">
                        <div class="tab">
                            <div class="tab-cell"><a href="#">
                                    <div class="invite-btns" onclick="mysend(this.id);" id="<?php echo base_url(); ?>/Main/invitation/<?php echo $userdata[0]['username']; ?>"><i class="fa fa-facebook" aria-hidden="true"></i><span> Refer via Facebook</span>
                                    </div>
                                </a></div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-xs-12 via-email-frm">
                        <form>
                            <label>Invite via Email</label>
                            <ul>
                                <li><input type="text" name="name" id="invite_name" placeholder="NAME"
                                           class="textbox-sign"></li>
                                <li><input type="text" name="email" id="invite_email" placeholder="EMAIL ID"
                                           class="textbox-sign"></li>
                                <button class="start-btn" type="button" name="submit" id="invite_friend">invite</button>
                            </ul>
                        </form>
                    </div>
                </div>


            </div> <!--/invite-pg-inner -->
        </div>
    </div>
</div>

<script>

    $(document).ready(function () {

        $(document).on('click', '#invite_friend', function () {
            // alert("hi");
            var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
            var name = $("#invite_name").val();
            var email = $("#invite_email").val();
            if (name == "") {
                // alert("Pleaser Enter Name");

                $(".alert-sectn").fadeIn();
                $("#alert-msg").text("Pleaser Enter Name");
                setTimeout(function () {
                    $('.alert-sectn').fadeOut();
                }, 3000);


                return false;
            }
            if (email == "") {
                $(".alert-sectn").fadeIn();
                $("#alert-msg").text("Pleaser enter email");
                setTimeout(function () {
                    $('.alert-sectn').fadeOut();
                }, 3000);


                return false;

            }

            if (reg.test(email) == false) {

                $(".alert-sectn").fadeIn();
                $("#alert-msg").text("Pleaser enter valid Email");
                setTimeout(function () {
                    $('.alert-sectn').fadeOut();
                }, 3000);


                return false;
            }


            var data = {
                "user_name": name,
                "user_email": email

            };

            $.ajax({
                type: "POST",
                url: "" + base_url + "Main/save_invite",
                //dataType: 'json',
                data: data,
                success: function (html) {
$("#invite_name").val("");
$("#invite_email").val("");

                    $(".alert-sectn").fadeIn();
                    $("#alert-msg").text(html);
                    setTimeout(function () {
                        $('.alert-sectn').fadeOut();
                    }, 3000);

                }


            });


        });
    });




</script>
<!--invite page ends -->