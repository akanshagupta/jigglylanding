<!--privacy-pg con -->
<div class="privacy-pg col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
  <div class="container-fluid">
    <div class="row">

      <div class="privacy-pg-con col-lg-10 col-md-10 col-sm-11 col-xs-11 nopadding">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
           <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 content_headline">
             <h2>privacy policy </h2> <hr>
           </div>
           
           <div class="privacy-text">
             <p>Thank you for visiting www.rigalio.com, a web site of Rigalio Pvt. Ltd. (hereinafter, "Rigalio").  </p>
           </div> <!-- /privacy-text -->
           <div class="privacy-text">
             <p>Rigalio ("www.rigalio.com") is a part of the web site dedicated to luxury product cataloguing managed by Rigalio. The company RIGALIO, with registered offices Building No-17, Sector-32, Gurgaon-122002, Haryana, only manages the display of the products and the brands showcased within Rigalio.This privacy policy (the "Privacy Policy") applies to the web site rigalio.com (the "Site") and refers to the personal data processing activities performed by Rigalio upon the user visit to the Site and the user interaction with the services and functionalities offered by the Site.   </p>
           </div> <!-- /privacy-text -->
           <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 content_headline">
             <h4>THE NATURE AND KIND OF DATA PROCESSED. THE PURPOSES OF THE DATA PROCESSING  </h4> <hr>
           </div>
           <div class="privacy-text">
             <p>In general, the visit and consultation of the Site does not imply collection and processing of personal data of the user.</p>
             <p>The processing of personal data of users that visit and consult the Site is limited to the so named surfing data, namely the data whose transmission to the Site is implicit in the functioning of the information systems in charge of the managing of the Site and in the use of communications protocols proper of the Internet. Surfing data are, for example, the IP addresses or the domain names of the computers employed by the users who connect to the Site and other parameters relating to the type and the information operating system employed by the user. Surfing data are not collected by Rigalio in order to be associated with to users or identified persons, however surfing data for their own nature may allow the identification of the user if they are processed and associated with other data owned by third parties. Surfing data are collected and processed by Rigalio exclusively for statistical purposes and in anonymous form in relation to the access and use of the Site and for purposes of monitoring the correct functioning of the Site and of enhancing the Site functioning and content. Surfing data are deleted immediately after the processing in anonymous form; said data may be used for purposes of assessing possible responsibilities in case of information crimes realized against the Site or through the Site. With the exception of the aforementioned circumstance, Rigalio keeps the users' surfing data only temporarily for a maximum period of seven days or otherwise required under applicable law. </p>
             <p>Furthermore, Rigalio collects and processes personal data voluntarily provided by user or collected, also from third parties, in the course of its activity. Personal data may be voluntarily provided by user for example upon interaction of the user with the Site functionalities and request of the services offered by the Site, for example upon user subscription to the Site, to the Rigalio Circle program or to other initiatives offered through the Site, upon user request of information or informative material to Rigalio or user sending to Rigalio of a communication through traditional mail or through the Site. Rigalio will also process user's personal data to assess or defend a legal claim and to comply with laws, regulations and Community legislation.  </p>
             <p>With the user's consent, that is free and optional, Rigalio may also process user's personal data for purposes of defining of individual and group profiles (profiling), and for marketing purposes, notably to send to user, also through newsletter, e-mails, sms and mms, information and updates on our products, offers, exclusive sales, promotional campaigns and on events and similar initiatives organized by Rigalio or to which Rigalio takes part to, including possible invitations to said events. User will always have the opportunity to object the processing of his/her personal data for the sending of promotional information through e-mail; in each communication there will be a specific section in which are specified the conditions for revoking the consent and not receiving anymore promotional material and information. It is understood that the profiling activities will in no case be performed with information relating to user's purchase of products through Rigalio.  </p>
           </div> <!-- /privacy-text -->
           <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 content_headline">
              <h4>PLACE WHERE THE DATA is COLLECTED And STORED </h4> <hr>
           </div>
           <div class="privacy-text">
              <p>Personal data will be collected and processed through an electronic system managed by Rigalio that is located at the offices of Rigalio as above specified. Moreover, other companies of the Rigalio group are involved in the data processing in relation to managing of the Site and relevant services and functionalities and the processing purposes above reported. User is invited to look at the section of the Privacy Policy relating to the extent of data communication for further information on data communication and on details on personal data transfer out of India.  </p>
           </div>
           <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 content_headline">
               <h4>COOKIES </h4> <hr>
           </div>
           <div class="privacy-text">
              <p>Cookies are small data files stored on the hard disk of the users' computer. The Site deploys cookies for specific and limited purposes, notably in order to offer to user an easier surfing on the Site, to allow users subscribed to the Site, the Rigalio Circle program or other initiatives offered through the Site an easier login, for internal security purposes, for statistical analysis (for example in relation to the Site traffic) and system management. The so called 'session cookies' are stored in a temporary memory and are deleted when user closes the browser.  </p>
              <p>Cookies that are used upon subscription of the user to the Site, the Rigalio Circle program or other initiatives offered through the Site collect user personal information that allows Rigalio to recognize the user in case of following visit to the Site and they are used to allow us to understand what are the sections of the Site that are of the user's interest and to offer to the user an easier access to the sections of the Site devoted to subscribers to the Site, the Rigalio Circle program or other initiatives offered through the Site, an easier and personalized surfing of the Site and of the information published on the Site, and for the security and management purposes above reported. Cookies deployed by Rigalio do not allow Rigalio to run programs on the user's computer or to download virus or other malicious programs on the same and do not allow Rigalio any kind of control on the user's computer. Rigalio does not use cookies to access information stored on the user's computer, to store therein information and to monitor user's activities. The foregoing applies to user's computer and any other kind of device used by user to connect to the Site.The Site may contain links to other sites. Rigalio does not have any type of access or control on cookies and other user tracking technologies that are present on web sites of third parties that are accessible from the Site, on the availability and on any content or material contained in, or obtained through, any such sites, and hereby expressly disclaims any relevant liability. Rigalio invites user to consult the privacy policy of third parties' web sites that user accesses from the Site in order to be informed on the conditions of collection and processing of user's personal data, since this Privacy Policy only applies to the Site as above defined. Most web browsers automatically accept cookies and tags, but user can usually change user's web browser to disable this function. The section "Help" of the toolbar placed on the majority of browsers explains how to avoid receipt by browser of cookies and other technologies employed to track users, how to obtain from browser notice of receipt of the referenced technologies or how to disable them completely. Disabling of cookies may limit the possibility to use the Site and may prevent user from fully benefiting of the Site functionalities and services. </p>
           </div>
           <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 content_headline">
               <h4>FEATURES AND CONDITIONS OF THE PERSONAL DATA PROCESSING  </h4> <hr>
           </div>
           <div class="privacy-text">
             <p>Personal data collected by Rigalio are processed mainly with electronic, information and telematic means, according to logics and procedures strictly related to the purpose of processing above reported. Data may also be processed without electronic means through manual means; in any case data are kept only for the time necessary to achieve the specific processing purpose from time to time sought and according to applicable legislation on data protection, including security and confidentiality principles that inspire our activity. We will process data according to applicable security requirements to minimize the risks of data destruction and loss, even accidental, of unauthorized access, unlawful processing or processing that does not comply with the purposes of data collection and of illicit or not correct use of data. Moreover, information system and programs are configured so as to minimize the use of personal and identification data so that said data are used only when necessary to the specific processing purpose from time to time sought.  </p>
           </div>
           <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 content_headline">
               <h4>PROVIDING OF PERSONAL DATA </h4> <hr>
           </div>
           <div class="privacy-text">
             <p>Except for the foregoing description in relation to the surfing data, providing of personal data by user is necessary for performance of the services and functionalities offered by the Site and required by user, such as for example user subscription to the Site, the Rigalio Circle program or other initiatives offered through the Site and relevant management, replying to user's questions, and sending of information and informative material requested by user. Providing of personal data by user is mandatory to assess and defend legal claims and to comply with applicable legislation; denial of providing personal data by user in the above reported circumstances would make it impossible for user to subscribe to the Site, the Rigalio Circle program or other initiatives offered through the Site, to receive the services, functionalities, replies, information and informative material requested to Rigalio.  </p>
             <p>Providing of personal data by user for profiling and marketing purposes as above specified is free and optional; denial of providing personal data by user for profiling and marketing purposes will have no consequence, for example said denial will have no consequence on the possibility for user to subscribe to the Site, the Rigalio Circle program or other initiatives offered through the Site, to receive the services, functionalities, replies, information and informative material requested to Rigalio. </p>
           </div>
           <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 content_headline">
               <h4>USER'S RIGHTS </h4> <hr>
           </div>
           <div class="privacy-text">
             <p>User is entitled at any moment to enforce his/her rights as provided under the Privacy Code, for example the right to obtain confirmation that user's personal data exist or do not exist, verify their content, origin and accuracy, ask for their integration, updating, amendment, deletion, transformation in anonymous form, block for breach of laws and regulations, and user may oppose for legitimate reason the data processing.   </p>
             <p>To enforce user's rights and for any query or request relating to the personal data processing by Rigalio, user may contact the Data Processor, currently the Information Systems Manager at Rigalio, who is domiciled for this duty at the offices of Rigalio as follows: privacy@rigalio.com.</p>
             <p> This Privacy Policy is subject to updating by Rigalio. The version published on the Site is the version actually in force. If Rigalio changes the Privacy Policy, it will notify user of such changes by posting a revised link on the home page of the Site for 30 days that reads "Newly Revised Privacy Policy" and that links to this page where the revised Privacy Policy will be posted. Rigalio will also, where required by applicable law, obtain user's consent for any such changes. Rigalio invites user to periodically verify the Privacy Policy to be informed on any change to the same.</p>
             <p> <b>The Effective Date of this Privacy Policy is: 01/04/2016. </b> </p>
           </div>
        </div>
      </div> <!--/privacy-pg-con -->
      

    </div>
  </div>  
</div><!--privacy-pg-con ends -->
<footer>
  <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 footer-inner">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
      <p>All rights reserved. All content belongs to respective owners. </p>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
       <ul>
         <li><a href="<?php echo base_url();?>main/aboutus">About us </a> </li>
         <li><a href="<?php echo base_url();?>main/contactus">Contact us</a> </li>
         <li><a href="<?php echo base_url();?>main/privacy">Privacy </a> </li>
         <li><a href="<?php echo base_url(); ?>main/faq">FAQ </a></li>
       </ul>
    </div>
  </div>
</footer>
  </body>
<script src="<?php echo base_url(); ?>content/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url(); ?>content/js/bootstrap-hover-dropdown.js"></script>
<script>
    // very simple to use!
    $(document).ready(function() {
      $('.js-activated').dropdownHover().dropdown();
    });
</script>
<style>

</style>

  <script src="content/js/bootstrap.min.js"></script>
  <script src="content/js/bootstrap-hover-dropdown.js"></script>
</html>
