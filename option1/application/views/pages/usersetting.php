<?php
    //print_r($userdata); exit;
?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"/>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
 <script src="<?php echo base_url(); ?>content/js/flatpickr.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>content/js/flatpickr.min.css"/>
<script type="text/javascript">
		$(function() {
    $( "#skills" ).autocomplete({
        source: '<?php echo base_url(); ?>main/getcity',
		minLength: 2
    });
});
		</script>
<div class="settings-tagline">
    <div class="col-lg-10 col-md-10 col-sm-11 col-xs-11 nopadding settings-info">
        <?php if ($userdata[0]['profile_picture'] == '') { ?>
            <img src="<?php echo base_url(); ?>content/images/profile/profile_pcture.jpg" class="img-responsive">
        <?php } else { ?>
            <span class="pf-img"
                  style="background: url(<?php echo base_url(); ?><?php echo $userdata[0]['profile_picture']; ?>);"> </span>

        <?php } ?>
        <span
            class="user-name"><?php echo $getuserdataall[0]['firstname']; ?></span>
    </div>
</div>  <!--/settings-tagline -->
<!--settings page con -->
<div class="settings-pg col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding nomargin">
    <div class="container-fluid">
        <div class="row">
            <div class="settings-pg-con col-lg-6 col-md-6 col-sm-10 col-xs-12 nopadding">
                <div class="settings-tab-panel">
                    <ul class="nav nav-pills">
                        <li class="active"><a href="#details" data-toggle="tab">details </a></li>
                        <li><a href="#communication" data-toggle="tab"> communication</a></li>
                        <li><a href="#privacy" data-toggle="tab">privacy </a></li>
                    </ul>
                </div> <!--/settings-tab-panel -->
                <div class="settings-blocks col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                    <div id="myTabContent2" class="tab-content">
                        <div class="details-tab-con tab-pane fade active in" id="details">
                            <ul class="nopadding">

                                <li><label>Name</label>
                                    <button class="edit">edit</button>
                                    <div class="password-info">
                                        <ul class="nopadding">
                                            <p id="usererror"></p>
                                            <li><input type="text" id="user_name" name="user_name" value="<?php echo $getuserdataall[0]['firstname']; ?>"
                                                       placeholder="Enter Name"/></li>


                                        </ul>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                                            <!--<input type="button" class="save-btn" value="save" id="savepass">-->
                                            <button type="button" class="save-btn" id="save_name">save</button>
                                        </div>
                                    </div> <!--/Password-info -->
                                </li>

                                <li><label>Username</label>
                                    <button class="edit">edit</button>
                                    <div class="password-info">
                                        <ul class="nopadding">
                                            <p id="usererror"></p>
                                            <li><input type="text" id="user_username" onBlur="existuser();" name="user_username" value="<?php echo $getuserdataall[0]['username']; ?>"
                                                       placeholder="Enter Username "
                                                       class="textboxd">
                                                       </li>
                                        </ul>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                                            <!--<input type="button" class="save-btn" value="save" id="savepass">-->
                                            <button type="button" class="save-btn" id="save_username">save</button>
                                        </div>
                                    </div> <!--/Password-info -->
                                </li>

                                <li><label>Occupation</label>
                                    <button class="edit">edit</button>
                                    <div class="password-info">
                                        <ul class="nopadding">
                                            <p id="usererror"></p>
                                            <li>
                                            <?php /*?><input type="text" id="user_occupation" name="user_occupation" value="<?php echo $getuserdataall[0]['occupation']; ?>"
                                                       placeholder="Enter Occupation"
                                                       class="textboxd"><?php */?>
                                                       
                                                       <select class="signin-textbox" id="user_occupation"
                                                                name="user_occupation">
                                                            <option value="select occupation">Please select Occupation
                                                            </option>
                                                            <option value="Remunerated" <?php if ($getuserdataall[0]['occupation'] == 'Remunerated')  echo 'selected = "selected"'; ?>> Remunerated</option>
                                                            <option value="Entrepreneurial" <?php if ($getuserdataall[0]['occupation'] == 'Entrepreneurial')  echo 'selected = "selected"'; ?>> Entrepreneurial</option>
                                                            <option value="Government/Public Sector" <?php if ($getuserdataall[0]['occupation'] == 'Government/Public Sector')  echo 'selected = "selected"'; ?>>Government/Public Sector
                                                            </option>
                                                            <option value="Multinational Corporations" <?php if ($getuserdataall[0]['occupation'] == 'Multinational Corporations')  echo 'selected = "selected"'; ?>>Multinational Corporations
                                                            </option>
                                                            <option value="Public Limited Corporation" <?php if ($getuserdataall[0]['occupation'] == 'Public Limited Corporation')  echo 'selected = "selected"'; ?>>Public Limited Corporation
                                                            </option>
                                                            <option value="Private Limited Corporation" <?php if ($getuserdataall[0]['occupation'] == 'Private Limited Corporation')  echo 'selected = "selected"'; ?>>Private Limited Corporation
                                                            </option>
                                                            <option value="Partnership" <?php if ($getuserdataall[0]['occupation'] == 'Partnership')  echo 'selected = "selected"'; ?>>Partnership</option>
                                                            <option value="Proprietorship" <?php if ($getuserdataall[0]['occupation'] == 'Proprietorship')  echo 'selected = "selected"'; ?>>Proprietorship</option>
                                                            <option value="Chartered Accountant" <?php if ($getuserdataall[0]['occupation'] == 'Chartered Accountant')  echo 'selected = "selected"'; ?>>Chartered Accountant
                                                            </option>
                                                            <option value="Engineer" <?php if ($getuserdataall[0]['occupation'] == 'Engineer')  echo 'selected = "selected"'; ?>>Engineer</option>
                                                            <option value="Architect" <?php if ($getuserdataall[0]['occupation'] == 'Architect')  echo 'selected = "selected"'; ?>>Architect</option>
                                                            <option value="Lawyer" <?php if ($getuserdataall[0]['occupation'] == 'Lawyer')  echo 'selected = "selected"'; ?>>Lawyer</option>
                                                            <option value="Doctor" <?php if ($getuserdataall[0]['occupation'] == 'Doctor')  echo 'selected = "selected"'; ?>>Doctor</option>
                                                            <option value="Trader" <?php if ($getuserdataall[0]['occupation'] == 'Trader')  echo 'selected = "selected"'; ?>>Trader</option>
                                                            <option value="Consultant" <?php if ($getuserdataall[0]['occupation'] == 'Consultant')  echo 'selected = "selected"'; ?>>Consultant</option>
                                                            <option value="Media Houses" <?php if ($getuserdataall[0]['occupation'] == 'Media Houses')  echo 'selected = "selected"'; ?>>Media Houses</option>
                                                        </select>
                                                        </li>
                                        </ul>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                                            <!--<input type="button" class="save-btn" value="save" id="savepass">-->
                                            <button type="button" class="save-btn" id="save_occupation">save</button>
                                        </div>
                                    </div> <!--/Password-info -->
                                </li>

                                <li><label>Organization</label>
                                    <button class="edit">edit</button>
                                    <div class="password-info">
                                        <ul class="nopadding">
                                            <p id="usererror"></p>
                                            <li><input type="text" id="user_organization" name="user_organization" value="<?php echo $getuserdataall[0]['organisation']; ?>"
                                                       placeholder="Enter Organization"
                                                       class="textboxd"></li>


                                        </ul>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                                            <!--<input type="button" class="save-btn" value="save" id="savepass">-->
                                            <button type="button" class="save-btn" id="save_organization">save</button>
                                        </div>
                                    </div> <!--/Password-info -->
                                </li>


                                <li><label>Designation</label>
                                    <button class="edit">edit</button>
                                    <div class="password-info">
                                        <ul class="nopadding">
                                            <p id="usererror"></p>
                                            <li><input type="text" id="user_designation" name="user_designation" value="<?php echo $getuserdataall[0]['designation']; ?>"
                                                       placeholder="Enter Designation"
                                                       class="textboxd"></li>


                                        </ul>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                                            <!--<input type="button" class="save-btn" value="save" id="savepass">-->
                                            <button type="button" class="save-btn" id="save_designation">save</button>
                                        </div>
                                    </div> <!--/Password-info -->
                                </li>


                                <li><label>Social Links</label>
                                    <button class="edit">edit</button>
                                    <div class="password-info">
                                        <ul class="nopadding">
                                            <p id="usererror"></p>
                                            <li><input type="text" id="user_fb" name="user_fb" value="<?php echo $getuserdataall[0]['fb_link']; ?>"
                                                       placeholder="Enter Facebook Link"
                                                       class="textboxd"></li>
                                            <li><input type="text" id="user_linkedin" name="user_linkedin" value="<?php echo $getuserdataall[0]['linkedin_link']; ?>"
                                                       placeholder="Enter linkedin Link"
                                                       class="textboxd"></li>
                                            <li><input type="text" id="user_twitter" name="user_twitter" value="<?php echo $getuserdataall[0]['twitter_link']; ?>"
                                                       placeholder="Enter twitter Link"
                                                       class="textboxd"></li>


                                        </ul>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                                            <!--<input type="button" class="save-btn" value="save" id="savepass">-->
                                            <button type="button" class="save-btn" id="save_social">save</button>
                                        </div>
                                    </div> <!--/Password-info -->
                                </li>

                                <li><label>Gender</label>
                                    <button class="edit">edit</button>

                                    <div class="mode-comm">
                                        <ul class="nopadding">
                                            <li><input id="male" type="radio" name="gen"
                                                       value="male" <?php if ($getuserdataall[0]['subtitle'] == 'male') {
                                                    echo "checked";
                                                } ?>/>
                                                <label for="radio1">Male</label></li>
                                            <li><input id="female" type="radio" name="gen"
                                                       value="female" <?php if ($getuserdataall[0]['subtitle'] == 'female') {
                                                    echo "checked";
                                                } ?>/>
                                                <label for="radio1">Female</label></li>


                                        </ul>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                                            <button type="button" class="save-btn" id="save_gender">save</button>
                                        </div>
                                    </div> <!--/mode-comm-inner -->

                                </li>

                                <li><label>Location</label>
                                    <button class="edit">edit</button>
                                    <div class="password-info">
                                        <ul class="nopadding">
                                            <p id="usererror"></p>
                                            <li>
                       <div class="ui-widget">
    <label for="skills"> </label>
    <input id="skills" value="<?php echo $getuserdataall[0]['city']; ?>" class="signin-textbox" placeholder="Select City" required>
</div>

<!--<input type="text" id="user_username" name="user_location" value="<?php echo $getuserdataall[0]['city']; ?>"
                                                       placeholder="Enter City "
                                                       class="textboxd">--></li>


                                        </ul>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                                            <!--<input type="button" class="save-btn" value="save" id="savepass">-->
                                            <button type="button" class="save-btn" id="save_location">save</button>
                                        </div>
                                    </div> <!--/Password-info -->
                                </li>

                                <li><label>Password</label>
                                    <button class="edit">edit</button>
                                    <div class="password-info">
                                        <ul class="nopadding">
                                            <p id="usererror"></p>
                                            <!--<li><input type="password" id="currentpass" name="currentpass" value=""
                                                       placeholder="CURRENT PASSWORD" onBlur="existpassword()"
                                                       class="textboxd"></li>-->
                                             <li><input type="password" id="currentpass" name="currentpass" value=""
                                                       placeholder="CURRENT PASSWORD" class="textboxd"></li>
                                            <li class="passwrdshow"><input type="password" name="newpass"
                                                                           placeholder="NEW PASSWORD" id="newpass"
                                                                           value="" class="textboxd "/><span
                                                    class="text">Show</span><input id="test2" type="checkbox"
                                                                                   class="hiden-chck"/></li>

                                        </ul>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                                            <!--<input type="button" class="save-btn" value="save" id="savepass">-->
                                            <button type="button" class="save-btn" id="savepass123">save</button>
                                        </div>
                                    </div> <!--/Password-info -->
                                </li>
                                <li><label>Email address</label>
                                    <button class="edit">edit</button>
                                    <div class="email-settings">
                                        <h5>We already have your following email addresses </h5>
                                        <ul class="nopadding addajaxemail">
                                            <li><label><?php echo $getuserdataall[0]['email']; ?> </label>
                                                <button class="primary">primary</button>
                                            </li>
                                            <?php
                                                foreach ($useremail as $emailid) {
                                                    ?>
                                                    <li id="ajaxemail">
                                                        <label><?php echo $emailid['useremail']; ?></label><a
                                                            href="<?php echo base_url(); ?>usersetting/makeprimary/<?php echo $emailid['useremailid']; ?>">
                                                            <button class="make-primary">make primary</button>
                                                        </a> <a
                                                            href="<?php echo base_url(); ?>usersetting/deleteemail/<?php echo $emailid['useremailid']; ?>">
                                                            <button class="remove-btn">Remove</button>
                                                        </a></li>
                                                <?php } ?>
                                        </ul>
                                        <h4 class="add-email-click"> Add new email address</h4>
                                        <div class="add-email">
                                            <input type="text" name="addemail" value="" id="addemail"
                                                   placeholder="NEW EMAIL ADDRESS" class="textboxd addemail">
                                            <!--<button class="verify-btn">verify</button>
                                            <p class="verify-msg" style="display: none;">Check your email. New email address is updated </p>-->
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                                                <button class="save-btn" id="saveemail">save</button>
                                            </div>
                                        </div>

                                    </div> <!--/email-settings -->
                                </li>
                                <li><label>date of birth</label>
                                    <button class="edit">edit</button>
                                    <div class="dob-info">
                                        <ul class="nopadding">
                                            <li><input type="text" name="currentdob" id="currentdob"
                                                       value="<?php echo $getuserdataall[0]['dob']; ?>"
                                                       class="textboxd"></li>
                                            <li><input type="text" name="newdob" value="" placeholder="ENTER NEW DATE"
                                                       id="newdob" class="textboxd"></li>
                                        </ul>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                                            <button id="savedob" class="save-btn">save</button>
                                        </div>
                                    </div> <!--/dob-info -->
                                </li>
                                <li><label>Phone number</label>
                                    <button class="edit">edit</button>
                                    <div class="phone-info">
                                        <?php
                                            if ($getuserdataall[0]['phone'] == '') {
                                                ?>
                                                <h5>Please Add Your Phone Number</h5>
                                                <?php
                                            } else {
                                                ?>
                                                <h5>We already have your following phone numbers </h5>
                                                <ul class="nopadding addajaxphno">
                                                    <li><label>+91 <?php echo $getuserdataall[0]['phone']; ?></label>
                                                        <button class="primary">primary</button>
                                                    </li>
                                                    <?php
                                                        foreach ($userphoneno as $phoneno) {
                                                            ?>
                                                            <li><label>+91 <?php echo $phoneno['phoneno']; ?> </label><a
                                                                    href="<?php echo base_url(); ?>usersetting/makeprimaryphone/<?php echo $phoneno['phoneid']; ?>">
                                                                    <button class="make-primary">make primary</button>
                                                                </a> <a
                                                                    href="<?php echo base_url(); ?>usersetting/deletephoneno/<?php echo $phoneno['phoneid']; ?>">
                                                                    <button class="remove-btn">Remove</button>
                                                                </a></li>
                                                        <?php } ?>
                                                </ul>
                                            <?php } ?>
                                        <h4 class="add-ph-click"> Add new Phone no.</h4>
                                        <div class="add-ph">
                                            <input type="text" name="addphone" id="addphone" value=""
                                                   placeholder="NEW PHONE NO." class="textboxd">
                                            <!-- <button class="verify-btn">verify</button>-->
                                            <!--<p class="verify-ph" style="display: none;">Check your phone. New phone no. is updated </p>-->
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                                                <button class="save-btn" id="savephone">save</button>
                                            </div>
                                        </div> <!--/phone-info -->
                                </li>
                            </ul>
                        </div> <!--/details-tab-con -->
                        <div class="comm-tab-con tab-pane" id="communication">
                            <ul>
                                <!-- <li> <label>Frequency of information</label><button class="edit">edit </button>
                  <div class="freq-info">
                    <ul class="nopadding">
                    <li><input id="radio1" type="radio" name="info" value="Weekly" <?php echo ($getuserdataall[0]['reminder_type'] == 'Weekly') ? "checked" : ""; ?> ><label for="radio1">Weekly</label></li>
                    <li><input id="radio2" type="radio" name="info" value="Fortnightly" <?php echo ($getuserdataall[0]['reminder_type'] == 'Fortnightly') ? "checked" : ""; ?>><label for="radio2">Fortnightly</label></li>
                    <li><input id="radio3" type="radio" name="info" value="Monthly" <?php echo ($getuserdataall[0]['reminder_type'] == 'Monthly') ? "checked" : ""; ?> ><label for="radio1">Monthly</label></li>
                       
                </ul>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding"><button class="save-btn" id="saveinfo">save</button> <span class="alert-msg">You can choose any one of the option </span> </div>
                  </div> <!--/freq-info-inner -->
                                <!--</li>-->
                                <li><label>Would you like to subscribe to our newsletter.</label>
                                    <button class="edit">edit</button>
                                    <div class="mode-comm">
                                        <ul class="nopadding">
                                            <li><input id="keepyou" type="radio" name="keepyou"
                                                       value="Yes" <?php if ($getuserdataall[0]['contact_type'] == 'Yes') {
                                                    echo "checked";
                                                } ?>/>
                                                <label for="keepyou1">Yes</label></li>
                                            <li><input id="check8" type="radio" name="keepyou"
                                                       value="No" <?php if ($getuserdataall[0]['contact_type'] == 'No') {
                                                    echo "checked";
                                                } ?>/>
                                                <label for="radio1">No</label></li>


                                        </ul>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                                            <button class="save-btn" id="savecommunication">save</button>
                                            <span class="alert-msg">You can choose any one of the option </span></div>
                                    </div> <!--/mode-comm-inner -->
                                </li>
                            </ul>
                        </div> <!--/communication-tab-con -->
                        <div class="privacy-tab-con tab-pane" id="privacy">
                            <ul>
                                <!-- <li> <label>profile visibility</label> <button class="edit">edit </button> 
                                   <div class="profile-visibile">
                                      <h5>Who can see your profile on Rigalio </h5>
                                      <ul class="nopadding">
                                     <li><input id="radio1" type="radio" name="seeprofile" value="0" checked><label for="radio1">private</label></li>
                                     <li><input id="radio2" type="radio" name="seeprofile" value="1"><label for="radio2">members</label></li>
                                  </ul>
                                    </div> 
                                 </li>-->
                                <li><label>crowns</label>
                                    <button class="edit">edit</button>
                                    <div class="crown-info">
                                        <h5>Who is permitted to view your crowned items on Rigalio?</h5>
                                        <?php
                                            if (count($getuserdetail) == 0) {
                                                ?>
                                                <ul class="nopadding">
                                                    <li><input id="radio1" type="radio" name="seecrown" value="0"
                                                               class="radio1" checked><label for="radio1">Only
                                                            me</label></li>
                                                    <li><input id="radio2" type="radio" name="seecrown" value="1"
                                                               class="radio1"><label for="radio2">Followers</label></li>
                                                    <li><input id="radio3" type="radio" name="seecrown" value="2"
                                                               class="radio1"><label for="radio1">Members</label></li>
                                                </ul>
                                            <?php } else { ?>
                                                <ul class="nopadding">
                                                    <li><input id="radio1" type="radio" name="seecrown"
                                                               value="0" <?php echo ($getuserdetail[0]['crown_isactive'] == '0') ? "checked" : ""; ?>
                                                               class="radio1"><label for="radio1">Only me</label></li>
                                                    <li><input id="radio2" type="radio" name="seecrown"
                                                               value="1" <?php echo ($getuserdetail[0]['crown_isactive'] == '1') ? "checked" : ""; ?>
                                                               class="radio1"><label for="radio2">Followers</label></li>
                                                    <li><input id="radio3" type="radio" name="seecrown"
                                                               value="2" <?php echo ($getuserdetail[0]['crown_isactive'] == '2') ? "checked" : ""; ?>
                                                               class="radio1"><label for="radio1">Members</label></li>
                                                </ul>
                                            <?php } ?>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                                            <!--<input type="button" class="save-btn" value="save" id="savepass">-->
                                            <button id="savecrown" class="save-btn" type="button">save</button>
                                        </div>
                                    </div>
                                </li>
                                <li><label>comments</label>
                                    <button class="edit">edit</button>
                                    <div class="comments-setting">
                                        <h5>Who is permitted to view your commented items on Rigalio?</h5>
                                        <?php
                                            if (count($getuserdetail) == 0) {
                                                ?>
                                                <ul class="nopadding">
                                                    <li><input id="radio1" type="radio" name="seecomment" value="0"
                                                               class="radio1" checked><label for="radio1">Only
                                                            me</label></li>
                                                    <li><input id="radio2" type="radio" name="seecomment" value="1"
                                                               class="radio1"><label for="radio2">Followers</label></li>
                                                    <li><input id="radio3" type="radio" name="seecomment" value="2"
                                                               class="radio1"><label for="radio1">Members</label></li>
                                                </ul>
                                            <?php } else { ?>
                                                <ul class="nopadding">
                                                    <li><input id="radio1" type="radio" name="seecomment"
                                                               value="0" <?php echo ($getuserdetail[0]['comment_isactive'] == '0') ? "checked" : ""; ?>
                                                               class="radio1"><label for="radio1">Only me</label></li>
                                                    <li><input id="radio2" type="radio" name="seecomment"
                                                               value="1" <?php echo ($getuserdetail[0]['comment_isactive'] == '1') ? "checked" : ""; ?>
                                                               class="radio1"><label for="radio2">Followers</label></li>
                                                    <li><input id="radio3" type="radio" name="seecomment"
                                                               value="2" <?php echo ($getuserdetail[0]['comment_isactive'] == '2') ? "checked" : ""; ?>
                                                               class="radio1"><label for="radio1">Members</label></li>
                                                </ul>
                                            <?php } ?>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                                            <!--<input type="button" class="save-btn" value="save" id="savepass">-->
                                            <button id="savecomment" class="save-btn" type="button">save</button>
                                        </div>
                                    </div> <!-- /comments-setting-->
                                </li>
                                <!--    <li> <label>share</label> <button class="edit">edit </button>
                                      <div class="share-setting">
                                         <h5>Who can see your share on Rigalio </h5>
                                         <ul class="nopadding">
                                        <li><input id="radio1" type="radio" name="radio1" value="Item 1" class="radio1"><label for="radio1">Only me</label></li>
                                        <li><input id="radio2" type="radio" name="radio2" value="Item 2" class="radio1"><label for="radio2">Followers</label></li>
                                        <li><input id="radio3" type="radio" name="radio3" value="Item 3" class="radio1"><label for="radio1">Members</label></li>
                                     </ul>
                                       </div>
                                    </li>-->
                                <li><label>status</label>
                                    <button class="edit">edit</button>
                                    <div class="status-setting">
                                        <h5>Who is permitted to view your status on Rigalio?</h5>
                                        <?php
                                            if (count($getuserdetail) == 0) {
                                                ?>
                                                <ul class="nopadding">
                                                    <li><input id="radio1" type="radio" name="seestatus" value="0"
                                                               class="radio1" checked><label for="radio1">Only
                                                            me</label></li>
                                                    <li><input id="radio2" type="radio" name="seestatus" value="1"
                                                               class="radio1"><label for="radio2">Followers</label></li>
                                                    <li><input id="radio3" type="radio" name="seestatus" value="2"
                                                               class="radio1"><label for="radio1">Members</label></li>
                                                </ul>
                                            <?php } else { ?>
                                                <ul class="nopadding">
                                                    <li><input id="radio1" type="radio" name="seestatus"
                                                               value="0" <?php echo ($getuserdetail[0]['status_isactive'] == '0') ? "checked" : ""; ?>
                                                               class="radio1"><label for="radio1">Only me</label></li>
                                                    <li><input id="radio2" type="radio" name="seestatus"
                                                               value="1" <?php echo ($getuserdetail[0]['status_isactive'] == '1') ? "checked" : ""; ?>
                                                               class="radio1"><label for="radio2">Followers</label></li>
                                                    <li><input id="radio3" type="radio" name="seestatus"
                                                               value="2" <?php echo ($getuserdetail[0]['status_isactive'] == '2') ? "checked" : ""; ?>
                                                               class="radio1"><label for="radio1">Members</label></li>
                                                </ul>
                                            <?php } ?>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                                            <!--<input type="button" class="save-btn" value="save" id="savepass">-->
                                            <button id="savepost" class="save-btn" type="button">save</button>
                                        </div>
                                    </div> <!-- /status-setting-->
                                </li>
                                <li><label>photos</label>
                                    <button class="edit">edit</button>
                                    <div class="photos-setting">
                                        <h5>Who is permitted to view your photos on Rigalio?</h5>
                                        <?php
                                            if (count($getuserdetail) == 0) {
                                                ?>
                                                <ul class="nopadding">
                                                    <li><input id="radio1" type="radio" name="seephotos" value="0"
                                                               class="radio1" checked><label for="radio1">Only
                                                            me</label></li>
                                                    <li><input id="radio2" type="radio" name="seephotos" value="1"
                                                               class="radio1"><label for="radio2">Followers</label></li>
                                                    <li><input id="radio3" type="radio" name="seephotos" value="2"
                                                               class="radio1"><label for="radio1">Members</label></li>
                                                </ul>
                                            <?php } else { ?>
                                                <ul class="nopadding">
                                                    <li><input id="radio1" type="radio" name="seephotos"
                                                               value="0" <?php echo ($getuserdetail[0]['photos_isactive'] == '0') ? "checked" : ""; ?>
                                                               class="radio1"><label for="radio1">Only me</label></li>
                                                    <li><input id="radio2" type="radio" name="seephotos"
                                                               value="1" <?php echo ($getuserdetail[0]['photos_isactive'] == '1') ? "checked" : ""; ?>
                                                               class="radio1"><label for="radio2">Followers</label></li>
                                                    <li><input id="radio3" type="radio" name="seephotos"
                                                               value="2" <?php echo ($getuserdetail[0]['photos_isactive'] == '2') ? "checked" : ""; ?>
                                                               class="radio1"><label for="radio1">Members</label></li>
                                                </ul>
                                            <?php } ?>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                                            <!--<input type="button" class="save-btn" value="save" id="savepass">-->
                                            <button id="savephotos" class="save-btn" type="button">save</button>
                                        </div>
                                    </div> <!-- /photos-setting-->
                                </li>
                                <li><label>people</label>
                                    <button class="edit">edit</button>
                                    <div class="ppl-fol-setting">
                                        <h5>Who is permitted to view which individuals you engage with on Rigalio?</h5>
                                        <?php
                                            if (count($getuserdetail) == 0) {
                                                ?>
                                                <ul class="nopadding">
                                                    <li><input id="radio1" type="radio" name="seefollower" value="0"
                                                               class="radio1" checked><label for="radio1">Only
                                                            me</label></li>
                                                    <li><input id="radio2" type="radio" name="seefollower" value="1"
                                                               class="radio1"><label for="radio2">Followers</label></li>
                                                    <li><input id="radio3" type="radio" name="seefollower" value="2"
                                                               class="radio1"><label for="radio1">Members</label></li>
                                                </ul>
                                            <?php } else { ?>
                                                <ul class="nopadding">
                                                    <li><input id="radio1" type="radio" name="seefollower"
                                                               value="0" <?php echo ($getuserdetail[0]['people_isactive'] == '0') ? "checked" : ""; ?>
                                                               class="radio1"><label for="radio1">Only me</label></li>
                                                    <li><input id="radio2" type="radio" name="seefollower"
                                                               value="1" <?php echo ($getuserdetail[0]['people_isactive'] == '1') ? "checked" : ""; ?>
                                                               class="radio1"><label for="radio2">Followers</label></li>
                                                    <li><input id="radio3" type="radio" name="seefollower"
                                                               value="2" <?php echo ($getuserdetail[0]['people_isactive'] == '2') ? "checked" : ""; ?>
                                                               class="radio1"><label for="radio1">Members</label></li>
                                                </ul>
                                            <?php } ?>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                                            <!--<input type="button" class="save-btn" value="save" id="savepass">-->
                                            <button id="savefollower" class="save-btn" type="button">save</button>
                                        </div>
                                    </div> <!-- /people follow-setting-->
                                </li>
                                <li><label>Brand</label>
                                    <button class="edit">edit</button>
                                    <div class="brandfol-setting">
                                        <h5>Who is permitted to view which brands you engage with on Rigalio?</h5>
                                        <?php
                                            if (count($getuserdetail) == 0) {
                                                ?>
                                                <ul class="nopadding">
                                                    <li><input id="radio1" type="radio" name="seebrand" value="0"
                                                               class="radio1"><label for="radio1">Only me</label></li>
                                                    <li><input id="radio2" type="radio" name="seebrand" value="1"
                                                               class="radio1"><label for="radio2">Followers</label></li>
                                                    <li><input id="radio3" type="radio" name="seebrand" value="2"
                                                               class="radio1"><label for="radio1">Members</label></li>
                                                </ul>
                                            <?php } else { ?>

                                                <ul class="nopadding">
                                                    <li><input id="radio1" type="radio" name="seebrand"
                                                               value="0" <?php echo ($getuserdetail[0]['brand_isactive'] == '0') ? "checked" : ""; ?>
                                                               class="radio1"><label for="radio1">Only me</label></li>
                                                    <li><input id="radio2" type="radio" name="seebrand"
                                                               value="1" <?php echo ($getuserdetail[0]['brand_isactive'] == '1') ? "checked" : ""; ?>
                                                               class="radio1"><label for="radio2">Followers</label></li>
                                                    <li><input id="radio3" type="radio" name="seebrand"
                                                               value="2" <?php echo ($getuserdetail[0]['brand_isactive'] == '2') ? "checked" : ""; ?>
                                                               class="radio1"><label for="radio1">Members</label></li>
                                                </ul>
                                            <?php } ?>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                                            <!--<input type="button" class="save-btn" value="save" id="savepass">-->
                                            <button id="savebrand" class="save-btn" type="button">save</button>
                                        </div>
                                    </div> <!-- /brandfolowing-setting-->
                                </li>
                                <li><label>Collection</label>
                                    <button class="edit">edit</button>
                                    <div class="brandfol-setting">
                                        <h5>Who can see your collection on Rigalio </h5>
                                        <?php
                                            if (count($getuserdetail) == 0) {
                                                ?>
                                                <ul class="nopadding">
                                                    <li><input id="radio1" type="radio" name="seecollection" value="0"
                                                               class="radio1"><label for="radio1">Only me</label></li>
                                                    <li><input id="radio2" type="radio" name="seecollection" value="1"
                                                               class="radio1"><label for="radio2">Followers</label></li>
                                                    <li><input id="radio3" type="radio" name="seecollection" value="2"
                                                               class="radio1"><label for="radio1">Members</label></li>
                                                </ul>
                                            <?php } else { ?>

                                                <ul class="nopadding">
                                                    <li><input id="radio1" type="radio" name="seecollection"
                                                               value="0" <?php echo ($getuserdetail[0]['collection_isactive'] == '0') ? "checked" : ""; ?>
                                                               class="radio1"><label for="radio1">Only me</label></li>
                                                    <li><input id="radio2" type="radio" name="seecollection"
                                                               value="1" <?php echo ($getuserdetail[0]['collection_isactive'] == '1') ? "checked" : ""; ?>
                                                               class="radio1"><label for="radio2">Followers</label></li>
                                                    <li><input id="radio3" type="radio" name="seecollection"
                                                               value="2" <?php echo ($getuserdetail[0]['collection_isactive'] == '2') ? "checked" : ""; ?>
                                                               class="radio1"><label for="radio1">Members</label></li>
                                                </ul>
                                            <?php } ?>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                                            <!--<input type="button" class="save-btn" value="save" id="savepass">-->
                                            <button id="savecollection" class="save-btn" type="button">save</button>
                                        </div>
                                    </div> <!-- /brandfolowing-setting-->
                                </li>

                                <li><label>Timeline</label>
                                    <button class="edit">edit</button>
                                    <div class="share-setting">
                                        <h5>Who can see your Timeline on Rigalio </h5>
                                        <ul class="nopadding">
                                            <li><input id="radio2" type="radio"
                                                       name="seetimeline" <?php if ($user_all_detials[0]['timeline_isactive'] == 0) {
                                                    echo "checked";
                                                } ?> value="0" class="radio1"><label for="radio1">Only me</label></li>
                                            <li><input id="radio2" type="radio" name="seetimeline"
                                                       value="1" <?php if ($user_all_detials[0]['timeline_isactive'] == 1) {
                                                    echo "checked";
                                                } ?> class="radio1"><label for="radio2">Followers</label></li>
                                            <li><input id="radio2" type="radio" name="seetimeline"
                                                       value="2" <?php if ($user_all_detials[0]['timeline_isactive'] == 2) {
                                                    echo "checked";
                                                } ?> class="radio1"><label for="radio2">Members</label></li>
                                        </ul>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                                            <!--<input type="button" class="save-btn" value="save" id="savepass">-->
                                            <button id="savetimeline" class="save-btn" type="button">save</button>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div> <!--/privacy-tab-con -->
                    </div>
                </div> <!--/settings-blocks -->

            </div> <!--/settings-pg-con -->
        </div>
    </div>
</div>
<!--settings-pg-con ends -->

<script>
    $.toggleShowPassword({
        field: '#newpass',
        control: '#test2'
    });

    // date picker script
    $(function () {
        $("#newdob").datepicker();
    });

    function existuser() {
        var username = $("#user_username").val();
        // var username = document.getElementById("username123");
        var data = {
            "username": username
        };
        $.ajax({
            type: "POST",
            url: "" + base_url + "main/userexist",
            // url: "http://localhost/finalrigalio/main/inviteuserlogin?username=" + username + "&password=" + password,
            data: data,
            //crossDomain:true,
            success: function (html) {
                //alert(html);
                var inviteuser = html;
                if (inviteuser == 1) {
                    //alert(html);
                   // document.getElementById('usererror').innerHTML = "User Exist !";
                   // $("#usererror").html("user exist");
                    $("#alert-msg").text("User Exist"); $(".alert-sectn").fadeIn();
                    setTimeout(function () {
                        $('.alert-sectn').fadeOut();
                    }, 3000);
                }
                else {
                    $("#usererror").html("");
                }
            }
        });
    }

</script>

