<?php

//print_r($editorial_cat_data);
?>
<div class="category-pg-con editorial-pd col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
    <div class="category-pg-inner col-lg-11 col-md-11 col-sm-12 col-xs-12">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 tagline-inner nopadding">
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 nopadding breadcrumb">
                        <span>You are here: </span>
                        <ul>
                            <li><a href="<?php echo base_url();?>">Home</a></li>
                            <li><a href="#"> Editorial </a></li>
                        </ul>
                    </div> <!--/breadcrumb -->
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 nopadding category-pg-tag">
                        <ul class="nopadding">
                          <li><span class="category-iconic icomoon cat_editorial icon-timepiece-jewellery"></span> </li>
                          <li><h3><?php echo $editorial_cat_data[0]['editorial_subcat_name'];?></h3> </li>
                        </ul>
                        
                    </div> <!--/category-pg-tag -->
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 nopadding category_pg">
                        <div class="follow-status" id="2">
                            <button data-text="Follow curator" class="follow-cat-btn normal follow-cat cat_editorial">
                                <i class="fa fa-check"></i></button>
                        </div>
                    </div>
                </div> <!--/tagline-inner -->
            </div>
        </div>
    </div> <!-- /category-pg-inner-->
</div>  <!--/category-pg-con -->
<div class="main_content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 wall editorial-blocks1 whats_new_content">
                <div class="grid" id="masonry-grid">

                    <?php 
					foreach($editorial_cat_data as $editorial_cat_val ) 
					//print_r($editorial_cat_val); exit;
					{ ?>
                    <div class="wall-column grid-item">
                        <div class="wall-item">
                            <div class="category cat_editorial">
                                <div class="category_img" data-url="<?php echo base_url(); ?>editorial/artical/<?php echo $editorial_cat_val['editorial_id']; ?>">
                                    <img src="<?php echo base_url().$editorial_cat_val['cproduct_image']; ?>" class="img-responsive">
                                    <div class="editorial-icon"> <img src="<?php echo base_url(); ?>content/images/icons/editorial-icon-block.png" class="img-responsive"> </div>
                                    <div class="hover-content-cat" data-url="<?php echo base_url(); ?>editorial/artical/<?php echo $editorial_cat_val['editorial_id']; ?>">
                                        <div class="tab">
                                            <div class="tab-in" id="<?php echo $editorial_cat_val['curator_id']; ?>">
                                                <a href="javascript:void(0)" class="myan follow-brand-sectn" id="32">
                                                    <button class="follow-curator-btn follow" data-text="Follow Curator"><i class="fa fa-check"></i></button> </a>
                                            </div>  <!--/tab-in -->
                                        </div> <!--/tab -->
                                    </div> <!--/hover-content-cat -->
                                </div>
                                <div class="category_content curated-con">
                                    <div class="cur-cat"><span><?php echo $editorial_cat_val['category_Name']; ?></span></div>
                                    <a href="<?php echo base_url(); ?>/editorial/artical/<?php echo $editorial_cat_val['cproduct_id']; ?>">
                                        <h3><a href="<?php echo base_url(); ?>editorial/artical/<?php echo $editorial_cat_val['editorial_id']; ?>"> <?php echo $editorial_cat_val['cproduct_Title']; ?></h3></a>
                                    <h2><?php echo $editorial_cat_val['cproductSmallDiscription']; ?></h2>
                                </div> <!--/category_content-->
                                <div class="cur-infor">
                                    <a href="<?php echo  base_url(); ?>editorial/curator/<?php echo $editorial_cat_val['curator_id']; ?>"><span class="pf-img" style="background:url(<?php echo base_url(); ?><?php echo $editorial_cat_val['curator_picture']; ?>);"> </span>
                                        <span class="cur-name"><?php echo $editorial_cat_val['curator_name']; ?></span></a> <a target="_blank" href="#"><span class="read-more">Read More </span></a>
                                </div>
                                <div class="category_options">
                                    <table>
                                        <tbody><tr>
                                            <td class="date-status cat_editorial">
                                                <p><?php
                                                        $now = time(); $startDate=$editorial_cat_val['editorial_datetime']; // or your date as well
                                                        $your_date = strtotime($startDate);
                                                        $datediff = $now - $your_date;
                                                        echo floor(($datediff/(60*60*24))+1); ?> days ago</p>
                                            </td>
                                            <?php
                                                            $me = [];
                                                            foreach($user_crown as $user_foll){
                                                                
                                                                    $me[] = $user_foll['cproduct_id'];
                                                            }
                                                            $flag=1;
                                                            for($i=0;$i<count($me);$i++){
                                                            if($editorial_cat_val['cproduct_id'] ==$me[$i]){
                                                                $flag=0;
                                                                break;
                                                            }
                                                            } 
                                                            if($flag==0)
                                                            {?> 
                     <td class="crown-sectn11 cproduct-crown active" id="<?php echo $editorial_cat_val['cproduct_id']; ?>"> <span id="cproductcrowncount"><?php foreach ($this->getdata->cproduct_count_crown($editorial_cat_val['cproduct_id']) as $count_no) {
                                echo $count_no['no'];
                            } ?></span> <span class="icomoon icon-crown"> </span></td>
                            <?php } 
                                                           else { ?> 
                                                           
                                                             <td class="crown-sectn11 cproduct-crown" id="<?php echo $editorial_cat_val['cproduct_id']; ?>"> <span id="cproductcrowncount"><?php foreach ($this->getdata->cproduct_count_crown($editorial_cat_val['cproduct_id']) as $count_no) {
                                echo $count_no['no'];
                            } ?></span> <span class="icomoon icon-crown"> </span></td>
                            <?php }  ?> 
                            
                                          <!--  <td class="crown-sectn cat_editorial" id="59"><span id="crowncount">0 </span>
                                                <span class="icomoon icon-crown"> </span></td>-->
                                                
                                            <td class="comment-sectn cat_editorial">
                                                <a href="google.com">
                                                    <span class="text">0 </span> <span class="icomoon icon-chat"> </span></a></td>
                                            <td class="fwd-icon-sectn cat_editorial" id="fwd-id1"><span> </span> <span class="icomoon icon-sharing"></span></td>
                                        </tr>
                                        </tbody></table>

                                    <div class="fwd-social-icons sec7" style="display:none;">
                                        <ul class="cat-follow-icons">
                                            <li>
                                                <a target="_blank" href="#" class="fb"></a>
                                            </li>
                                            <li>
                                                <a target="_blank" href="#" class="twitter"></a>
                                            </li>
                                            <li class="show-more"><a class="more" href="javascript:void(0)"></a>
                                            </li>
                                        </ul>
                                        <div class="hidden-more-icons" style="display:none;">
                                            <ul class="follow-icons-more">
                                                <li>

                                                    <a target="_blank" href="#" class="linkdin"></a>
                                                </li>
                                                <li>
                                                    <a target="_blank" href="#" class="gplus"></a>

                                                </li>
                                                <li>
                                                    <a target="_blank" href="#" class="pini"></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div> <!--/inner forward icons social -->
                                </div> <!--/category_options-->

                            </div> <!--/category-->
                        </div> <!--/wall-item -->
                    </div>  <!--/wall-column -->

<?php } ?>


                </div> <!--/masonry-grid -->
            </div>
        </div>
    </div>
</div> <!--/main_content -->

</body>
<script src="<?php echo base_url(); ?>content/js/hover.js"></script>
<script type="text/javascript">
    onsload();
</script>
</html>
