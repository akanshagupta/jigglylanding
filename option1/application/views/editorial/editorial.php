<?php

//print_r($editorial);
?>
<div class="category-pg-con editorial-pd col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
    <div class="category-pg-inner col-lg-11 col-md-11 col-sm-12 col-xs-12">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 tagline-inner nopadding">
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 nopadding breadcrumb">
                        <span>You are here: </span>
                        <ul>
                            <li><a href="<?php echo base_url();?>">Home</a></li>
                            <li><a href="#"> Editorial </a></li>
                        </ul>
                    </div> <!--/breadcrumb -->
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 nopadding category-pg-tag">
                        <ul class="nopadding">
                          <li><span class="category-iconic icomoon cat_editorial icon-timepiece-jewellery"></span> </li>
                          <li><h3>Editorial</h3> </li>
                        </ul>
                    </div> <!--/category-pg-tag -->
                   <!-- <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 nopadding category_pg">
                        <button class="sub-cat-btn cat_editorial">sub category</button>
                        <div class="follow-status" id="2">
                            <button data-text="Follow curator" class="follow-cat-btn normal follow-cat cat_editorial">
                                <i class="fa fa-check"></i></button>
                        </div>
                    </div>-->
                </div> <!--/tagline-inner -->
                <!--<div class="hidden-cat-optns" style="display: none;">
                    <ul>
                        <li class="cat_hotel"><a href="http://www.rigalio.com/subcategory/hotels-and-resorts/15 "><span class="icomoon icon-hotels-resorts2">  </span>
                                <h4>Hotels and Resorts </h4></a></li>
                        <li class="cat_hotel"><a href="http://www.rigalio.com/subcategory/vacations/16 "><span class="icomoon icon-vacations2">  </span>
                                <h4>Vacations </h4></a></li>
                        <li class="cat_hotel"><a href="http://www.rigalio.com/subcategory/fine-dining/19 "><span class="icomoon icon-fine-dinning2">  </span>
                                <h4>Fine Dining </h4></a></li>
                        <li class="cat_hotel"><a href="http://www.rigalio.com/subcategory/art-and-design/21 "><span class="icomoon icon-art-design2">  </span>
                                <h4>Art and Design </h4></a></li>
                    </ul>
                </div>-->
            </div>
        </div>
    </div> <!-- /category-pg-inner-->
</div>  <!--/category-pg-con -->
<div class="main_content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-10 col-md-10 col-sm-11 col-xs-12 wall editorial-collection whats_new_content">
                <div class="grid" id="masonry-grid">

                    <?php foreach($editorial as $editorial_val) { ?>
                    <div class="wall-column grid-item">
                        <div class="wall-item">
                            <div class="category collection-category">
                                <div class="category_img" data-url="<?php echo base_url()."editorial/editorial_subcat/".$editorial_val['editorial_subcat_name']."/".$editorial_val['editorial_subcat_id']; ?>">
                                    <img src="<?php echo base_url().$editorial_val['editorial_image']; ?>" class="img-responsive">
                                    <div class="hover-content-cat collection-cat-hover" data-url="<?php echo base_url()."editorial/editorial_subcat/".$editorial_val['editorial_subcat_name']."/".$editorial_val['editorial_subcat_id']; ?>">

                                            <div class="tab">
                                                <div class="tab-cell">
                                                    <span class="cat-name"> <a href="<?php echo base_url()."editorial/editorial_subcat/".$editorial_val['editorial_subcat_name']."/".$editorial_val['editorial_subcat_id']; ?>"><?php echo $editorial_val['editorial_subcat_name']; ?> </a></span>
                                                  <hr><button class="explore-btn"> explore </button>
                                                </div>
                                            </div>

                                    </div> <!--/hover-content-cat -->
                                </div> <!--/category_img -->
                            </div> <!-- /category-->
                        </div>
                    </div>


             <?php } ?>


                </div> <!--/masonry-grid -->
            </div>
        </div>
    </div>
</div> <!--/main_content -->

</body>
<script src="<?php echo base_url(); ?>content/js/hover.js"></script>
<script type="text/javascript">
    onsload();
</script>
</html>
