<!doctype html>
<html lang="en" class="no-js">
<head>
<script type="text/javascript" src="https://platform.linkedin.com/in.js">
  api_key: 756jk5lm9azphk
  //scope: r_basicprofile r_emailaddress r_contactinfo
 // onLoad: onLinkedInLoad
  authorize: true
</script>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    

	<link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700|Merriweather:400italic,400' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="<?php echo base_url(); ?>content/landingpage/css/reset.css"> <!-- CSS reset -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>content/landingpage/css/style.css"> <!-- Resource style -->
	<script src="<?php echo base_url(); ?>content/landingpage/js/modernizr.js"></script> <!-- Modernizr -->
	<link rel="icon" type="image/png" href="<?php echo base_url(); ?>content/landingpage/img/favicon.png">
        <script type="text/javascript" src="<?php echo base_url(); ?>/content/js/fblogin.js"></script>
	<title>Rigalio</title>
    <meta property="og:title" content="Request an Invite to access the World of Luxury" />
<meta property="og:type" content="product" />
<meta property="og:image" content="<?php echo base_url(); ?>content/landingpage/img/facebook-share-image-1.jpg" />
<meta property="og:image" content="<?php echo base_url(); ?>content/landingpage/img/facebook-share-image-2.jpg" />
<meta property="og:image" content="<?php echo base_url(); ?>content/landingpage/img/facebook-share-image-3.jpg" />
<meta property="og:url" content="https://www.facebook.com/rigalio" />
<meta property="og:description" content="Rigalio is one of a kind invite only digital luxury cataloguing platform providing its consumers the finest and most exclusive insights into the world of luxury. For an unedited dose of luxury create your own timeline and enjoy a personalised experience." />
</head>
<body>
<div class="landscape-pg" style="display:none;"><img src="<?php echo base_url(); ?>content/landingpage/img/turndevice.gif"> </div>
<script>
   (function (i, s, o, g, r, a, m) {
       i['GoogleAnalyticsObject'] = r;
       i[r] = i[r] || function () {
               (i[r].q = i[r].q || []).push(arguments)
           }, i[r].l = 1 * new Date();
       a = s.createElement(o),
           m = s.getElementsByTagName(o)[0];
       a.async = 1;
       a.src = g;
       m.parentNode.insertBefore(a, m)
   })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

   ga('create', 'UA-80647225-1', 'auto');
   ga('send', 'pageview');

</script>
	<div class="projects-container">
		<div class="alert-sectn" id="alert-sectn" style="display: none;">
        <h4 id="alert-msg"></h4>
    </div>
        <ul>
			<li class="cd-single-project project1 is-loaded">
				<div class="cd-title">
					<h2>Infinite Paradise</h2>
					<p>More</p>
				</div> <!-- .cd-title -->

				<div class="cd-project-info">
                
					<div class="inner-con">
					    <div class="logo-part"> <img src="<?php echo base_url(); ?>content/landingpage/img/logo.png" class="img-responsive"> </div>
                        
					    <h3>Infinite Paradise</h3> <hr>
					    <p>Rigalio is one of a kind digital luxury cataloguing platform providing its consumers the finest and most exclusive insights into the world of luxury. In addition to this the registered members are allowed access to Rigalio’s Luxury Newswire. This section highlights interesting product launches along with other luxury news from the world.</p>
                        
                        <div class="foot-con">
                         <p id="msg2" class="msg2 langinpg"></p>
                         <div class="invite-part"> <a href="#" class="invite-btn"> <i> <img src="<?php echo base_url(); ?>content/landingpage/img/invite-icon.png" class="img-responsive"> </i> request invite  </a> </div>
                        <div class="invite-via-sym" style="display:none;">
						    <ul>
                            <!--<li class="via-fb"><fb:login-button size="xlarge" max_rows="7" scope="email,user_friends" data-auto-logout-link="true" onlogin="checkLoginState();"> </fb:login-button><a class="register-fb fb_login" onlogin="checkLoginState();"></a></li>-->
						      <li class="via-fb "> <div class="register-fb"><fb:login-button size="small" max_rows="3" scope="email,user_friends" data-auto-logout-link="true" onlogin="checkLoginState();"> </fb:login-button><a class="register-fb fb_login" onlogin="checkLoginState();"></a> </div> </li>
						      <li class="via-link"> <div class="linkedin-log"><script type="in/Login"></script></div>
                        <div id="profiles"></div> </li>
						    </ul>
						  </div>
						 <div class="visit-site">Visit <a href="http://www.rigalio.com/?utm_source=rigalio&utm_medium=LP&utm_campaign=Invitation%20" target="_blank">Rigalio</a></div>
						<div class="social-icons">
						  <ul>
						    <li>Follow Rigalio: </li>
                            <li> <a href="https://www.facebook.com/rigalio" target="_blank"> <img src="<?php echo base_url(); ?>content/landingpage/img/facebook.png"> </a></li>
						    <li><a href="https://plus.google.com/u/0/116609420281563249981/posts" target="_blank"> <img src="<?php echo base_url(); ?>content/landingpage/img/gplus.png"> </a></li>
						    <li><a href="https://www.linkedin.com/company/7604532" target="_blank"> <img src="<?php echo base_url(); ?>content/landingpage/img/linkedin.png"> </a></li>
						    <li><a href="http://www.twitter.com/rigalioluxury" target="_blank"> <img src="<?php echo base_url(); ?>content/landingpage/img/twitter.png"> </a></li>
						    <li><a href="http://www.instagram.com/rigalio" target="_blank"> <img src="<?php echo base_url(); ?>content/landingpage/img/instagram.png"> </a></li></ul>
						 </div>
                        </div> <!--/foot-con -->
						
					</div> 
				</div> <!-- .cd-project-info -->
			</li>

			<li class="cd-single-project project2 is-loaded">
				<div class="cd-title">
					<h2>Scintillatingly Luxurious</h2>
					<p>More</p>
				</div> <!-- .cd-title -->

				<div class="cd-project-info">
					<div class="inner-con">
					    <div class="logo-part"> <img src="<?php echo base_url(); ?>content/landingpage/img/logo.png" class="img-responsive"> </div>
					    <h3>Scintillatingly Luxurious</h3> <hr>
					    <p>Rigalio is an exclusive members’ only website offering its discerning clients an insight into the world of luxury. The registered members can browse products, follow brands and categories and further create their own inner circle.</p>
                        
                        <div class="foot-con">
                         <p id="msg2" class="msg2 langinpg"></p>
                         <div class="invite-part"> <a href="#" class="invite-btn"> <i> <img src="<?php echo base_url(); ?>content/landingpage/img/invite-icon.png" class="img-responsive"> </i> request invite  </a> </div>
                          <div class="invite-via-sym" style="display:none;">
						    <ul>
                            <!--<li class="via-fb"><fb:login-button size="xlarge" max_rows="7" scope="email,user_friends" data-auto-logout-link="true" onlogin="checkLoginState();"> </fb:login-button><a class="register-fb fb_login" onlogin="checkLoginState();"></a></li>-->
						      <li class="via-fb "> <div class="register-fb"><fb:login-button size="small" max_rows="3" scope="email,user_friends" data-auto-logout-link="true" onlogin="checkLoginState();"> </fb:login-button><a class="register-fb fb_login" onlogin="checkLoginState();"></a> </div> </li>
						      <li class="via-link"> <div class="linkedin-log"><script type="in/Login"></script></div>
                        <div id="profiles"></div> </li>
						    </ul>
						  </div>
						<div class="visit-site">Visit <a href="http://www.rigalio.com/?utm_source=rigalio&utm_medium=LP&utm_campaign=Invitation%20" target="_blank">Rigalio</a></div>
						<div class="social-icons">
						  <ul>
						    <li>Follow Rigalio: </li>
                            <li> <a href="https://www.facebook.com/rigalio" target="_blank"> <img src="<?php echo base_url(); ?>content/landingpage/img/facebook.png"> </a></li>
						    <li><a href="https://plus.google.com/u/0/116609420281563249981/posts" target="_blank"> <img src="<?php echo base_url(); ?>content/landingpage/img/gplus.png"> </a></li>
						    <li><a href="https://www.linkedin.com/company/7604532" target="_blank"> <img src="<?php echo base_url(); ?>content/landingpage/img/linkedin.png"> </a></li>
						    <li><a href="http://www.twitter.com/rigalioluxury" target="_blank"> <img src="<?php echo base_url(); ?>content/landingpage/img/twitter.png"> </a></li>
						    <li><a href="http://www.instagram.com/rigalio" target="_blank"> <img src="<?php echo base_url(); ?>content/landingpage/img/instagram.png"> </a></li></ul>
						 </div>
                        </div> <!--/foot-con -->
						
					</div>  
				</div> <!-- .cd-project-info -->
			</li>

			<li class="cd-single-project project3 is-loaded">
				<div class="cd-title">
					<h2>Exclusivity Personified</h2>
					<p>More</p>
				</div> <!-- .cd-title -->

				<div class="cd-project-info">
					<div class="inner-con">
					    <div class="logo-part"> <img src="<?php echo base_url(); ?>content/landingpage/img/logo.png" class="img-responsive"> </div>
					    <h3>Exclusivity Personified</h3> <hr>
					    <p>By being a part of Rigalio, you get to avail exclusive invites to launch events, new products, auctions and offers by the various luxury brands. For an unedited dose of luxury create your own timeline and get a personalised experience.</p>
                         
                        <div class="foot-con">
                        <p id="msg2" class="msg2 langinpg"></p>
                         <div class="invite-part"> <a href="#" class="invite-btn"> <i> <img src="<?php echo base_url(); ?>content/landingpage/img/invite-icon.png" class="img-responsive"> </i> request invite  </a> </div>
                        <div class="invite-via-sym" style="display:none;">
						    <ul>
                            <!--<li class="via-fb"><fb:login-button size="xlarge" max_rows="7" scope="email,user_friends" data-auto-logout-link="true" onlogin="checkLoginState();"> </fb:login-button><a class="register-fb fb_login" onlogin="checkLoginState();"></a></li>-->
						      <li class="via-fb "> <div class="register-fb"><fb:login-button size="small" max_rows="3" scope="email,user_friends" data-auto-logout-link="true" onlogin="checkLoginState();"> </fb:login-button><a class="register-fb fb_login" onlogin="checkLoginState();"></a> </div> </li>
						      <li class="via-link"> <div class="linkedin-log"><script type="in/Login"></script></div>
                        <div id="profiles"></div> </li>
						    </ul>
						  </div>
						<div class="visit-site">Visit <a href="http://www.rigalio.com/?utm_source=rigalio&utm_medium=LP&utm_campaign=Invitation%20" target="_blank">Rigalio</a></div>
						<div class="social-icons">
						  <ul>
						    <li>Follow Rigalio: </li>
                            <li> <a href="https://www.facebook.com/rigalio" target="_blank"> <img src="<?php echo base_url(); ?>content/landingpage/img/facebook.png"> </a></li>
						    <li><a href="https://plus.google.com/u/0/116609420281563249981/posts" target="_blank"> <img src="<?php echo base_url(); ?>content/landingpage/img/gplus.png"> </a></li>
						    <li><a href="https://www.linkedin.com/company/7604532" target="_blank"> <img src="<?php echo base_url(); ?>content/landingpage/img/linkedin.png"> </a></li>
						    <li><a href="http://www.twitter.com/rigalioluxury" target="_blank"> <img src="<?php echo base_url(); ?>content/landingpage/img/twitter.png"> </a></li>
						    <li><a href="http://www.instagram.com/rigalio" target="_blank"> <img src="<?php echo base_url(); ?>content/landingpage/img/instagram.png"> </a></li></ul>
						 </div>
                        </div> <!--/foot-con -->
						
					</div> 
				</div> <!-- .cd-project-info -->
			</li>

			<li class="cd-single-project project4 last-col is-loaded">
				<div class="cd-title">
				   <div class="main-logo2"> <a href="http://www.rigalio.com/?utm_source=rigalio&utm_medium=LP&utm_campaign=Invitation%20" target="_blank"><img src="<?php echo base_url(); ?>content/landingpage/img/rigaliologo.png"></a> </div>
				
					<form>
	<h2>Request an invite</h2>
 <p id="msg2" class="msg2"></p>
					  <ul>
					    <li> <div class="invite-btns"><span> Request via Facebook</span> </div> 
					       <div class="register-fb"><fb:login-button size="xlarge" max_rows="7" scope="email,user_friends" data-auto-logout-link="true" onlogin="checkLoginState();"> </fb:login-button><a class="register-fb fb_login" onlogin="checkLoginState();"></a></div> </li>
                     
					     <li> <div class="invite-btns"><span> Request via Linkedin</span> </div> 
					    	 <div class="linkedin-log"><script type="in/Login"></script></div>
					    
                        <div id="profiles"></div>
</li>
					  </ul>
					</form>
                    <div class="foot-con">
                     
                   
					 <div class="sharing-list" style="display:none;">
					  <ul>
					    <li> <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url();?>main/invite" target="_blank"> <img src="<?php echo base_url(); ?>content/landingpage/img/facebook-share.png" class="img-responsive"></a> </li>
                         <li> <a href="https://plus.google.com/share?url=<?php echo base_url();?>main/invite" target="_blank"><img src="<?php echo base_url(); ?>content/landingpage/img/google-plus-share.png" class="img-responsive"></a> </li>
                         
                          <li> <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo base_url();?>main/invite" target="_blank"><img src="<?php echo base_url(); ?>content/landingpage/img/linkedin-share.png" class="img-responsive"></a> </li>
                         
					    <li> 
                        
                        <a href="https://twitter.com/home?status=<?php echo base_url();?>main/invite" target="_blank"><img src="<?php echo base_url(); ?>content/landingpage/img/twitter-share.png" class="img-responsive"></a> </li>
					   
					   
                        <li>
                       <img id="copyButton" src="<?php echo base_url(); ?>content/landingpage/img/copy-url.png" class="img-responsive"> 
                        </li> 
					  </ul>
					</div>
                     <div class="share-pg"><a> Share this Page </a></div>
					 <div class="social-icons">
						  <ul>
                            <li>Follow Rigalio: </li>
						    <li> <a href="https://www.facebook.com/rigalio" target="_blank"> <img src="<?php echo base_url(); ?>content/landingpage/img/facebook.png"> </a></li>
						    <li><a href="https://plus.google.com/u/0/116609420281563249981/posts" target="_blank"> <img src="<?php echo base_url(); ?>content/landingpage/img/gplus.png"> </a></li>
						    <li><a href="https://www.linkedin.com/company/7604532" target="_blank"> <img src="<?php echo base_url(); ?>content/landingpage/img/linkedin.png"> </a></li>
						    <li><a href="http://www.twitter.com/rigalioluxury" target="_blank"> <img src="<?php echo base_url(); ?>content/landingpage/img/twitter.png"> </a></li>
						    <li><a href="http://www.instagram.com/rigalio" target="_blank"> <img src="<?php echo base_url(); ?>content/landingpage/img/instagram.png"> </a></li></ul>
						 </div>
					<p> By clicking on the Request button, you agree with the<a href="http://www.rigalio.com/main/terms_conditions?utm_source=rigalio&utm_medium=LP&utm_campaign=Invitation%20" target="_blank"> Terms and Conditions</a> and <a href="http://www.rigalio.com/main/privacy?utm_source=rigalio&utm_medium=LP&utm_campaign=Invitation%20" target="_blank">Privacy Policy</a> of Rigalio</p>
                    </div> <!--/foot-con -->
				</div> <!-- .cd-title -->

				<div class="cd-project-info">
				</div> <!-- .cd-project-info -->
			</li>
		</ul>
		<a href="#0" class="cd-close">Close</a>
		<a href="#0" class="cd-scroll">Scroll</a>
        <input type="text" id="copyTarget" style="opacity:0;" value="<?php
$url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
echo $url;
?>"> 
	</div> <!-- .project-container -->
     
<script src="<?php echo base_url(); ?>content/landingpage/js/jquery-2.1.1.js"></script>
<script src="<?php echo base_url(); ?>content/landingpage/js/main.js"></script> <!-- Resource jQuery -->
<script>
var base_url = 'http://www.rigalio.com/';
function onLinkedInLoad() {
		 var refnameinkdin = '';
 
    IN.Event.on(IN, "auth", onLinkedInAuth);
  }
  
  // 2. Runs when the viewer has authenticated
  function onLinkedInAuth() {
      refnameinkdin = $('#refvariable').html();
	 // console.log(refnameinkdin);
   var myinfo =  IN.API.Profile("me").fields("id","first-name", "last-name", "email-address", "headline", "siteStandardProfileRequest", "picture-url", "industry").result(displayProfiles);
    
  }

  // 2. Runs when the Profile() API call returns successfully
   function displayProfiles(profiles) {
//console.log(profiles);
  var member = profiles.values[0];
  $('.loader-part').show();
  refnameinkdin = $('#refvariable').html();
  //console.log(refnameinkdin);
//console.log(profiles);
//console.log(member);
if(refnameinkdin)
{
	 var data = {
		"refnameinkdin ":  refnameinkdin,
        "fname":  member.firstName,
        "lname":  member.lastName,
		"email": member.emailAddress,
		"headline": member.headline, 
		"industry": member.industry, 
		"picture": member.pictureUrl,
		"link": member.siteStandardProfileRequest.url
    };
	$.ajax({
        type: "POST",
        url: "" + base_url + "main/frendreflinkedin",
        data: data,
        success: function (html) {
           // var me = html
		   //alert(html);
if(html==1){
$('.loader-part').hide();
$('#refermsg2').html("You have already requested an invite.");
}else {
 $('.loader-part').hide();
$('#refermsg2').html("Thank you for your interest, We will facililate your invite and get back to you soon.");
 $('.aftersubmitrefer').hide();
}
        
        }
    });  //ajax ends here
	
	
}
else
{
    var data = {
        "fname":  member.firstName,
        "lname":  member.lastName,
		"email": member.emailAddress,
		//"phoneno": member.phoneNumbers,
		"headline": member.headline, 
		"industry": member.industry, 
		"picture": member.pictureUrl,
		"link": member.siteStandardProfileRequest.url
    };
	
   $.ajax({
        type: "POST",
        url: "" + base_url + "main/reqinvitelinkedin",
        data: data,
        success: function (html) {
           // var me = html
		  // alert(html);
		  if(html==1){

$('.msg2').html("You have already requested an invite.");
}else {

$('.msg2').html("Thank you for your interest, We will facililate your invite and get back to you soon.");
}
        
        }
    });  //ajax ends here
}

  }
  
    </script>
    <script type="application/javascript">
document.getElementById("copyButton").addEventListener("click", function() {
    copyToClipboard(document.getElementById("copyTarget"));
});

function copyToClipboard(elem) {
	//alert('link copied');
	 $("#alert-msg").text("link copied");
            $(".alert-sectn").fadeIn();
            setTimeout(function () {
                $('.alert-sectn').fadeOut();
            }, 3000);
	  // create hidden text element, if it doesn't already exist
    var targetId = "_hiddenCopyText_";
    var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA"  || elem.tagName === "input";
    var origSelectionStart, origSelectionEnd;
    if (isInput) {
        // can just use the original source element for the selection and copy
        target = elem;
        origSelectionStart = elem.selectionStart;
        origSelectionEnd = elem.selectionEnd;
    } else {
        // must use a temporary form element for the selection and copy
        target = document.getElementById(targetId);
        if (!target) {
            var target = document.createElement("textarea");
            target.style.position = "absolute";
            target.style.left = "-9999px";
            target.style.top = "0";
            target.id = targetId;
            document.body.appendChild(target);
        }
        target.textContent = elem.textContent;
    }
    // select the content
    var currentFocus = document.activeElement;
   // target.focus();
    target.setSelectionRange(0, target.value.length);
    
    // copy the selection
    var succeed;
    try {
    	  succeed = document.execCommand("copy");
    } catch(e) {
        succeed = false;
    }
    // restore original focus
    if (currentFocus && typeof currentFocus.focus === "function") {
        //currentFocus.focus();
    }
    
    if (isInput) {
        // restore prior selection
        elem.setSelectionRange(origSelectionStart, origSelectionEnd);
    } else {
        // clear temporary content
        target.textContent = "";
    }
	
    return succeed;
}

// ga events implementation
//social share 

/*
$(".sharing-list").on('click',function(){ 

ga('send', {
  hitType: 'event',
  eventCategory: 'ILP',
  eventAction: 'SS',
  eventLabel: 'click'
});

 });

*/
//social links implementation

$(document).on('click','.social-icons',function(){
	
	ga('send', {
  hitType: 'event',
  eventCategory: 'ILP',
  eventAction: 'SL',
  eventLabel: 'click'
});
});
 
$(document).on('click','.project1',function(){
	console.log('.project1');
	ga('send', {
  hitType: 'event',
  eventCategory: 'ILP',
  eventAction: 'IP',
  eventLabel: 'view'
});
});


$(document).on('click','.project2',function(){
	console.log('.project2');
	ga('send', {
  hitType: 'event',
  eventCategory: 'ILP',
  eventAction: 'SL',
  eventLabel: 'view'
});
});



$(document).on('click','.project3',function(){
	console.log('.project3');
	ga('send', {
  hitType: 'event',
  eventCategory: 'ILP',
  eventAction: 'EP',
  eventLabel: 'view'
});
});

$(document).on('click','.main-logo2,.visit-site',function(){
	
	ga('send', {
  hitType: 'event',
  eventCategory: 'ILP',
  eventAction: 'HP',
  eventLabel: 'LB'
});
	
});

</script>

</body>
</html>