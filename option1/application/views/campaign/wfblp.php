<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0">

    <title>Landing Page</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url(); ?>content/landingpage/wfblp/content/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url(); ?>content/landingpage/wfblp/content/css/landing-page1.css" rel="stylesheet">
    <link rel="icon" type="image/png" href="http://www.rigalio.com/content/images/favicon.png">
</head>

<body>
<div class="alert-sectn" id="alert-sectn" style="display: none;">
        <h4 id="alert-msg"></h4>
    </div>
    <div class="intro-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                   <div class="main-logo">
                     <img src="<?php echo base_url(); ?>content/landingpage/wfblp/content/images/logo.png" class="img-responsive">
                   </div>
                    <div class="intro-message inner-height">
                      <div class="tab">
                        <div class="tab-cell">
                            <h3>The Ultimate Destination for <span class="utlight"> Discovering Luxury in the World. </span></h3>
                            <hr class="intro-divider">
                            <p>Rigalio is the first in the world to offer registered users a luxury product profiling platform offering the finest and most exclusive insights into the world of luxury. </p>
                        </div>
                       
                      </div>
                      <div class="arrow-stru bounce"></div>
                      <h6>Please fill in the details below to enable your Invite Request.  </h6>
                    </div> <!--/intro-message -->
                </div>
                <div class="col-lg-12 header-frm">
                  <div class="mid col-lg-10 col-sm-12">
                        <ul>
                         <li class="namefield"> <input type="text" name="usernameone" id="usernameone" class="textbox1" required><span class="floating-label">ENTER YOUR NAME</span></li>
                         <li class="emailfield"> <input type="text" name="emailone" id="emailone" class="textbox1" required> <span class="floating-label">ENTER YOUR EMAIL ID</span> </li>
                         <li class="btnfield"><button class="register-btn" id="reg1">REQUEST AN INVITE</button></li>
                        </ul>
                  </div>
                  
                </div> <!--/header-frm -->
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.intro-header -->

    <!-- Page Content -->
    <div class="content-section-a">

        <div class="container-fluid">
            <div class="row">
             <div class="col-lg-12 col-sm-12 fingertip-con">
               <div class="col-lg-6 col-sm-6 col-xs-12 inner-height2">
                 <div class="intro tab">
                   <div class="tab-cell">
                     <h3>luxury at your fingertips </h3>
                     <h5>Exclusively for registered users </h5>
                   </div>
                 </div> <!--/intro -->
                </div>
                <div class="col-lg-6 col-sm-6 col-xs-12">
                    
                </div>
             </div> <!--/fingertip-con -->
             <div class="col-lg-11 col-sm-12 nopadding whats-mor-con mid">
               <ul class="nopadding">
                 <li>offers from luxury brands </li>
                 <li class="seprator"> <hr> </li>
                 <li>private auction alerts </li>
                 <li class="seprator"> <hr> </li>
                 <li>Curated Catalogues </li>
               </ul>
             </div> <!--/whats-mor-con -->
                
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.content-section-a -->

    <div class="content-section-b bestspoke-sectn">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6 col-sm-7 bestspoke-con inner-height2">
                  <div class="tab intro2">
                    <div class="tab-cell">
                      <h3> a bestspoke saga of luxury </h3>
                      <h5>Presonalization in our forte </h5>
                    </div>
                  </div>
                </div>
                <div class="col-lg-12 nopadding bggrey">
                   <div class="col-lg-11 col-sm-12 nopadding whats-mor-con mid">
                       <ul class="nopadding">
                         <li>Create your luxury profile </li>
                         <li class="seprator"> <hr> </li>
                         <li>Assort your Collection</li>
                         <li class="seprator"> <hr> </li>
                         <li>Connect with Rigalio Members</li>
                       </ul>
                    </div> <!--/whats-mor-con -->
                </div>
            </div>
        </div><!-- /.container -->
    </div><!-- /.content-section-b -->
    
    <div class="content-section-d blackbg">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 nopadding">
                  <div class="intro-message inner-height4">
                      <div class="tab">
                        <div class="tab-cell">
                            <h3>come onboard<span class="utlight"> rigalio</span></h3>
                            <p>Discover Deep Desires </p>
                        </div>
                      </div>
                       
                       <h6>Please fill in the details below to enable your Invite Request.  </h6>
                    </div>
                </div>
                <div class="col-lg-12 header-frm">
                  <div class="mid col-lg-10 col-sm-12">
                     
                        <ul>
                         <li class="namefield"> <input type="text" name="usernametwo" value="" id="usernametwo" class="textbox1" required><span class="floating-label">ENTER YOUR NAME</span> </li>
                         <li class="emailfield"> <input type="text" name="emailtwo" value="" id="emailtwo" class="textbox1" required > <span class="floating-label">ENTER YOUR EMAIL ID</span> </li>
                         <li class="btnfield"><button class="register-btn" id="reg2">REQUEST AN INVITE</button>
                         
                         
                          </li>
                        </ul>
                     
                  </div>
                  
                </div> <!--/header-frm -->
            </div>

        </div>
        <!-- /.container -->
    </div><!-- /blackbg -->
    <div class="content-section-e col-lg-12 nopadding ">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 col-sm-12 col-xs-12 nopadding somemore1">
                  <div class="col-lg-6 col-xs-12 folowlux-con nopadding">
                    <h3>follow luxury </h3>
                    <div class="col-lg-12 nopadding social-con">
                      <ul class="nopadding">
                        <li> <a href="https://www.facebook.com/rigalio" target="_blank">facebook </a> </li>
                        <li> <a href="http://www.instagram.com/rigalio" target="_blank">instagram </a> </li>
                        <li> <a href="http://www.twitter.com/rigalioluxury" target="_blank">twitter </a> </li>
                        <li> <a href="https://www.linkedin.com/company/7604532" target="_blank">linkedin </a> </li>
                        <li> <a href="https://plus.google.com/u/0/116609420281563249981/posts" target="_blank">google plus </a> </li>
                      </ul>
                    </div>
                  </div> <!--/folowlux-con -->
                  <div class="col-lg-6 col-xs-12 spread-con nopadding">
                    <h3>spread the word <Span id="copyButton">click to copy </Span> </h3>
                    <a href="<?php echo base_url(); ?>"><h5>http://rigalio.com </h5></a>
                   
                  </div> <!--/spread-con -->
                </div> <!-- /somemore1-->
            </div>
        </div><!-- /.container -->
    </div><!-- /content-section-e -->
    <div class="footer-con col-lg-12 nopadding ">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-9 col-sm-12 col-xs-12 footer-con-in nopadding">
                  <div class="col-lg-6 col-sm-6 col-xs-12 nopadding">
                    <a href="<?php echo base_url(); ?>" target="_blank" ><h4>visit rigalio </h4></a>
                  </div> 
                  <div class="col-lg-6 col-sm-6 col-xs-12 foot-optns nopadding">
                    <ul class="nopadding">
                      <li><a href="<?php echo base_url(); ?>main/faq" target="_blank"> faq</a> </li>
                      <li><a href="<?php echo base_url(); ?>main/privacy" target="_blank"> privacy policy</a> </li>
                      <li><a href="<?php echo base_url(); ?>main/terms_conditions" target="_blank"> terms and conditions</a> </li>
                    </ul>
                  </div> <!--/foot-optns-->
                </div> <!-- /footer-con-in-->
            </div>
        </div><!-- /.container -->
    </div><!-- /footer-con -->


    <!-- jQuery -->
    <script src="<?php echo base_url(); ?>content/landingpage/wfblp/content/js/jquery-1.9.1.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>content/landingpage/wfblp/content/js/bootstrap.min.js"></script>
    
    <script>
	// copy url script start here
	document.getElementById("copyButton").addEventListener("click", function() {
        copyToClipboard(document.getElementById("copyTarget"));
    });

    function copyToClipboard(elem) {
        //alert('link copied');
        $("#alert-msg").text("URL Copied");
        $(".alert-sectn").show("medium");
        setTimeout(function(){
            $('.alert-sectn').fadeOut();
        },3000);
        // create hidden text element, if it doesn't already exist
        var targetId = "_hiddenCopyText_";
        var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA"  || elem.tagName === "input";
        var origSelectionStart, origSelectionEnd;
        if (isInput) {
            // can just use the original source element for the selection and copy
            target = elem;
            origSelectionStart = elem.selectionStart;
            origSelectionEnd = elem.selectionEnd;
        } else {
            // must use a temporary form element for the selection and copy
            target = document.getElementById(targetId);
            if (!target) {
                var target = document.createElement("textarea");
                target.style.position = "absolute";
                target.style.left = "-9999px";
                target.style.top = "0";
                target.id = targetId;
                document.body.appendChild(target);
            }
            target.textContent = elem.textContent;
        }
        // select the content
        var currentFocus = document.activeElement;
        // target.focus();
        target.setSelectionRange(0, target.value.length);

        // copy the selection
        var succeed;
        try {
            succeed = document.execCommand("copy");
        } catch(e) {
            succeed = false;
        }
        // restore original focus
        if (currentFocus && typeof currentFocus.focus === "function") {
            //currentFocus.focus();
        }

        if (isInput) {
            // restore prior selection
            elem.setSelectionRange(origSelectionStart, origSelectionEnd);
        } else {
            // clear temporary content
            target.textContent = "";
        }

        return succeed;
    }

	// copy url script end here
	$(document).on('click', '#reg1', function ()
	{
	var usernameone = $('#usernameone').val();
	var emailone = $('#emailone').val();
	var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;// JavaScript Document
	//alert(usernameone); alert(emailone);
	
	if (usernameone == '') {
	//alert("Please enter your name");
$("#alert-msg").text("Please enter your name");
                    $(".alert-sectn").fadeIn();
                    setTimeout(function () {
                        $('.alert-sectn').fadeOut();
                    }, 3000);
	return false;
    }
	
	if (emailone == '') {
	//alert("Please enter your email");
$("#alert-msg").text("Please Enter email");
                    $(".alert-sectn").fadeIn();
                    setTimeout(function () {
                        $('.alert-sectn').fadeOut();
                    }, 3000);
	return false;
    }
	
	 if (reg.test(emailone) == false) {
	// alert("Please enter your valid email");
	 $("#alert-msg").text("Invalid Email Address");
                    $(".alert-sectn").fadeIn();
                    setTimeout(function () {
                        $('.alert-sectn').fadeOut();
                    }, 3000);
	  return false;
      }
	  else {
		  
		 var base_url= "<?php echo base_url(); ?>";
		 var data = {
		  "usernameone": usernameone,
		  "emailone": emailone
	      };
		
		   $.ajax({
            type: "POST",
            url: "" + base_url + "Request/submituserinfo",
            //dataType: 'json',
            data: data,
            success: function (html) {
				var result = html;
				if(result == '2')
				{
				  $("#alert-msg").text("Already register");
				  $(".alert-sectn").fadeIn();
				  setTimeout(function () {
					  $('.alert-sectn').fadeOut();
				  }, 3000);
				}
				if(result == '1')
				{
				  window.location.href = "" + base_url + "request/beforeregthankyou";
				}
              }
        });
		
		  
	  }
	 

		});
		
		$(document).on('click', '#reg2', function ()
	{
	var usernametwo = $('#usernametwo').val();
	var emailtwo = $('#emailtwo').val();
	var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;// JavaScript Document
	//alert(usernameone); alert(emailone);
	
	if (usernametwo == '') {
	//alert("Please enter your name");
$("#alert-msg").text("Please enter your name");
                    $(".alert-sectn").fadeIn();
                    setTimeout(function () {
                        $('.alert-sectn').fadeOut();
                    }, 3000);
	return false;
    }
	
	if (emailtwo == '') {
	//alert("Please enter your email");
$("#alert-msg").text("Please Enter email");
                    $(".alert-sectn").fadeIn();
                    setTimeout(function () {
                        $('.alert-sectn').fadeOut();
                    }, 3000);
	return false;
    }
	
	 if (reg.test(emailtwo) == false) {
	// alert("Please enter your valid email");
	 $("#alert-msg").text("Invalid Email Address");
                    $(".alert-sectn").fadeIn();
                    setTimeout(function () {
                        $('.alert-sectn').fadeOut();
                    }, 3000);
	  return false;
      }
	   else {
		   var base_url="<?php echo base_url(); ?>";
		   var data = {
            "usernameone": usernametwo,
            "emailone": emailtwo
        };
		
		   $.ajax({
            type: "POST",
            url: "" + base_url + "Request/submituserinfo",
            //dataType: 'json',
            data: data,
            success: function (html) {  
				var result = html;
				if(result == '2')
				{
					$("#alert-msg").text("Already register");
                    $(".alert-sectn").fadeIn();
                    setTimeout(function () {
                        $('.alert-sectn').fadeOut();
                    }, 3000);
				}
				if(result == '1')
				{
					window.location.href = "" + base_url + "request/beforeregthankyou";
				}
              }
        });
		
		  
	  }

		});
	</script>

</body>

</html>
