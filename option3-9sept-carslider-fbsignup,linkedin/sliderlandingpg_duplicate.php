<!DOCTYPE html>
<html lang="en">

<head>
	<script type="text/javascript" src="http://platform.linkedin.com/in.js" defer>
		api_key: 756jk5lm9azphk
		scope: r_basicprofile r_emailaddress
		onLoad: onLinkedInLoad
		//authorize: true
	</script>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
<meta name="description" content="Rigalio is a one stop destination to explore the finest luxury brands, products, news, auctions and editorials across numerous categories.">
<meta name="author" content="">

<title>Sign up to explore the final word on Luxury - Rigalio</title>

<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url(); ?>content/landingpage/wfblp/sliderlandingpg/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="<?php echo base_url(); ?>content/landingpage/wfblp/sliderlandingpg/css/landing2.css" rel="stylesheet">
<!-- jQuery -->
<script src="<?php echo base_url(); ?>content/landingpage/wfblp/sliderlandingpg/js/jquery-2.1.1.js"></script>

<link rel="icon" type="image/png" href="<?php echo base_url(); ?>content/landingpage/wfblp/sliderlandingpg/images/favicon.png">
<script src="<?php echo base_url(); ?>content/landingpage/wfblp/sliderlandingpg/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>content/landingpage/wfblp/sliderlandingpg/js/landing2.js"></script>
<?php /*?><script src="<?php echo base_url(); ?>content/landingpage/wfblp/sliderlandingpg/js/jquery.touchSwipe.min.js" defer></script><?php */?>
<meta property="og:title" content="Sign up to explore the final word on Luxury - Rigalio"/>
	<meta property="og:type" content="product"/>
	<meta property="og:image" content="<?php echo base_url(); ?>content/landingpage/wfblp/sliderlandingpg/images/slide1/slide-1-image.jpg"/>
	<meta property="og:image" content="<?php echo base_url(); ?>content/landingpage/wfblp/sliderlandingpg/images/slide2/slide2-bg.jpg"/>
	<meta property="og:image" content="<?php echo base_url(); ?>content/landingpage/wfblp/sliderlandingpg/images/slide3/slide3-bg.jpg"/>
        <meta property="og:image" content="<?php echo base_url(); ?>content/landingpage/wfblp/sliderlandingpg/images/slide4/slide4-bg.jpg"/>
	<meta property="og:url" content="https://www.facebook.com/rigalio"/>
	<meta property="og:description"
		  content="Rigalio is a one stop destination to explore the finest luxury brands, products, news, auctions and editorials across numerous categories."/>
	<script>
		!function (f, b, e, v, n, t, s) {
			if (f.fbq)return;
			n = f.fbq = function () {
				n.callMethod ?
					n.callMethod.apply(n, arguments) : n.queue.push(arguments)
			};
			if (!f._fbq)f._fbq = n;
			n.push = n;
			n.loaded = !0;
			n.version = '2.0';
			n.queue = [];
			t = b.createElement(e);
			t.async = !0;
			t.src = v;
			s = b.getElementsByTagName(e)[0];
			s.parentNode.insertBefore(t, s)
		}(window,
			document, 'script', 'http://connect.facebook.net/en_US/fbevents.js');

		fbq('init', '1652554988366433');
		fbq('track', "PageView");
        </script>
	<noscript><img height="1" width="1" style="display:none"
				   src="https://www.facebook.com/tr?id=1652554988366433&ev=PageView&noscript=1"
		/></noscript>
	<!-- End Facebook Pixel Code -->
	<script type="text/javascript">
		var axel = Math.random() + "";
		var a = axel * 10000000000000;
		document.write('<iframe src="https://5902450.fls.doubleclick.net/activityi;src=5902450;type=invmedia;cat=ora854ub;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
	</script>
	<noscript>
		<iframe
			src="https://5902450.fls.doubleclick.net/activityi;src=5902450;type=invmedia;cat=ora854ub;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=1?"
			width="1" height="1" frameborder="0" style="display:none"></iframe>
	</noscript>

	<script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 876864855;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
	</script>
	<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
	</script>
	<noscript>
		<div style="display:inline;">
			<img height="1" width="1" style="border-style:none;" alt=""
				 src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/876864855/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
	</noscript>
</head>

<body>
<div id="fb-root"></div>

 </div> <!--/fixed-content -->
<div id="pagename" style="display:none;">signup link page</div>
<script>
	(function (i, s, o, g, r, a, m) {
		i['GoogleAnalyticsObject'] = r;
		i[r] = i[r] || function () {
				(i[r].q = i[r].q || []).push(arguments)
			}, i[r].l = 1 * new Date();
		a = s.createElement(o),
			m = s.getElementsByTagName(o)[0];
		a.async = 1;
		a.src = g;
		m.parentNode.insertBefore(a, m)
	})(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

	ga('create', 'UA-80647225-1', 'auto');
	ga('send', 'pageview');

</script>
    <header id="myCarousel" class="carousel slide">
        <div class="upper-slide">
          <div class="fixed-content">
              <div class="rigalio-logo">
                <a href="http://www.rigalio.com/?utm_source=rigalio&utm_medium=LP&utm_campaign=Invitation" target="_blank"><img src="<?php echo base_url(); ?>content/landingpage/wfblp/sliderlandingpg/images/logo-white.png" class="img-responsive"> </a>
              </div>
              
              <div class="follow-icons">
                <h5>follow </h5>
                <ul class="nopadding">
                  <li class="fb-ico"><a href="https://www.facebook.com/rigalio"> </a></li>
                  <li class="gplus-ico"><a href="https://plus.google.com/116609420281563249981/posts"> </a> </li>
                  <li class="link-ico"><a href="https://www.linkedin.com/company/7604532"> </a> </li>
                  <li class="insta-ico"><a href="https://www.instagram.com/rigalio/"> </a> </li>
                  <li class="twi-ico"> <a href="www.twitter.com/rigalioluxury"> </a> </li>
                </ul>
              </div> <!-- /follow-icons-->
              <div class="scroll-part">
                <h5>scroll down to know more </h5>
                <div class="arrow-stru bounce">
                   <img src="<?php echo base_url(); ?>content/landingpage/wfblp/sliderlandingpg/images/down-arrow.png" class="img-responsive">
                </div>
              </div> <!--/scroll-part -->
              
            </div> <!--/fixed-content -->
        </div> <!-- /upper-slide-->
        <!-- Indicators -->
        <div class="main-menu">
          <ul class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active">time</li>
            <li data-target="#myCarousel" data-slide-to="1">adventure</li>
            <li data-target="#myCarousel" data-slide-to="2">style</li>
            <li data-target="#myCarousel" data-slide-to="3">drive</li>
            <li class="play-state"><div class="state"><div class="normal-play open"><img src="<?php echo base_url(); ?>content/landingpage/wfblp/sliderlandingpg/images/pause-ellipse.png"> </div> <div class="stop-play"><img src="<?php echo base_url(); ?>content/landingpage/wfblp/sliderlandingpg/images/play-ellipse.png"> </div> </div></li>
          </ul>


        </div> <!--/menu -->

        <!-- Wrapper for Slides -->
        <div class="carousel-inner">
            
            <div class="item active slide2">
                <!-- Set the second background image using inline CSS below. -->
                <div class="fill"></div>
                <div class="col-lg-12 col-xs-12 inner-con">
                  <div class="col-lg-4 left-line nopadding">
                    <div class="tab">
                      <div class="tab-in">
                        <h4>The complication of a double tourbillon  </h4>
                      </div>
                    </div>
                  </div> <!--/left-line -->
                  <div class="col-lg-4 mid-line nopadding">
                    <h3> Treating yourself to </h3> <h2>an exquisite </h2> <h4>timepiece ?  </h4>
                    <div class="explore-part"><a
								href="http://www.rigalio.com/?utm_source=rigalio&utm_medium=LP&utm_campaign=Invitation"
								target="_blank">
                    <button>explore rigalio</button></a></div>
                  </div> <!--/mid-line -->
                  <div class="col-lg-4 right-line nopadding">
                    <div class="tab">
                      <div class="tab-in">
                        <h4>The beauty of a vintage minute repeater </h4>
                      </div>
                    </div>
                  </div> <!--/right-line -->
                  <div class="below-tagline">
                     <span>explore the world's best collections at <b>rigalio </b> </span>
                  </div> <!--/below-tagline -->
                </div>
                <div class="upper-slide">
                  <div class="fixed-content">
                      <div class="link-btn-part" onclick="onLinkedInLoad();">
                      <script type="in/Login"></script> 							
                      <div id="profiles"></div>
                        <button></button>
                      </div>
                    </div> <!--/fixed-content -->
                </div> <!-- /upper-slide-->
                <div class="col-lg-12 nopadding section2">
                  <div class="col-lg-10 col-xs-12 section2-inner">
                    <div class="col-lg-12 content-headline">
                       <h3>time </h3> <hr>
                    </div>
                    <div class="col-lg-12 nopadding part2">
                      <div class="col-lg-3 ol-sm-6 col-xs-12 divsn-col4 first-col">
                        <div class="img-part col-lg-12 nopadding"><a href="http://www.rigalio.com/subcategory/watches/6?utm_source=Rigalio&utm_medium=lin&utm_campaign=lux" target="_blank"><img src="<?php echo base_url(); ?>content/landingpage/wfblp/sliderlandingpg/images/slide2/slide2-low1.png" class="img-responsive"></a></div>
                        <div class="descriptn-con col-lg-12 nopadding"> <p>Detailed specifications straight<br><span> from the brand’s headquarter. </span> </p></div>
                      </div> <!--4 col division -->
                      <div class="col-lg-3 ol-sm-6 col-xs-12 divsn-col4 sec-col">
                        <div class="img-part col-lg-12 nopadding"><a href="http://www.rigalio.com/subcategory/watches/6?utm_source=Rigalio&utm_medium=lin&utm_campaign=lux" target="_blank"><img src="<?php echo base_url(); ?>content/landingpage/wfblp/sliderlandingpg/images/slide2/slide2-low2.png" class="img-responsive"></a></div>
                        <div class="descriptn-con col-lg-12 nopadding"> <p>First hand expert reviews from <br><span>people in the industry for decades.</span> </p></div>
                      </div> <!--4 col division -->
                      <div class="col-lg-3 ol-sm-6 col-xs-12 divsn-col4 thi-col">
                        <div class="img-part col-lg-12 nopadding"><a href="http://www.rigalio.com/subcategory/watches/6?utm_source=Rigalio&utm_medium=lin&utm_campaign=lux" target="_blank"><img src="<?php echo base_url(); ?>content/landingpage/wfblp/sliderlandingpg/images/slide2/slide3-low3.png" class="img-responsive"></a></div>
                        <div class="descriptn-con col-lg-12 nopadding"> <p>  Latest product updates,<br> <span> launches, offers, discounts and news from brands. </span> </p></div>
                      </div> <!--4 col division -->
                      <div class="col-lg-3 ol-sm-6 col-xs-12 divsn-col4 for-col">
                        <div class="img-part col-lg-12 nopadding"><a href="http://www.rigalio.com/subcategory/watches/6?utm_source=Rigalio&utm_medium=lin&utm_campaign=lux" target="_blank"><img src="<?php echo base_url(); ?>content/landingpage/wfblp/sliderlandingpg/images/slide2/slide4-low4.png" class="img-responsive"></a></div>
                        <div class="descriptn-con col-lg-12 nopadding"> <p>Get in touch with brand <br> <span>executives at the press of a button. </span></p></div>
                      </div> <!--4 col division -->
                    </div>
                    <div class="division col-lg-12 nopadding"> <hr> </div>
                    
                    <div class="sign-up-foot signup-link col-lg-12 nopadding" onclick="onLinkedInLoad();">
					     <script type="in/Login"></script> 							
                      <div id="profiles"></div>
                      <button></button> </div>
                    <div class="col-lg-12 nopadding footer-con">
                      <p>Our leading watch connoisseurs highlight fascinating timepieces from luxury watch brands as well as news, reviews, features, events, auctions and insights from the world of tourbillons, double axils and minute repeaters. </p>
                      <div class="footer-social">
                        <ul>
                          <li class="fb-foot"><a href="https://www.facebook.com/rigalio"> </a> </li>
                          <li class="gplus-foot"><a href="https://plus.google.com/116609420281563249981/posts"> </a> </li>
                          <li class="linkedin-foot"><a href="https://www.linkedin.com/company/7604532"> </a> </li>
                          <li class="insta-foot"><a href="https://www.instagram.com/rigalio/"> </a> </li>
                          <li class="twitter-foot"><a href="www.twitter.com/rigalioluxury"> </a> </li>
                        </ul>
                      </div> <!--/footer-social -->
                      <div class="explore-part"><a href="http://www.rigalio.com/?utm_source=rigalio&utm_medium=LP&utm_campaign=Invitation" target="_blank"><button>explore rigalio</button></a> </div>
                    </div> <!--/footer-con -->
                  </div> <!--/section2-inner -->
                </div> <!-- /section2-->
                
            </div> <!-- /slide2-->
            <div class="item slide3">
                <!-- Set the third background image using inline CSS below. -->
                <div class="fill"></div>
                <div class="col-lg-12 col-xs-12 inner-con">
                  <div class="col-lg-4 left-line nopadding">
                    <div class="tab">
                      <div class="tab-in">
                        <h4>The solitude of   the Antarctica  </h4>
                      </div>
                    </div>
                  </div> <!--/left-line -->
                  <div class="col-lg-4 mid-line nopadding">
                    <h3> The next family </h3> <h2>vacation</h2> <h4>seeks a new destination </h4>
                    <div class="explore-part"><a
								href="http://www.rigalio.com/?utm_source=rigalio&utm_medium=LP&utm_campaign=Invitation"
								target="_blank">
                    <button>explore rigalio</button></a></div>
                  </div> <!--/mid-line -->
                  <div class="col-lg-4 right-line nopadding">
                    <div class="tab">
                      <div class="tab-in">
                        <h4>An African Safari on the Masaimara</h4>
                      </div>
                    </div>
                  </div> <!--/right-line -->
                  <div class="below-tagline">
                     <span>explore the finest customized travel deals from luxury travel experts on <b>rigalio </b> </span>
                  </div> <!--/below-tagline -->
                </div>
                <div class="upper-slide">
                  <div class="fixed-content">
                      <div class="link-btn-part" onclick="onLinkedInLoad();">
                      <script type="in/Login"></script> 							
                      <div id="profiles"></div>
                        <button></button>
                      </div>
                    </div> <!--/fixed-content -->
                </div> <!-- /upper-slide-->
                
                <div class="col-lg-12 nopadding section2">
                  <div class="col-lg-10 col-xs-12 section2-inner">
                    <div class="col-lg-12 content-headline">
                       <h3>adventure </h3> <hr>
                    </div>
                    <div class="col-lg-12 nopadding part2">
                      <div class="col-lg-3 ol-sm-6 col-xs-12 divsn-col4 first-col">
                        <div class="img-part col-lg-12 nopadding"><a href="http://www.rigalio.com/subcategory/vacations/16?utm_source=Rigalio&utm_medium=lin&utm_campaign=lux" target="_blank"><img src="<?php echo base_url(); ?>content/landingpage/wfblp/sliderlandingpg/images/slide3/slide3-low1.png" class="img-responsive"></a></div>
                        <div class="descriptn-con col-lg-12 nopadding"> <p>Experience different  <br><span> travel destinations. </span> </p></div>
                      </div> <!--4 col division -->
                      <div class="col-lg-3 ol-sm-6 col-xs-12 divsn-col4 sec-col">
                        <div class="img-part col-lg-12 nopadding"><a href="http://www.rigalio.com/subcategory/vacations/16?utm_source=Rigalio&utm_medium=lin&utm_campaign=lux" target="_blank"><img src="<?php echo base_url(); ?>content/landingpage/wfblp/sliderlandingpg/images/slide3/slide3-low2.png" class="img-responsive"></a></div>
                        <div class="descriptn-con col-lg-12 nopadding"> <p>Acquire in depth knowledge <br><span>about our curated destinations <br>especially for you.</span> </p></div>
                      </div> <!--4 col division -->
                      <div class="col-lg-3 ol-sm-6 col-xs-12 divsn-col4 thi-col">
                        <div class="img-part col-lg-12 nopadding"><a href="http://www.rigalio.com/subcategory/vacations/16?utm_source=Rigalio&utm_medium=lin&utm_campaign=lux" target="_blank"><img src="<?php echo base_url(); ?>content/landingpage/wfblp/sliderlandingpg/images/slide3/slide3-low3.png" class="img-responsive"></a></div>
                        <div class="descriptn-con col-lg-12 nopadding"> <p>  Follow the insights <br> <span> given by the internationally <br> awarded travel gurus. </span> </p></div>
                      </div> <!--4 col division -->
                      <div class="col-lg-3 ol-sm-6 col-xs-12 divsn-col4 for-col">
                        <div class="img-part col-lg-12 nopadding"><a href="http://www.rigalio.com/subcategory/vacations/16?utm_source=Rigalio&utm_medium=lin&utm_campaign=lux" target="_blank"><img src="<?php echo base_url(); ?>content/landingpage/wfblp/sliderlandingpg/images/slide3/slide3-low4.png" class="img-responsive"></a></div>
                        <div class="descriptn-con col-lg-12 nopadding"> <p>Let Rigalio design custom travel   <br> <span>packages in consultation with luxury operators for your personalized needs. </span></p></div>
                      </div> <!--4 col division -->
                    </div>
                    <div class="division col-lg-12 nopadding"> <hr> </div>
                    <div class="sign-up-foot signup-link col-lg-12 nopadding" onclick="onLinkedInLoad();">
					     <script type="in/Login"></script> 							
                      <div id="profiles"></div><button></button> 
                      </div>
                    <div class="col-lg-12 nopadding footer-con">
                      <p>Seasoned travel carriers make you experience the most exotic destinations on the land, seas and the skies. Let Rigalio help you enjoy the vacation trip of a lifetime. </p>
                      <div class="footer-social">
                        <ul>
                          <li class="fb-foot"><a href="https://www.facebook.com/rigalio"> </a> </li>
                          <li class="gplus-foot"><a href="https://plus.google.com/116609420281563249981/posts"> </a> </li>
                          <li class="linkedin-foot"><a href="https://www.linkedin.com/company/7604532"> </a> </li>
                          <li class="insta-foot"><a href="https://www.instagram.com/rigalio/"> </a> </li>
                          <li class="twitter-foot"><a href="www.twitter.com/rigalioluxury"> </a> </li>
                        </ul>
                      </div> <!--/footer-social -->
                      <div class="explore-part"><a href="http://www.rigalio.com/?utm_source=rigalio&utm_medium=LP&utm_campaign=Invitation" target="_blank"><button>explore rigalio</button></a> </div>
                    </div> <!--/footer-con -->
                  </div> <!--/section2-inner -->
                </div> <!-- /section2-->
            </div><!--/slide3 -->
            <div class="item slide4">
                <!-- Set the third background image using inline CSS below. -->
                <div class="fill"></div>
                <div class="col-lg-12 col-xs-12 inner-con">
                  <div class="col-lg-4 left-line nopadding">
                    <div class="tab">
                      <div class="tab-in">
                        <h4>Suit up like a Parisian gentleman </h4>
                      </div>
                    </div>
                  </div> <!--/left-line -->
                  <div class="col-lg-4 mid-line nopadding">
                    <h3> What to wear for </h3> <h2>the next </h2> <h4>big occasion in life </h4>
                    <div class="explore-part"><a
								href="http://www.rigalio.com/?utm_source=rigalio&utm_medium=LP&utm_campaign=Invitation"
								target="_blank">
                    <button>explore rigalio</button></a></div>
                  </div> <!--/mid-line -->
                  <div class="col-lg-4 right-line nopadding">
                    <div class="tab">
                      <div class="tab-in">
                        <h4>Stand apart in a Savile Row creation  </h4>
                      </div>
                    </div>
                  </div> <!--/right-line -->
                  <div class="below-tagline">
                    <span>Learn about the best in the world of men’s fashion from our ‘haute’ style curators.  </span>
                  </div> <!--/below-tagline -->
                </div>
                <div class="upper-slide">
                  <div class="fixed-content">
                      <div class="link-btn-part" onclick="onLinkedInLoad();">
                      <script type="in/Login"></script> 							
                      <div id="profiles"></div>
                        <button></button>
                      </div>
                    </div> <!--/fixed-content -->
                </div> <!-- /upper-slide-->
                
                <div class="col-lg-12 nopadding section2">
                  <div class="col-lg-10 col-xs-12 section2-inner">
                    <div class="col-lg-12 content-headline">
                       <h3>style </h3> <hr>
                    </div>
                    <div class="col-lg-12 nopadding part2">
                      <div class="col-lg-3 ol-sm-6 col-xs-12 divsn-col4 first-col">
                        <div class="img-part col-lg-12 nopadding"><a href="http://www.rigalio.com/subcategory/men’s-fashion/10?utm_source=Rigalio&utm_medium=lin&utm_campaign=lux" target="_blank"><img src="<?php echo base_url(); ?>content/landingpage/wfblp/sliderlandingpg/images/slide4/slide4-low1.png" class="img-responsive"></a></div>
                        <div class="descriptn-con col-lg-12 nopadding"> <p>Choose from a wide array  <br><span>of style accessories to team up with the perfect three-piece.</span> </p></div>
                      </div> <!--4 col division -->
                      <div class="col-lg-3 ol-sm-6 col-xs-12 divsn-col4 sec-col">
                        <div class="img-part col-lg-12 nopadding"><a href="http://www.rigalio.com/subcategory/men’s-fashion/10?utm_source=Rigalio&utm_medium=lin&utm_campaign=lux" target="_blank"><img src="<?php echo base_url(); ?>content/landingpage/wfblp/sliderlandingpg/images/slide4/slide4-low2.png" class="img-responsive"></a></div>
                        <div class="descriptn-con col-lg-12 nopadding"> <p> Know more about niche  <br><span>luxury brands that are unknown to the common masses.</span> </p></div>
                      </div> <!--4 col division -->
                      <div class="col-lg-3 ol-sm-6 col-xs-12 divsn-col4 thi-col">

                        <div class="img-part col-lg-12 nopadding"><a href="http://www.rigalio.com/subcategory/men’s-fashion/10?utm_source=Rigalio&utm_medium=lin&utm_campaign=lux" target="_blank"><img src="<?php echo base_url(); ?>content/landingpage/wfblp/sliderlandingpg/images/slide4/slide4-low3.png" class="img-responsive"></a></div>
                        <div class="descriptn-con col-lg-12 nopadding"> <p>Request fashion pieces from<br> <span> the recent collections exclusively at your doorstep. </span> </p></div>
                      </div> <!--4 col division -->
                      <div class="col-lg-3 ol-sm-6 col-xs-12 divsn-col4 for-col">
                        <div class="img-part col-lg-12 nopadding"><a href="http://www.rigalio.com/subcategory/men’s-fashion/10?utm_source=Rigalio&utm_medium=lin&utm_campaign=lux" target="_blank"><img src="<?php echo base_url(); ?>content/landingpage/wfblp/sliderlandingpg/images/slide4/slide4-low4.png" class="img-responsive"></a></div>
                        <div class="descriptn-con col-lg-12 nopadding"> <p>Get in touch with<br> <span>our style curators to suit your personalized tastes.</span></p></div>
                      </div> <!--4 col division -->
                    </div>
                    <div class="division col-lg-12 nopadding"> <hr> </div>
                    <div class="sign-up-foot signup-link col-lg-12 nopadding" onclick="onLinkedInLoad();"><script type="in/Login"></script> 							
                      <div id="profiles"></div><button></button> </div>
                    <div class="col-lg-12 nopadding footer-con">
                      <p>Fashion experts bringing you the latest insights from the biggest brands in the world of luxury fashion. Our style curators will help you dress for a party, promotion or the biggest occasion of your life, wedding. Trust our style pundits to assist you in scorching the ramp or walking down the aisle.  </p>
                      <div class="footer-social">
                        <ul>
                          <li class="fb-foot"><a href="https://www.facebook.com/rigalio"> </a> </li>
                          <li class="gplus-foot"><a href="https://plus.google.com/116609420281563249981/posts"> </a> </li>
                          <li class="linkedin-foot"><a href="https://www.linkedin.com/company/7604532"> </a> </li>
                          <li class="insta-foot"><a href="https://www.instagram.com/rigalio/"> </a> </li>
                          <li class="twitter-foot"><a href="www.twitter.com/rigalioluxury"> </a> </li>
                        </ul>
                      </div> <!--/footer-social -->
                      <div class="explore-part"><a href="http://www.rigalio.com/?utm_source=rigalio&utm_medium=LP&utm_campaign=Invitation" target="_blank"><button>explore rigalio</button></a> </div>
                    </div> <!--/footer-con -->
                  </div> <!--/section2-inner -->
                </div> <!-- /section2-->
            </div><!-- /slide4-->
            <div class="item slide1">
                <!-- Set the first background image using inline CSS below. -->
                <div class="fill"></div>
                <div class="col-lg-12 col-xs-12 inner-con">
                  <div class="col-lg-4 left-line nopadding">
                    <div class="tab">
                      <div class="tab-in">
                        <h4>the benchmark in performance engineering  </h4>
                      </div>
                    </div>
                  </div> <!--/left-line -->
                  <div class="col-lg-4 mid-line nopadding">
                    <h3> planning to make a</h3> <h2>burning </h2> <h4>statement this year </h4>
                    <div class="explore-part"> <a
                href="http://www.rigalio.com/?utm_source=rigalio&utm_medium=LP&utm_campaign=Invitation"
                target="_blank">
                    <button>explore rigalio</button></a> </div>
                  </div> <!--/mid-line -->
                  <div class="col-lg-4 right-line nopadding">
                    <div class="tab">
                      <div class="tab-in">
                        <h4>the ultimate luxury icon crafted in wood </h4>
                      </div>
                    </div>
                  </div> <!--/right-line -->
                  <div class="below-tagline">
                    <span>separate the best from the rest in the world of automobiles on <b>rigalio </b> </span>
                  </div> <!--/below-tagline -->
                </div>
                <div class="upper-slide">
                  <div class="fixed-content">
                      <div class="link-btn-part" onclick="onLinkedInLoad();">
                      <script type="in/Login" data-width="300"></script>              
                      <div id="profiles"></div>
                        <button></button>
                      </div>
                    </div> <!--/fixed-content -->
                </div> <!-- /upper-slide-->
                
                <div class="col-lg-12 nopadding section2">
                  <div class="col-lg-10 col-xs-12 section2-inner">
                    <div class="col-lg-12 content-headline">
                       <h3>drive </h3> <hr>
                    </div>
                    <div class="col-lg-12 nopadding part2">
                      <div class="col-lg-3 ol-sm-6 col-xs-12 divsn-col4 first-col">
                        <div class="img-part col-lg-12 nopadding"><a href="http://www.rigalio.com/category/wings-and-wheels/1?utm_source=Rigalio&utm_medium=lin&utm_campaign=lux" target="_blank"><img src="<?php echo base_url(); ?>content/landingpage/wfblp/sliderlandingpg/images/slide1/slide1-low1.png" class="img-responsive"></a></div>
                        <div class="descriptn-con col-lg-12 nopadding"> <p>Detailed specifications <br><span> straight from the brand. </span> </p></div>
                      </div> <!--4 col division -->
                      <div class="col-lg-3 ol-sm-6 col-xs-12 divsn-col4 sec-col">
                        <div class="img-part col-lg-12 nopadding"><a href="http://www.rigalio.com/category/wings-and-wheels/1?utm_source=Rigalio&utm_medium=lin&utm_campaign=lux" target="_blank"><img src="<?php echo base_url(); ?>content/landingpage/wfblp/sliderlandingpg/images/slide1/slide1-low2.png" class="img-responsive"></a></div>
                        <div class="descriptn-con col-lg-12 nopadding"> <p>First hand expert reviews <br><span>from the brand’s test drivers.</span> </p></div>
                      </div> <!--4 col division -->
                      <div class="col-lg-3 ol-sm-6 col-xs-12 divsn-col4 thi-col">
                        <div class="img-part col-lg-12 nopadding"><a href="http://www.rigalio.com/category/wings-and-wheels/1?utm_source=Rigalio&utm_medium=lin&utm_campaign=lux" target="_blank"><img src="<?php echo base_url(); ?>content/landingpage/wfblp/sliderlandingpg/images/slide1/slide1-low3.png" class="img-responsive"></a></div>
                        <div class="descriptn-con col-lg-12 nopadding"> <p> Latest product updates,<br> <span> launches, offers and news from car marques. </span> </p></div>
                      </div> <!--4 col division -->
                      <div class="col-lg-3 ol-sm-6 col-xs-12 divsn-col4 for-col">
                        <div class="img-part col-lg-12 nopadding"><a href="http://www.rigalio.com/category/wings-and-wheels/1?utm_source=Rigalio&utm_medium=lin&utm_campaign=lux" target="_blank"><img src="<?php echo base_url(); ?>content/landingpage/wfblp/sliderlandingpg/images/slide1/slide1-low4.png" class="img-responsive"></a></div>
                        <div class="descriptn-con col-lg-12 nopadding"> <p>Get in touch with brands <br> <span>at the press of a button. </span></p></div>
                      </div> <!--4 col division -->
                    </div>
                    <div class="division col-lg-12 nopadding"> <hr> </div>
                  
                    <div class="sign-up-foot signup-link col-lg-12 nopadding" onclick="onLinkedInLoad();"><script type="in/Login"></script>               
                      <div id="profiles"></div>
                        <button></button> </div>
                    <div class="col-lg-12 nopadding footer-con">
                      <p>Our dedicated petrolheads cover and feature the hottest and rarest wheels of luxury automobile brands as well as reviews, features, events, auctions and insights from the world of supercars and hypercars. </p>
                      <div class="footer-social">
                        <ul>
                          <li class="fb-foot"><a href="https://www.facebook.com/rigalio"> </a> </li>
                          <li class="gplus-foot"><a href="https://plus.google.com/116609420281563249981/posts"> </a> </li>
                          <li class="linkedin-foot"><a href="https://www.linkedin.com/company/7604532"> </a> </li>
                          <li class="insta-foot"><a href="https://www.instagram.com/rigalio/"> </a> </li>
                          <li class="twitter-foot"><a href="www.twitter.com/rigalioluxury"> </a> </li>
                        </ul>
                      </div> <!--/footer-social -->
                      <div class="explore-part"><a href="http://www.rigalio.com/?utm_source=rigalio&utm_medium=LP&utm_campaign=Invitation" target="_blank"><button>explore rigalio</button></a> </div>
                    </div> <!--/footer-con -->
                  </div> <!--/section2-inner -->
                </div> <!-- /section2-->
            </div> <!--/slide1 -->
        </div> <!-- /carousel-inner-->
        
        <div class="static-sectn"><div class="goto-top-icon"><a class="scrollup" href="javascript:void(0)"><img src="<?php echo base_url(); ?>content/landingpage/wfblp/sliderlandingpg/images/go-to-top_white.png" class="img-responsive"> </a> </div></div>

        <!-- Controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span><img src="<?php echo base_url(); ?>content/landingpage/wfblp/sliderlandingpg/images/arrow-left.png" class="img-responsive"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span><img src="<?php echo base_url(); ?>content/landingpage/wfblp/sliderlandingpg/images/arrow-right.png" class="img-responsive"></span>
        </a>

    </header>
    
    <!-- Script to Activate the Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 5000,//changes the speed
        pause: "false"
    })
    </script>

<script>
	var base_url = '<?php echo base_url(); ?>';
	function onLinkedInLoad() {
		var pagename = '';
		IN.Event.on(IN, "auth", onLinkedInAuth);
	}

	// 2. Runs when the viewer has authenticated
	function onLinkedInAuth() {
		pagename = $('#pagename').html();
		// console.log(refnameinkdin);
		var myinfo = IN.API.Profile("me").fields("id", "first-name", "last-name", "email-address", "headline", "siteStandardProfileRequest", "picture-url", "industry").result(displayProfiles);

	}

	// 2. Runs when the Profile() API call returns successfully
	function displayProfiles(profiles) {
//console.log(profiles);
		var member = profiles.values[0];
		//$('.loader-part').show();
		// $(".msg2").html('<img src=\"<?php echo base_url() . "spin.gif"; ?>\"/>');
		var data = {
            "pagename": pagename,
			"fname": member.firstName,
			"lname": member.lastName,
			"email": member.emailAddress,
			//"phoneno": member.phoneNumbers,
			"headline": member.headline,
			"industry": member.industry,
			"picture": member.pictureUrl,
			"link": member.siteStandardProfileRequest.url,
			"utm_source": getCookie('utm_source'),
			"utm_medium" : getCookie('utm_medium'),
			"utm_campaign" : getCookie('utm_campaign')
		};

		$.ajax({
			type: "POST",
			url: "" + base_url + "request/sabyasacchiinvitelinkedin",
			data: data,
			success: function (html) {
				// var me = html
				// alert(html);
				console.log(html);
				if (html == 1) {
					$("#alert-msg").text("You have already requested an invite.");
					$(".alert-sectn").fadeIn();
					setTimeout(function () {
						$('.alert-sectn').fadeOut();
					}, 3000);
					// $('.msg2').html("You have already requested an invite.");
				} else {
					window.location.href = "" + base_url + "request/langingpagesignup";
					// $('.msg2').html("Thank you for your interest, We will facililate your invite and get back to you soon.");

					//  window.location.href = "http://www.rigalio.com/Main/thankyou";
				}
			}
		});  //ajax ends here


	}

	function mysend(url){

		FB.ui({
			method: 'send',
			link: url,
		});
	}
</script>
    
    <script>
	var base_url= "<?php echo base_url(); ?>";
	// facebook invite button script start here

	window.fbAsyncInit = function() {
		FB.init({
			appId   : '165343490529660',

			oauth   : true,
			status  : true, // check login status
			cookie  : true, // enable cookies to allow the server to access the session
			xfbml   : true // parse XFBML
		});

	};



	function fb_login(){

 pagename = $('#pagename').html();
	  //console.log(pagename);
	  
		FB.login(function(response) {
				if (response.authResponse) {
					console.log('Authenticated!');
					// location.reload(); //or do whatever you want
					 $('body').addClass('loading');
					FB.api('me?fields=first_name,last_name,email,gender,link,friends,locale,invitable_friends.limit(1000),birthday,picture.width(500).height(500)', function(response) {
						console.log('Successful login for: ' + response);
						//console.log(response);
						var i = response.invitable_friends;
						// console.log(refname);
						var data = {
							"pagename": pagename,
							"fname": response.first_name,
							"lname": response.last_name,
							"email": response.email,
							"user_fb_id": response.id,
							"utm_source": getCookie('utm_source'),
							"utm_medium" : getCookie('utm_medium'),
							"utm_campaign" : getCookie('utm_campaign'),
							"gender": response.gender,
							"picture": response['picture']['data']['url'],
							"link": response.link,
							"birthday": response.birthday,
							"friendlist": i
						};
						$.ajax({
							type: "POST",
							url: "" + base_url + "request/sabyasacchireqinvitefb",
							data: data,
							success: function (html) {
								if (html == 1) {
									 $('body').removeClass('loading');
//$('.msg2').html("You have already requested an invite.");
									$("#alert-msg").text("You have already requested an invite.");
									$(".alert-sectn").fadeIn();
									setTimeout(function () {
										$('.alert-sectn').fadeOut();
									}, 3000);
								} else {
									// $('body').removeClass('loading');
//window.location.href="afterregthankyou.php";
									window.location.href = "" + base_url + "request/langingpagesignup";
//$('.msg2').html("Thank you for your interest, We will facililate your invite and get back to you soon.");
								}
							}
						});  //ajax ends here


					});

			} else {
					console.log('User cancelled login or did not fully authorize.');
				}
			},
			{
				scope: 'email,user_friends'
			});

	}


	(function() {
		var e = document.createElement('script');
		e.src = document.location.protocol + '//connect.facebook.net/en_US/all.js';
		e.async = true;
		document.getElementById('fb-root').appendChild(e);
	}());

	// facebook invite button script end here
	
	//cookies for the campaign

	function getUrlVars(){
		var vars = {};
		var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
			vars[key] = value;
		});
		return vars;
	}
	if(getUrlVars()["utm_medium"]){

		var utm_medium = getUrlVars()["utm_medium"];
		var utm_campaign = getUrlVars()["utm_campaign"];
		var utm_source = getUrlVars()["utm_source"];

		function setCookie(cname, cvalue, exdays) {
			var d = new Date();
			d.setTime(d.getTime() + (exdays * 1000 * 60 * 60 * 24 * 30));
			// var expires = "expires="+d.toUTCString();
			var expires = "expires="+new Date(new Date().getTime() + 1000 * 60 * 60 * 24 * 30 * 30);
			document.cookie = cname + "=" + cvalue + "; " + expires + "; path=/";
		}
		setCookie("utm_source", utm_source, new Date(new Date().getTime() + 1000 * 60 * 60 * 24 * 30));
		setCookie("utm_medium", utm_medium, new Date(new Date().getTime() + 1000 * 60 * 60 * 24 * 30));
		setCookie("utm_campaign", utm_campaign, new Date(new Date().getTime() + 1000 * 60 * 60 * 24 * 30));
	}

	function getCookie(c_name) { if (document.cookie.length > 0) { c_start = document.cookie.indexOf(c_name + "="); if (c_start != -1) { c_start = c_start + c_name.length + 1; c_end = document.cookie.indexOf(";", c_start); if (c_end == -1) { c_end = document.cookie.length; } return unescape(document.cookie.substring(c_start, c_end)); } } return "N/A"; }

	function listCookies() {
		var theCookies = document.cookie.split(';');
		var aString = '';
		for (var i = 1 ; i <= theCookies.length; i++) {
			aString += i + ' ' + theCookies[i-1] + "\n";
		}
		return aString;
	}


	console.log(getCookie("utm_source"));
	console.log(getCookie("utm_medium"));
	console.log(getCookie("utm_campaign"));
</script>


</body>
</html>


