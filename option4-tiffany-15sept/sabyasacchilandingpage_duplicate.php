<!doctype html>
<html lang="en" class="no-js">
<head>
<!--<script src="http://www.rigalio.com/content/landingpage/wfblp/content2/js/link.js"></script>-->
	<script type="text/javascript">
		api_key: 756jk5lm9azphk
		scope: r_basicprofile r_emailaddress
		onLoad: onLinkedInLoad
		//authorize: true
	</script>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0">
	<link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700|Merriweather:400italic,400' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="<?php echo base_url(); ?>content/landingpage/wfblp/content2/css/reset.css"> <!-- CSS reset -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>content/landingpage/wfblp/content2/css/tiffany.css"> <!-- Resource style -->
	 <!-- Modernizr -->
	<link rel="icon" type="image/png" href="<?php echo base_url(); ?>content/landingpage/wfblp/content2/img/favicon.png">
	<!-- <script type="text/javascript" src="<?php echo base_url(); ?>/content/js/landinglogin.js"></script>-->
	<title>Rigalio</title>
	<meta property="og:title" content="Request an Invite to access the World of Luxury"/>
	<meta property="og:type" content="product"/>
	<meta property="og:image" content="<?php echo base_url(); ?>content/landingpage/wfblp/content2/img/facebook-share-image-1.jpg"/>
	<meta property="og:image" content="<?php echo base_url(); ?>content/landingpage/wfblp/content2/img/facebook-share-image-2.jpg"/>
	<meta property="og:image" content="<?php echo base_url(); ?>content/landingpage/wfblp/content2/img/facebook-share-image-3.jpg"/>
	<meta property="og:url" content="https://www.facebook.com/rigalio"/>
	<meta property="og:description"
		  content="Rigalio is one of a kind invite only digital luxury cataloguing platform providing its consumers the finest and most exclusive insights into the world of luxury. For an unedited dose of luxury create your own timeline and enjoy a personalised experience."/>
	
</head>
<body>
<div id="fb-root"></div>
<div id="pagename" style="display:none;">sabyasacchi duplicate page</div>
<div class="landscape-pg" style="display:none;"><img
		src="<?php echo base_url(); ?>content/landingpage/wfblp/content2/img/turndevice.gif"></div>
<script>
	(function (i, s, o, g, r, a, m) {
		i['GoogleAnalyticsObject'] = r;
		i[r] = i[r] || function () {
				(i[r].q = i[r].q || []).push(arguments)
			}, i[r].l = 1 * new Date();
		a = s.createElement(o),
			m = s.getElementsByTagName(o)[0];
		a.async = 1;
		a.src = g;
		m.parentNode.insertBefore(a, m)
	})(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

	ga('create', 'UA-80647225-1', 'auto');
	ga('send', 'pageview');

</script>
<div class="projects-container">
	<div class="alert-sectn" id="alert-sectn" style="display: none;">
		<h4 id="alert-msg"></h4>
	</div>
	<ul>
		<li class="cd-single-project project1 is-loaded">
			<div class="cd-title">
				<h2>Look enchanting </br> in red</h2>
				<p>More</p>
			</div> <!-- .cd-title -->

			<div class="cd-project-info">

				<div class="inner-con">
					<div class="logo-part"><img src="<?php echo base_url(); ?>content/landingpage/wfblp/content2/img/logo.png"
												class="img-responsive"></div>

					<h3>Look enchanting in red</h3>
					<hr>
					<p>A sultry floor length gown in red with a deep neckline, decorated in gold zardozi embroidery. A perfect party dress to don this festive season and step out in a bold avatar while still looking your traditional best</p>
					<div class="share-pg read-pg"><a href="http://www.rigalio.com/editorial/article/5/?utm_source=Lyxel&utm_medium=LP&utm_campaign=Wedding1" target="_blank">Read More</a>
					</div>
					<div class="foot-con">
						<p id="msg2" class="msg2 langinpg"></p>
						<!--<div class="invite-part"> <a href="#" class="invite-btn"> Sign up via:</a> </div>-->
						<div class="invite-via-sym">
							<ul>
								<span> <a href="#" class="invite-btn">Sign up via:</a> </span><li onclick="fb_login();" class="via-fb">
									<div class="register-fb"> </div>
								</li>
								<?php /*?><li class="via-link" onclick="onLinkedInLoad();">
									<div class="linkedin-log"> <script type="in/Login"></script></div>
									<div id="profiles"></div>
								</li><?php */?>
							</ul>
						</div>
						<div class="visit-site">Explore <a
								href="http://www.rigalio.com/?utm_source=rigalio&utm_medium=LP&utm_campaign=Invitation%20"
								target="_blank">Rigalio</a></div>
						<div class="social-icons">
							<ul>
								<li>Follow Rigalio:</li>
								<li><a href="https://www.facebook.com/rigalio" target="_blank"> <img
											src="<?php echo base_url(); ?>content/landingpage/wfblp/content2/img/facebook.png"> </a>
								</li>
								<li><a href="https://plus.google.com/u/0/116609420281563249981/posts" target="_blank">
										<img src="<?php echo base_url(); ?>content/landingpage/wfblp/content2/img/gplus.png"> </a></li>
								<li><a href="https://www.linkedin.com/company/7604532" target="_blank"> <img
											src="<?php echo base_url(); ?>content/landingpage/wfblp/content2/img/linkedin.png"> </a>
								</li>
								<li><a href="http://www.twitter.com/rigalioluxury" target="_blank"> <img
											src="<?php echo base_url(); ?>content/landingpage/wfblp/content2/img/twitter.png"> </a>
								</li>
								<li><a href="http://www.instagram.com/rigalio" target="_blank"> <img
											src="<?php echo base_url(); ?>content/landingpage/wfblp/content2/img/instagram.png"> </a>
								</li>
							</ul>
						</div>
					</div> <!--/foot-con -->

				</div>
			</div> <!-- .cd-project-info -->
		</li>

		<li class="cd-single-project project2 is-loaded">
			<div class="cd-title">
				<h2> Stand tall and be </br> a head turner </h2>
				<p>More</p>
			</div> <!-- .cd-title -->

			<div class="cd-project-info">
				<div class="inner-con">
					<div class="logo-part"><img src="<?php echo base_url(); ?>content/landingpage/wfblp/content2/img/logo.png"
												class="img-responsive"></div>
					<h3>Stand tall and be a head turner</h3>
					<hr>
					<p>Contemporary and very feminine luxurious sandals crafted in satin to give your gorgeous feet a soft touch.</p>
					<div class="share-pg read-pg"><a href="http://www.rigalio.com/editorial/article/5/?utm_source=Lyxel&utm_medium=LP&utm_campaign=Wedding1" target="_blank">Read More</a>
					</div>
					<div class="foot-con">
						<p id="msg2" class="msg2 langinpg"></p>
						<!--<div class="invite-part"> <a href="#" class="invite-btn"> Sign up via:</a> </div>-->
						<div class="invite-via-sym">
							<ul>
								<span> <a href="#" class="invite-btn">Sign up via:</a> </span><li onclick="fb_login();" class="via-fb">
									<div class="register-fb"> </div>
								</li>


								<?php /*?><li class="via-link" onclick="onLinkedInLoad();"> <div class="linkedin-log"><script type="in/Login"></script> </div>
									<div id="profiles"></div></li><?php */?>
							</ul>
						</div>
						<div class="visit-site">Explore <a
								href="http://www.rigalio.com/?utm_source=rigalio&utm_medium=LP&utm_campaign=Invitation%20"
								target="_blank">Rigalio</a></div>
						<div class="social-icons">
							<ul>
								<li>Follow Rigalio:</li>
								<li><a href="https://www.facebook.com/rigalio" target="_blank"> <img
											src="<?php echo base_url(); ?>content/landingpage/wfblp/content2/img/facebook.png"> </a>
								</li>
								<li><a href="https://plus.google.com/u/0/116609420281563249981/posts" target="_blank">
										<img src="<?php echo base_url(); ?>content/landingpage/wfblp/content2/img/gplus.png"> </a></li>
								<li><a href="https://www.linkedin.com/company/7604532" target="_blank"> <img
											src="<?php echo base_url(); ?>content/landingpage/wfblp/content2/img/linkedin.png"> </a>
								</li>
								<li><a href="http://www.twitter.com/rigalioluxury" target="_blank"> <img
											src="<?php echo base_url(); ?>content/landingpage/wfblp/content2/img/twitter.png"> </a>
								</li>
								<li><a href="http://www.instagram.com/rigalio" target="_blank"> <img
											src="<?php echo base_url(); ?>content/landingpage/wfblp/content2/img/instagram.png"> </a>
								</li>
							</ul>
						</div>
					</div> <!--/foot-con -->

				</div>
			</div> <!-- .cd-project-info -->
		</li>

		<li class="cd-single-project project3 is-loaded">
			<div class="cd-title">
				<h2>For that perfect </br> pout and nails </h2>
				<p>More</p>
			</div> <!-- .cd-title -->

			<div class="cd-project-info">
				<div class="inner-con">
					<div class="logo-part"><img src="<?php echo base_url(); ?>content/landingpage/wfblp/content2/img/logo.png"
												class="img-responsive"></div>
					<h3>For that perfect pout and nails</h3>
					<hr>
					<p>As the weather cools down, it’s time to move towards a colour that is bold and beautiful, the scarlet and rouge range by Tom Ford makes them a perfect pick this festive season.</p>
					<div class="share-pg read-pg"><a href="http://www.rigalio.com/editorial/article/5/?utm_source=Lyxel&utm_medium=LP&utm_campaign=Wedding1" target="_blank">Read More</a>
					</div>
					<div class="foot-con">
						<p id="msg2" class="msg2 langinpg"></p>
						<!--<div class="invite-part"> <a href="#" class="invite-btn"> Sign up via:</a> </div>-->

						<div class="invite-via-sym">
							<ul>
								<span> <a href="#" class="invite-btn">Sign up via:</a> </span><li onclick="fb_login();" class="via-fb">
									<div class="register-fb"> </div>
								</li>
								<?php /*?><li class="via-link" onclick="onLinkedInLoad();"> <div class="linkedin-log"><script type="in/Login"></script> </div>
									<div id="profiles"></div></li><?php */?>

							</ul>
						</div>
						<div class="visit-site">Explore <a
								href="http://www.rigalio.com/?utm_source=rigalio&utm_medium=LP&utm_campaign=Invitation%20"
								target="_blank">Rigalio</a></div>
						<div class="social-icons">
							<ul>
								<li>Follow Rigalio:</li>
								<li><a href="https://www.facebook.com/rigalio" target="_blank"> <img
											src="<?php echo base_url(); ?>content/landingpage/wfblp/content2/img/facebook.png"> </a>
								</li>
								<li><a href="https://plus.google.com/u/0/116609420281563249981/posts" target="_blank">
										<img src="<?php echo base_url(); ?>content/landingpage/wfblp/content2/img/gplus.png"> </a></li>
								<li><a href="https://www.linkedin.com/company/7604532" target="_blank"> <img
											src="<?php echo base_url(); ?>content/landingpage/img/linkedin.png"> </a>
								</li>
								<li><a href="http://www.twitter.com/rigalioluxury" target="_blank"> <img
											src="<?php echo base_url(); ?>content/landingpage/wfblp/content2/img/twitter.png"> </a>
								</li>
								<li><a href="http://www.instagram.com/rigalio" target="_blank"> <img
											src="<?php echo base_url(); ?>content/landingpage/wfblp/content2/img/instagram.png"> </a>
								</li>
							</ul>
						</div>
					</div> <!--/foot-con -->

				</div>
			</div> <!-- .cd-project-info -->
		</li>

		<li class="cd-single-project project4 last-col is-loaded">
			<div class="cd-title">
				<div class="main-logo2"><a
						href="http://www.rigalio.com/?utm_source=rigalio&utm_medium=LP&utm_campaign=Invitation"
						target="_blank"><img src="<?php echo base_url(); ?>content/landingpage/wfblp/content2/img/rigaliologo.png"></a>
				</div>

				<form>
					<h3>Planning your Wedding? </h3><h2> Our style curators reveal selections that even luxury magazines don't.</h2>
					<p id="msg2" class="msg2"></p>
					<ul>
						<li onclick="fb_login();"> <div class="invite-btns"><span> Sign up via Facebook</span> </div>
							<div class="register-fb"> </div>
						</li>

						<?php /*?><li onclick="onLinkedInLoad();"> <div class="invite-btns"><span> Sign up via linkedin</span> </div>
							<div class="linkedin-log"> <script type="in/Login"></script> </div>
							<div id="profiles"></div>
						</li><?php */?>

					</ul>
				</form>
                <h4>#The<span>Wedding</span>Diaries </h4>
				<div class="foot-con">
					<div class="share-pg"><a> Share this Page </a>
					</div>
					<div class="sharing-list" style="display:none;">
						<ul>
							<li>
								<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url(); ?>main/invite"
								   target="_blank"> <img
										src="<?php echo base_url(); ?>content/landingpage/wfblp/content2/img/facebook-share.png"
										class="img-responsive"></a></li>
							<li><a href="https://plus.google.com/share?url=<?php echo base_url(); ?>main/invite"
								   target="_blank"><img
										src="<?php echo base_url(); ?>content/landingpage/wfblp/content2/img/google-plus-share.png"
										class="img-responsive"></a></li>

							<li>
								<a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo base_url(); ?>main/invite"
								   target="_blank"><img
										src="<?php echo base_url(); ?>content/landingpage/wfblp/content2/img/linkedin-share.png"
										class="img-responsive"></a></li>

							<li>

								<a href="https://twitter.com/home?status=<?php echo base_url(); ?>main/invite"
								   target="_blank"><img
										src="<?php echo base_url(); ?>content/landingpage/wfblp/content2/img/twitter-share.png"
										class="img-responsive"></a></li>


							<li>
								<img id="copyButton" src="<?php echo base_url(); ?>content/landingpage/wfblp/content2/img/copy-url.png"
									 class="img-responsive">
							</li>
						</ul>
					</div>
					<div class="social-icons">
						<ul>
							<li>Follow Rigalio:</li>
							<li><a href="https://www.facebook.com/rigalio" target="_blank"> <img
										src="<?php echo base_url(); ?>content/landingpage/wfblp/content2/img/facebook.png"> </a></li>
							<li><a href="https://plus.google.com/u/0/116609420281563249981/posts" target="_blank"> <img
										src="<?php echo base_url(); ?>content/landingpage/wfblp/content2/img/gplus.png"> </a></li>
							<li><a href="https://www.linkedin.com/company/7604532" target="_blank"> <img
										src="<?php echo base_url(); ?>content/landingpage/wfblp/content2/img/linkedin.png"> </a></li>
							<li><a href="http://www.twitter.com/rigalioluxury" target="_blank"> <img
										src="<?php echo base_url(); ?>content/landingpage/wfblp/content2/img/twitter.png"> </a></li>
							<li><a href="http://www.instagram.com/rigalio" target="_blank"> <img
										src="<?php echo base_url(); ?>content/landingpage/wfblp/content2/img/instagram.png"> </a></li>
						</ul>
					</div>
					<p> By clicking on the Request button, you agree with the<a
							href="http://www.rigalio.com/main/terms_conditions?utm_source=rigalio&utm_medium=LP&utm_campaign=Invitation%20"
							target="_blank"> Terms and Conditions</a> and <a
							href="http://www.rigalio.com/main/privacy?utm_source=rigalio&utm_medium=LP&utm_campaign=Invitation%20"
							target="_blank">Privacy Policy</a> of Rigalio</p>
				</div> <!--/foot-con -->
			</div> <!-- .cd-title -->

			<div class="cd-project-info">
			</div> <!-- .cd-project-info -->
		</li>
	</ul>
	<a href="#0" class="cd-close">Close</a>
	<a href="#0" class="cd-scroll">Scroll</a>
	<input type="text" id="copyTarget" style="opacity:0;" value="<?php
		$url = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		echo $url;
	?>">
</div> <!-- .project-container -->

<script src="<?php echo base_url(); ?>content/landingpage/wfblp/content2/js/jquery-2.1.1.js"></script>
<script src="<?php echo base_url(); ?>content/landingpage/wfblp/content2/js/main.js"></script> <!-- Resource jQuery -->
<!--<script src="<?php echo base_url(); ?>content/js/all.js"></script>-->
<script>
	var base_url= "<?php echo base_url(); ?>";
	// facebook invite button script start here

	window.fbAsyncInit = function() {
		FB.init({
			appId   : '165343490529660',
			oauth   : true,
			status  : true, // check login status
			cookie  : true, // enable cookies to allow the server to access the session
			xfbml   : true // parse XFBML
		});

	};


	function fb_login(){

 pagename = $('#pagename').html();
	  //console.log(pagename);
	  
		FB.login(function(response) {
				if (response.authResponse) {
					console.log('Authenticated!');
					// location.reload(); //or do whatever you want
					 $('body').addClass('loading');
					FB.api('me?fields=first_name,last_name,email,gender,link,friends,locale,invitable_friends.limit(1000),birthday,picture.width(500).height(500)', function(response) {
						console.log('Successful login for: ' + response);
						//console.log(response);
						var i = response.invitable_friends;
						// console.log(refname);
						var data = {
							"pagename": pagename,
							"fname": response.first_name,
							"lname": response.last_name,
							"email": response.email,
							"user_fb_id": response.id,
							"utm_source": getCookie('utm_source'),
							"utm_medium" : getCookie('utm_medium'),
							"utm_campaign" : getCookie('utm_campaign'),
							"gender": response.gender,
							"picture": response['picture']['data']['url'],
							"link": response.link,
							"birthday": response.birthday,
							"friendlist": i
						};
						$.ajax({
							type: "POST",
							url: "" + base_url + "request/sabyasacchireqinvitefb",
							data: data,
							success: function (html) {
								if (html == 1) {
									 $('body').removeClass('loading');
//$('.msg2').html("You have already requested an invite.");
									$("#alert-msg").text("You have already requested an invite.");
									$(".alert-sectn").fadeIn();
									setTimeout(function () {
										$('.alert-sectn').fadeOut();
									}, 3000);
								} else {
									// $('body').removeClass('loading');
//window.location.href="afterregthankyou.php";
									window.location.href = "" + base_url + "request/langingpagesignup";
//$('.msg2').html("Thank you for your interest, We will facililate your invite and get back to you soon.");
								}
							}
						});  //ajax ends here


					});

			} else {
					console.log('User cancelled login or did not fully authorize.');
				}
			},
			{
				scope: 'email,user_friends'
			});

	}


	(function() {
		var e = document.createElement('script');
		e.src = document.location.protocol + '//connect.facebook.net/en_US/all.js';
		e.async = true;
		document.getElementById('fb-root').appendChild(e);
	}());

	// facebook invite button script end here
</script>
<script>
	var base_url = '<?php echo base_url(); ?>';
	function onLinkedInLoad() {
		var pagename = '';
		IN.Event.on(IN, "auth", onLinkedInAuth);
	}

	// 2. Runs when the viewer has authenticated
	function onLinkedInAuth() {
		pagename = $('#pagename').html();
		// console.log(refnameinkdin);
		var myinfo = IN.API.Profile("me").fields("id", "first-name", "last-name", "email-address", "headline", "siteStandardProfileRequest", "picture-url", "industry").result(displayProfiles);

	}

	// 2. Runs when the Profile() API call returns successfully
	function displayProfiles(profiles) {
//console.log(profiles);
		var member = profiles.values[0];
		//$('.loader-part').show();
		// $(".msg2").html('<img src=\"<?php echo base_url() . "spin.gif"; ?>\"/>');
		var data = {
            "pagename": pagename,
			"fname": member.firstName,
			"lname": member.lastName,
			"email": member.emailAddress,
			//"phoneno": member.phoneNumbers,
			"headline": member.headline,
			"industry": member.industry,
			"picture": member.pictureUrl,
			"link": member.siteStandardProfileRequest.url,
			"utm_source": getCookie('utm_source'),
			"utm_medium" : getCookie('utm_medium'),
			"utm_campaign" : getCookie('utm_campaign')
		};

		$.ajax({
			type: "POST",
			url: "" + base_url + "request/sabyasacchiinvitelinkedin",
			data: data,
			success: function (html) {
				// var me = html
				// alert(html);
				console.log(html);
				if (html == 1) {
					$("#alert-msg").text("You have already requested an invite.");
					$(".alert-sectn").fadeIn();
					setTimeout(function () {
						$('.alert-sectn').fadeOut();
					}, 3000);
					// $('.msg2').html("You have already requested an invite.");
				} else {
					window.location.href = "" + base_url + "request/langingpagesignup";
					// $('.msg2').html("Thank you for your interest, We will facililate your invite and get back to you soon.");

					//  window.location.href = "http://www.rigalio.com/Main/thankyou";
				}
			}
		});  //ajax ends here


	}

	function mysend(url){

		FB.ui({
			method: 'send',
			link: url,
		});
	}
</script>
<script type="application/javascript">
	document.getElementById("copyButton").addEventListener("click", function () {
		copyToClipboard(document.getElementById("copyTarget"));
	});

	function copyToClipboard(elem) {
		//alert('link copied');
		$("#alert-msg").text("link copied");
		$(".alert-sectn").fadeIn();
		setTimeout(function () {
			$('.alert-sectn').fadeOut();
		}, 3000);
		// create hidden text element, if it doesn't already exist
		var targetId = "_hiddenCopyText_";
		var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA" || elem.tagName === "input";
		var origSelectionStart, origSelectionEnd;
		if (isInput) {
			// can just use the original source element for the selection and copy
			target = elem;
			origSelectionStart = elem.selectionStart;
			origSelectionEnd = elem.selectionEnd;
		} else {
			// must use a temporary form element for the selection and copy
			target = document.getElementById(targetId);
			if (!target) {
				var target = document.createElement("textarea");
				target.style.position = "absolute";
				target.style.left = "-9999px";
				target.style.top = "0";
				target.id = targetId;
				document.body.appendChild(target);
			}
			target.textContent = elem.textContent;
		}
		// select the content
		var currentFocus = document.activeElement;
		// target.focus();
		target.setSelectionRange(0, target.value.length);

		// copy the selection
		var succeed;
		try {
			succeed = document.execCommand("copy");
		} catch (e) {
			succeed = false;
		}
		// restore original focus
		if (currentFocus && typeof currentFocus.focus === "function") {
			//currentFocus.focus();
		}

		if (isInput) {
			// restore prior selection
			elem.setSelectionRange(origSelectionStart, origSelectionEnd);
		} else {
			// clear temporary content
			target.textContent = "";
		}

		return succeed;
	}

	// ga events implementation
	//social share

	/*
	 $(".sharing-list").on('click',function(){

	 ga('send', {
	 hitType: 'event',
	 eventCategory: 'ILP',
	 eventAction: 'SS',
	 eventLabel: 'click'
	 });

	 });

	 */
	//social links implementation

	$(document).on('click', '.social-icons', function () {

		ga('send', {
			hitType: 'event',
			eventCategory: 'ILP',
			eventAction: 'SL',
			eventLabel: 'click'
		});
	});

	$(document).on('click', '.project1', function () {
		console.log('.project1');
		ga('send', {
			hitType: 'event',
			eventCategory: 'ILP',
			eventAction: 'IP',
			eventLabel: 'view'
		});
	});


	$(document).on('click', '.project2', function () {
		console.log('.project2');
		ga('send', {
			hitType: 'event',
			eventCategory: 'ILP',
			eventAction: 'SL',
			eventLabel: 'view'
		});
	});


	$(document).on('click', '.project3', function () {
		console.log('.project3');
		ga('send', {
			hitType: 'event',
			eventCategory: 'ILP',
			eventAction: 'EP',
			eventLabel: 'view'
		});
	});

	$(document).on('click', '.main-logo2,.visit-site', function () {

		ga('send', {
			hitType: 'event',
			eventCategory: 'ILP',
			eventAction: 'HP',
			eventLabel: 'LB'
		});

	});

	//cookies for the campaign

	function getUrlVars(){
		var vars = {};
		var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
			vars[key] = value;
		});
		return vars;
	}
	if(getUrlVars()["utm_medium"]){

		var utm_medium = getUrlVars()["utm_medium"];
		var utm_campaign = getUrlVars()["utm_campaign"];
		var utm_source = getUrlVars()["utm_source"];

		function setCookie(cname, cvalue, exdays) {
			var d = new Date();
			d.setTime(d.getTime() + (exdays * 1000 * 60 * 60 * 24 * 30));
			// var expires = "expires="+d.toUTCString();
			var expires = "expires="+new Date(new Date().getTime() + 1000 * 60 * 60 * 24 * 30 * 30);
			document.cookie = cname + "=" + cvalue + "; " + expires + "; path=/";
		}
		setCookie("utm_source", utm_source, new Date(new Date().getTime() + 1000 * 60 * 60 * 24 * 30));
		setCookie("utm_medium", utm_medium, new Date(new Date().getTime() + 1000 * 60 * 60 * 24 * 30));
		setCookie("utm_campaign", utm_campaign, new Date(new Date().getTime() + 1000 * 60 * 60 * 24 * 30));
	}

	function getCookie(c_name) { if (document.cookie.length > 0) { c_start = document.cookie.indexOf(c_name + "="); if (c_start != -1) { c_start = c_start + c_name.length + 1; c_end = document.cookie.indexOf(";", c_start); if (c_end == -1) { c_end = document.cookie.length; } return unescape(document.cookie.substring(c_start, c_end)); } } return "N/A"; }

	function listCookies() {
		var theCookies = document.cookie.split(';');
		var aString = '';
		for (var i = 1 ; i <= theCookies.length; i++) {
			aString += i + ' ' + theCookies[i-1] + "\n";
		}
		return aString;
	}


	console.log(getCookie("utm_source"));
	console.log(getCookie("utm_medium"));
	console.log(getCookie("utm_campaign"));

</script>

<script>
		!function (f, b, e, v, n, t, s) {
			if (f.fbq)return;
			n = f.fbq = function () {
				n.callMethod ?
					n.callMethod.apply(n, arguments) : n.queue.push(arguments)
			};
			if (!f._fbq)f._fbq = n;
			n.push = n;
			n.loaded = !0;
			n.version = '2.0';
			n.queue = [];
			t = b.createElement(e);
			t.async = !0;
			t.src = v;
			s = b.getElementsByTagName(e)[0];
			s.parentNode.insertBefore(t, s)
		}(window,
			document, 'script', 'http://connect.facebook.net/en_US/fbevents.js');

		fbq('init', '1652554988366433');
		fbq('track', "PageView");</script>
	<noscript><img height="1" width="1" style="display:none"
				   src="https://www.facebook.com/tr?id=1652554988366433&ev=PageView&noscript=1"
		/></noscript>
	<!-- End Facebook Pixel Code -->
	<script type="text/javascript">
		var axel = Math.random() + "";
		var a = axel * 10000000000000;
		document.write('<iframe src="https://5902450.fls.doubleclick.net/activityi;src=5902450;type=invmedia;cat=ora854ub;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
	</script>
	<noscript>
		<iframe
			src="https://5902450.fls.doubleclick.net/activityi;src=5902450;type=invmedia;cat=ora854ub;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=1?"
			width="1" height="1" frameborder="0" style="display:none"></iframe>
	</noscript>

	<script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 876864855;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
	</script>
	<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
	</script>
	<noscript>
		<div style="display:inline;">
			<img height="1" width="1" style="border-style:none;" alt=""
				 src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/876864855/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
	</noscript>
<script src="<?php echo base_url(); ?>content/landingpage/wfblp/content2/js/modernizr.js"></script>

</body>
</html>
